package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Project

class ActionModuleTsTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.project$.test().values().with {
      assert it.size() == 1
      Project project = it.first()
      assert project
      ActionModuleTs generator = new ActionModuleTs(project)
      assert generator.relPath == 'action.module.ts'
      generator.source.with {
        assert it.contains(
        """
        |import {Observer} from 'rxjs'
        |import {NgModule} from '@angular/core'
        |import {CommonModule} from '@angular/common'
        |import {InvokeRestApi} from './action/invoke-rest-api.service'
        |import {SetValueToPageItem} from './action/set-value-to-page-item.service'
        |import {AddNumberToPageItem} from './action/add-number-to-page-item.service'
        """.trim().stripMargin())

        assert it.contains(
        """
        |export {InvokeRestApi, InvokeRestApiArgs} from './action/invoke-rest-api.service'
        |export {SetValueToPageItem, SetValueToPageItemArgs} from './action/set-value-to-page-item.service'
        |export {AddNumberToPageItem, AddNumberToPageItemArgs} from './action/add-number-to-page-item.service' 
        """.trim().stripMargin())

        assert it.contains(
        """
        |@NgModule(
        |{ imports: [
        |    CommonModule,
        |  ]
        |, providers: [
        |    InvokeRestApi,
        |    SetValueToPageItem,
        |    AddNumberToPageItem,
        |  ]
        |})
        |export class ActionModule{}
        """.trim().stripMargin())

        assert it.contains(
        """
        |export interface Action<TArgs, TStream> {
        |  observer : (args:TArgs) => Observer<TStream> 
        |}
        """.trim().stripMargin())
      }
    }
  }
}
