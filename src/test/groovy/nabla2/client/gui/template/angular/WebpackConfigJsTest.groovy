package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Project

class WebpackConfigJsTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.project$.test().values().with {
      assert it.size() == 1
      Project project = it.first()
      assert project
      WebPackConfigJs generator = new WebPackConfigJs(project)
      assert generator.relPath == 'webpack.config.js'
      generator.source.with {

        assert it.contains("""
        |const webpack = require('webpack')
        |const HtmlWebpackPlugin = require('html-webpack-plugin')
        |const ExtractTextPlugin = require('extract-text-webpack-plugin')
        """.stripMargin().trim())

        assert it.contains("""       
        |module.exports =
        |{ entry:
        |  { app: './src/main/js/main.ts' }
        |, output:
        |  { filename : '[name].js'
        |  , path : __dirname + '/src/main/resources/static/'
        |  }
        |, resolve:
        |  { extensions: ['.ts', '.js']
        |  }
        |, module:
        |  { rules:
        |    [ { test   :/\\.ts\$/
        |      , loaders:[ 'awesome-typescript-loader'
        |                , 'angular2-template-loader'
        |                ]
        |      }
        |    , { test   :/\\.html\$/
        |      , loader :'html-loader'
        |      }  
        |    , { test   :/\\.css\$/
        |      , loader :'style-loader!css-loader'
        |      }        
        |    ]
        |  }
        |, plugins:
        |  [ new HtmlWebpackPlugin({template:'src/main/js/index.html'})
        |  ] 
        |}
        """.stripMargin().trim())
      }
    }
  }
}
