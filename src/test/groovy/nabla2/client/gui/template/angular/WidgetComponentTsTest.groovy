package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.PageItemType

class WidgetComponentTsTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSourceOfAWidgetWithMultipleAttributes() {
    loader.pageItemType$.test().values().with {
      PageItemType itemType = it.find{ it.name.sameAs('カード') }
      assert itemType
      WidgetComponentTs generator = new WidgetComponentTs(itemType)
      assert generator.relPath == 'widget/card.component.ts'
      generator.source.with {

        assert it.contains("""
        |import {Component, Input, ChangeDetectionStrategy} from '@angular/core'
        |import {Router} from '@angular/router'
        |import {Observable, Subject, BehaviorSubject} from 'rxjs'
        |import {UIEventHub, ViewModel} from '../base.module'
        |import {AppState} from '../main'
        """.stripMargin().trim())

        assert it.contains("""
        |@Component(
        |{ template:`
        |<md-card>
        |  <md-card-header>
        |    <md-card-title>{{title}}</md-card-title>
        |    <md-card-subtitle>{{subtitle}}</md-card-subtitle>
        |  </md-card-header>
        |  <md-card-content>{{content}}</md-card-content>
        |</md-card>
        |`
        |, selector : 'widget-card'
        |, changeDetection: ChangeDetectionStrategy.OnPush
        |})
        |export class Card {
        """.stripMargin().trim())

        assert it.contains("""
        |  /**
        |   * 画面項目パス
        |   */
        |  @Input() path:string = '' 
        """.stripMargin().trim())

        assert it.contains("""
        |  /**
        |   * タイトル
        |   */
        |  @Input() title:any = ''
        |  /**
        |   * サブタイトル
        |   */
        |  @Input() subtitle:any = ''
        |  /**
        |   * 内容
        |   */
        |  @Input() content:any = ''
        """.stripMargin().trim())

        assert it.contains("""
        |  constructor(
        |    private uiEventHub:UIEventHub,
        |    private viewModel:ViewModel<AppState>,
        |    private router:Router,
        |  ) {}
        """.stripMargin().trim())

        assert it.contains("""
        |  on(event:Event, index?:number) {
        |    this.uiEventHub.next(this.path, event, index)
        |  }
        """.stripMargin().trim())

        assert it.contains("""
        |  update(event:Event) {
        |    const element:HTMLFormElement = <HTMLFormElement>event.target
        |    this.on(event)
        |    this.viewModel.update(this.path, element.value)
        |  }
        """.stripMargin().trim())
      }
    }
  }
}
