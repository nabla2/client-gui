package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Feature

class FeatureModuleTsTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.feature$.test().values().with {
      Feature feature = it.find{ it.name.sameAs('単語帳機能') }
      assert feature
      FeatureModuleTs generator = new FeatureModuleTs(feature)
      assert generator.relPath == 'app/deck.module.ts'
      generator.source.with {
        assert it.contains("""
        |import {Component, NgModule} from '@angular/core'
        |import {CommonModule as NgCommonModule} from '@angular/common'
        |import {RouterModule, Routes} from '@angular/router'
        |import {HttpModule} from '@angular/http'
        """.stripMargin().trim())

        assert it.contains("""
        |import {ListPageComponent} from './deck/list-page.component'
        |import {LearnPageComponent} from './deck/learn-page.component'
        |import {EditPageComponent} from './deck/edit-page.component'
        |import {WidgetModule} from '../widget.module'
        """.stripMargin().trim())

        assert it.contains("""       
        |@Component({
        |  template : `
        |    <h2>単語帳機能</h2>
        |    <router-outlet></router-outlet>
        |  `
        |})
        |class DeckComponent {}
        """.stripMargin().trim())

        assert it.contains("""
        |const routes:Routes =
        |[ { path: 'deck'
        |  , component: DeckComponent
        |  , children: [
        |      { path: 'list', component:ListPageComponent },
        |      { path: 'learn', component:LearnPageComponent },
        |      { path: 'edit', component:EditPageComponent },
        |      { path: '**', redirectTo: 'list' },
        |    ]
        |  }
        |]
        """.stripMargin().trim())

        assert it.contains("""
        |export interface DeckModuleState {
        |  deckListTable : {
        |    deckNameColumn : string
        |    deckLink : any
        |    numberOfCardsColumn : string
        |  }[]
        |  flashcard : {
        |    deckName : any
        |    numberDisp : any
        |    content : any
        |    currentNumber : any
        |    numberOfCards : any
        |    front : any
        |    back : any
        |  }
        |}
        """.stripMargin().trim())

        assert it.contains("""
        |export const DeckModuleInitialState:DeckModuleState = {
        |  deckListTable : [],
        |  flashcard : {
        |    deckName : "英検1級 001-100",
        |    numberDisp : "1枚目/100枚",
        |    content : null,
        |    currentNumber : 1,
        |    numberOfCards : 100,
        |    front : null,
        |    back : null,
        |  },
        |}
        """.stripMargin().trim())

        assert it.contains("""
        |@NgModule(
        |{ imports:
        |  [ NgCommonModule
        |  , HttpModule
        |  , WidgetModule
        |  , RouterModule.forChild(routes)
        |  ]
        |, declarations:
        |  [ DeckComponent
        |  , ListPageComponent
        |  , LearnPageComponent
        |  , EditPageComponent
        |  ]
        |})
        |export class DeckModule {}
        """.stripMargin().trim())
      }
    }
  }
}
