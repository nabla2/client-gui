package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Page

class PageTemplateHtmlTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.page$.test().values().with {
      Page page = it.find{
        it.featureName.sameAs('単語帳機能') &&
        it.name.sameAs('学習画面')
      }
      assert page
      PageTemplateHtml generator = new PageTemplateHtml(page)
      assert generator.relPath == 'app/deck/learn-page.component.html'
      generator.source.with {
        assert it.contains("""
        |<widget-card
        |  path = "deck.flashcard"
        |  key = "flashcard"
        |  [value] = "state?.deck?.flashcard"
        |  [subtitle] = "state?.deck?.flashcard?.numberDisp"
        |  [title] = "state?.deck?.flashcard?.deckName"
        |  [content] = "state?.deck?.flashcard?.content" >
        |</widget-card>
        """.stripMargin().trim())

        assert it.contains("""
        |<widget-button
        |  path = "deck.showAnswer"
        |  key = "showAnswer"
        |  [value] = "state?.deck?.showAnswer"
        |  title = "こたえ" >
        |</widget-button>
        """.stripMargin().trim())

        assert it.contains("""
        |<widget-button
        |  path = "deck.backToQuestion"
        |  key = "backToQuestion"
        |  [value] = "state?.deck?.backToQuestion"
        |  title = "もんだい" >
        |</widget-button>
        """.stripMargin().trim())

        assert it.contains("""
        |<widget-button
        |  path = "deck.showNextCard"
        |  key = "showNextCard"
        |  [value] = "state?.deck?.showNextCard"
        |  title = "つぎ" >
        |</widget-button>
        """.stripMargin().trim())
      }
    }
  }
}
