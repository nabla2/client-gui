package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Action
import nabla2.client.gui.model.Event

class EventServiceTsTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.event$.test().values().with {
      Event event = it.find {
        it.name.sameAs('画面が表示されたとき')
      }
      assert event
      EventServiceTs generator = new EventServiceTs(event)
      assert generator.relPath == 'event/when-page-showed.service.ts'
      generator.source.with {
        assert it.contains(
        """
        |import {Observable} from 'rxjs'
        |import {Injectable} from '@angular/core'
        |import {EventSource} from '../event.module'
        |import {UIEvent, UIEventHub, ViewModel} from '../base.module'
        |import {AppState} from '../main'
        """.trim().stripMargin())

        assert it.contains(
        """
        |@Injectable()
        |export class WhenPageShowed implements EventSource<WhenPageShowedArgs, any> {
        """.trim().stripMargin())

        assert it.contains(
        """
        |  constructor(
        |    private uiEventHub:UIEventHub,
        |    private viewModel:ViewModel<AppState>
        |  ) {}
        """.trim().stripMargin())

        assert it.contains(
        """
        |  observable(args:WhenPageShowedArgs, context?:any):Observable<any> {
        |    return Observable.empty()
        |  }
        |}
        """.trim().stripMargin())

        assert it.contains(
        """
        |export interface WhenPageShowedArgs {
        |  
        |}
        """.trim().stripMargin())
      }
    }
  }
}
