package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Project

class WidgetModuleTsTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.project$.test().values().with {
      assert it.size() == 1
      Project project = it.first()
      assert project
      WidgetModuleTs generator = new WidgetModuleTs(project)
      assert generator.relPath == 'widget.module.ts'
      generator.source.with {
        assert it.contains("""
        |import {NgModule} from '@angular/core'
        |import {CommonModule} from '@angular/common'
        |import {Button} from './widget/button.component'
        |import {Card} from './widget/card.component'
        |import {CheckFlag} from './widget/check-flag.component'
        |import {Checkboxes} from './widget/checkboxes.component'
        |import {DataHolder} from './widget/data-holder.component'
        |import {Form} from './widget/form.component'
        |import {MultiLineTextInput} from './widget/multi-line-text-input.component'
        |import {Pulldown} from './widget/pulldown.component'
        |import {SingleLineTextInput} from './widget/single-line-text-input.component'
        |import {TableColumnDataHolder} from './widget/table-column-data-holder.component'
        |import {TableColumn} from './widget/table-column.component'
        |import {Table} from './widget/table.component'
        """.stripMargin().trim())

        assert it.contains("""
        |@NgModule(
        |{ imports: [
        |    CommonModule,
        |  ]
        |, exports: [
        |    Button,
        |    Card,
        |    CheckFlag,
        |    Checkboxes,
        |    DataHolder,
        |    Form,
        |    MultiLineTextInput,
        |    Pulldown,
        |    SingleLineTextInput,
        |    Table,
        |    TableColumn,
        |    TableColumnDataHolder,
        |  ]
        |, declarations: [
        |    Button,
        |    Card,
        |    CheckFlag,
        |    Checkboxes,
        |    DataHolder,
        |    Form,
        |    MultiLineTextInput,
        |    Pulldown,
        |    SingleLineTextInput,
        |    Table,
        |    TableColumn,
        |    TableColumnDataHolder,
        |  ]
        |})
        |export class WidgetModule{}
        """.stripMargin().trim())
      }
    }
  }
}
