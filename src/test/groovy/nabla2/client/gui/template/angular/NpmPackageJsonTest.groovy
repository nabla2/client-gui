package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Project

class NpmPackageJsonTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.project$.test().values().with {
      assert it.size() == 1
      Project project = it.first()
      assert project
      NpmPackageJson generator = new NpmPackageJson(project)
      assert generator.relPath == 'package.json'
      generator.source.with {
        assert it.contains("""
        |{ "//"      : "@author nabla2.metamodel.generator"
        |, "name"    : "flashcard"
        |, "version" : "1.0.0"
        |, "scripts" :
        |  { "build" : "webpack"
        |  , "watch" : "webpack --watch"
        |  }
        """.stripMargin().trim())

        assert it.contains("""
        |, "dependencies":
        |  { "@angular/core"                     : "*"
        |  , "@angular/compiler"                 : "*"
        |  , "@angular/common"                   : "*"
        |  , "@angular/http"                     : "*"
        |  , "@angular/platform-browser"         : "*"
        |  , "@angular/platform-browser-dynamic" : "*"
        |  , "@angular/router"                   : "*"
        |  , "@angular/forms"                    : "*"
        |  , "core-js"                           : "*"
        |  , "rxjs"                              : "*"
        |  , "zone.js"                           : "*"
        |  , "deep-extend"                       : "*"
        |  }
        """.stripMargin().trim())

        assert it.contains("""
        |, "devDependencies":
        |  { "@types/node"                 : "*"
        |  , "@types/core-js"              : "*"
        |  , "@types/deep-extend"          : "*"
        |  , "typescript"                  : "*"
        |  , "webpack"                     : "*"
        |  , "angular2-template-loader"    : "*"
        |  , "awesome-typescript-loader"   : "*"
        |  , "style-loader"                : "*"
        |  , "css-loader"                  : "*"
        |  , "html-loader"                 : "*"
        |  , "html-webpack-plugin"         : "*"
        |  , "extract-text-webpack-plugin" : "*"
        |  }
        |}
        """.stripMargin().trim())
      }
    }
  }
}
