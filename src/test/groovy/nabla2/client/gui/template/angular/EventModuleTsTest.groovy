package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Project

class EventModuleTsTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.project$.test().values().with {
      assert it.size() == 1
      Project project = it.first()
      assert project
      EventModuleTs generator = new EventModuleTs(project)
      assert generator.relPath == 'event.module.ts'
      generator.source.with {
        assert it.contains(
        """
        |import {Observable} from 'rxjs'
        |import {NgModule} from '@angular/core'
        |import {CommonModule} from '@angular/common'
        |import {BaseModule} from './base.module'
        |import {WhenPageShowed} from './event/when-page-showed.service'
        """.trim().stripMargin())

        assert it.contains(
        """
        |export {WhenPageShowed, WhenPageShowedArgs} from './event/when-page-showed.service'
        """.trim().stripMargin())

        assert it.contains(
        """
        |@NgModule(
        |{ imports: [
        |    CommonModule,
        |    BaseModule,
        |  ]
        |, providers: [
        |    WhenPageShowed,
        |    WhenPageItemClicked,
        |    NoFilter,
        |  ]
        |})
        |export class EventModule{}
        """.trim().stripMargin())

        assert it.contains(
        """
        |export interface EventSource<TArgs, TStream> {
        |  observable : (args:TArgs) => Observable<TStream> 
        |}
        """.trim().stripMargin())
      }
    }
  }
}
