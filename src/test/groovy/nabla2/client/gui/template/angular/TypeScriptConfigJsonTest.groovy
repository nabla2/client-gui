package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Project

class TypeScriptConfigJsonTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.project$.test().values().with {
      assert it.size() == 1
      Project project = it.first()
      assert project
      TypeScriptConfigJson generator = new TypeScriptConfigJson(project)
      assert generator.relPath == 'tsconfig.json'
      generator.source.with {
        assert it.contains("""
        |{ "//" : "@author nabla2.metamodel.generator"
        |, "compilerOptions":
        |  { "target"                         : "es5"
        |  , "module"                         : "commonjs"
        |  , "moduleResolution"               : "node"
        |  , "sourceMap"                      : true
        |  , "emitDecoratorMetadata"          : true
        |  , "experimentalDecorators"         : true
        |  , "lib"                            : ["es2017", "dom"]
        |  , "noImplicitAny"                  : true
        |  , "suppressImplicitAnyIndexErrors" : true
        |  }
        |}
        """.stripMargin().trim())
      }
    }
  }
}
