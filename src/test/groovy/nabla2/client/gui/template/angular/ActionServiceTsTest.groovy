package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Action

class ActionServiceTsTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.action$.test().values().with {
      Action action = it.find {
        it.name.sameAs('【API名】を呼び出す')
      }
      assert action
      ActionServiceTs generator = new ActionServiceTs(action)
      assert generator.relPath == 'action/invoke-rest-api.service.ts'
      generator.source.with {
        assert it.contains(
        """
        |import {Observer, Observable} from 'rxjs'
        |import {Injectable} from '@angular/core'
        |import {Router} from '@angular/router'
        |import {Http, Response, RequestOptions, RequestOptionsArgs} from '@angular/http'
        |import {AppState} from '../main'
        |import {ViewModel, UIEvent, UIEventHub} from '../base.module'
        |import {Action} from '../action.module'
        """.trim().stripMargin())

        assert it.contains(
        """
        |@Injectable()
        |export class InvokeRestApi implements Action<InvokeRestApiArgs, any>{
        """.trim().stripMargin())

        assert it.contains(
        """
        |  constructor(
        |    private http:Http,
        |    private router:Router,
        |    private viewModel:ViewModel<AppState>,
        |    private eventHub:UIEventHub,
        |  ) {} 
        """.trim().stripMargin())

        assert it.contains(
        """
        |  observer(args:InvokeRestApiArgs):Observer<any> {
        |    const invoke = (data:any):void => {
        |      this.http
        |      .get(args[0].url(), args[0].options) 
        |      .map((it:Response) => it.json())
        |      .subscribe(args[0].observer)  
        |    }
        |    return {
        |      next : invoke,
        |      error : () => {},
        |      complete : () => {}
        |    }
        |  } 
        |}
        """.trim().stripMargin())

        assert it.contains(
        """
        |export interface InvokeRestApiArgs {
        |  [0]: { url(): string, options: RequestOptions, observer: Observer<any> },
        |} 
        """.trim().stripMargin())
      }
    }
  }
}
