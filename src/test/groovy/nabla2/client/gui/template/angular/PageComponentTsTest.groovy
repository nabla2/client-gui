package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Page

class PageComponentTsTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.page$.test().values().with {
      Page page = it.find{
        it.featureName.sameAs('単語帳機能') &&
        it.name.sameAs('学習画面')
      }
      assert page
      PageComponentTs generator = new PageComponentTs(page)
      assert generator.relPath == 'app/deck/learn-page.component.ts'
      generator.source.with {
        assert it.contains("""
        |import {Component, OnInit, OnDestroy} from '@angular/core'
        |import {ActivatedRoute, Router} from '@angular/router'
        |import {Http, Response, RequestOptions, Headers} from '@angular/http'
        |import {Observable, Observer, Subject} from 'rxjs'
        |import {AppState} from '../../main'
        |import {UIEventHub, ViewModel, ValidationError} from '../../base.module'
        |import {DeckModuleState} from '../deck.module'
        |import {InvokeRestApi, InvokeRestApiArgs, AddNumberToPageItem, AddNumberToPageItemArgs, SetValueToPageItem, SetValueToPageItemArgs} from '../../action.module'
        |import {WhenPageShowed, WhenPageShowedArgs, WhenPageItemClicked, WhenPageItemClickedArgs, NoFilter, NoFilterArgs} from '../../event.module'
        """.stripMargin().trim())

        assert it.contains("""
        |@Component(
        |{ templateUrl : './learn-page.component.html'
        |})
        |export class LearnPageComponent implements OnInit, OnDestroy {
        |
        |  private state:AppState = null
        |  private path:string = '/deck/learn'
        |  private unsubscribe:Subject<void> = new Subject<void>()
        """.stripMargin().trim())

        assert it.contains("""
        |  constructor(
        |    private route:ActivatedRoute,
        |    private router:Router,
        |    private viewModel:ViewModel<AppState>,
        |    private eventHub:UIEventHub,
        |    private invokeRestApi:InvokeRestApi,
        |    private addNumberToPageItem:AddNumberToPageItem,
        |    private setValueToPageItem:SetValueToPageItem,
        |    private whenPageShowed:WhenPageShowed,
        |    private whenPageItemClicked:WhenPageItemClicked,
        |    private noFilter:NoFilter,
        |  ) {}
        """.stripMargin().trim())

        assert it.contains("""
        |  ngOnInit() {
        |    this.viewModel.model\$.subscribe(m => this.state = m)
        |    this.invokeRestApiWhenPageShowed_1()
        |    this.addNumberToPageItemWhenPageItemClicked_2()
        |    this.invokeRestApiWhenPageItemClicked_3()
        |    this.setValueToPageItemWhenPageItemClicked_4()
        |    this.setValueToPageItemWhenPageItemClicked_5()
        |  }
        """.stripMargin().trim())

        assert it.contains("""
        |  ngOnDestroy() {
        |    this.unsubscribe.next()
        |    this.unsubscribe.complete()
        |  }
        """.stripMargin().trim())

        assert it.contains("""
        |  invokeRestApiWhenPageShowed_1():void {
        """.stripMargin().trim())

        assert it.contains("""
        |    const eventArgs:WhenPageShowedArgs = [
        |    ]
        """.stripMargin().trim())

        assert it.contains("""
        |    const actionArgs:InvokeRestApiArgs = [
        |      (() => {
        |        const paramMap = {
        |        }
        |        return {
        |          requestId: '現在の番号のカード情報を取得',
        |          request: () => { return {
        |            url: `http://localhost:8080/api/deck/\${this.state.deck.flashcard.deckName}/cards/\${this.state.deck.flashcard.currentNumber}/`,
        |            method: 'GET',
        |            headers: new Headers({ 'Content-Type': 'application/json' }),
        |          }}
        """.stripMargin().trim())

        assert it.contains("""
        |        , observer : {
        |            next : (res:Response) => {
        |              const body:any = res.json()
        |              this.viewModel.update('deck', {
        |              flashcard: {
        |                currentNumber: body.card.order,
        |                content: body.card.front,
        |                front: body.card.front,
        |                back: body.card.back,
        |              },
        |            })
        |            },
        """.stripMargin().trim())

        assert it.contains("""
        |            error :this.handleRestServiceError(paramMap),
        |            complete : () => {},
        |          }
        |        }
        |      })(),
        |    ]
        """.stripMargin().trim())

        assert it.contains("""
        |    this.whenPageShowed.observable(eventArgs, this)
        |    .filter(this.noFilter.filter(filterArgs))
        |    .do(() => console.log(`画面が表示されたとき【現在の番号のカード情報を取得】を呼び出す`))
        |    .takeUntil(this.unsubscribe)
        |    .subscribe(this.invokeRestApi.observer(actionArgs))
        |  }
        """.stripMargin().trim())
      }
    }
  }
}
