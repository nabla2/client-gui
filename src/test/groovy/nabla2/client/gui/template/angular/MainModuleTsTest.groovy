package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Project

class MainModuleTsTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.project$.test().values().with {
      assert it.size() == 1
      Project project = it.first()
      assert project
      MainTs generator = new MainTs(project)
      assert generator.relPath == 'main.ts'
      generator.source.with {
        assert it.contains("""
        |import 'core-js/es6'
        |import 'core-js/es7/reflect'
        """.stripMargin().trim())

        assert it.contains("""
        |import {Observable} from 'rxjs'
        |import {Component, NgModule} from '@angular/core'
        |import {platformBrowserDynamic} from '@angular/platform-browser-dynamic'
        |import {BrowserModule} from '@angular/platform-browser'
        |import {RouterModule, Routes} from '@angular/router'
        |import {BaseModule, ViewModel, ValidationError} from './base.module'
        |import {ActionModule} from './action.module'
        |import {EventModule} from './event.module'
        |import {ConfigModule, ConfigModuleState, ConfigModuleInitialState} from './app/config.module'
        |import {DeckModule, DeckModuleState, DeckModuleInitialState} from './app/deck.module'
        |
        |require('zone.js/dist/zone')
        """.stripMargin().trim())

        assert it.contains("""
        |@Component(
        |{ selector : 'app'
        |, template : `
        |  <router-outlet></router-outlet>
        |  `
        |})
        |class AppComponent {}
        """.stripMargin().trim())

        assert it.contains("""
        |const routes:Routes = [
        |]
        """.stripMargin().trim())

        assert it.contains("""
        |export interface AppState {
        |  config : ConfigModuleState
        |  deck : DeckModuleState
        |  validationErrors : ValidationError[]
        |}
        """.stripMargin().trim())

        assert it.contains("""
        |const initialState:AppState = {
        |  config : ConfigModuleInitialState,
        |  deck : DeckModuleInitialState,
        |  validationErrors : [],
        |} 
        """.stripMargin().trim())

        assert it.contains("""
        |@NgModule(
        |{ imports:
        |  [ BrowserModule
        |  , ConfigModule
        |  , DeckModule
        |  , RouterModule.forRoot(routes,{useHash:true})
        |  , BaseModule
        |  , ActionModule
        |  , EventModule
        |  ]
        |, declarations :[AppComponent]
        |, bootstrap    :[AppComponent]
        |})
        """.stripMargin().trim())

        assert it.contains("""
        |class AppModule {
        |  constructor(viewModel:ViewModel<AppState>) {
        |    viewModel.initialize(initialState)
        |  }
        |}
        |
        |platformBrowserDynamic().bootstrapModule(AppModule)
        """.stripMargin().trim())
      }
    }
  }
}
