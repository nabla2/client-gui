package nabla2.client.gui.template.angular

import nabla2.client.gui.GuiEntityLoader
import nabla2.client.gui.model.Project

class IndexHtmlTest extends GroovyTestCase {

  static GuiEntityLoader getLoader() {
    GuiEntityLoader.from(
      Class.getResourceAsStream('/project.xlsx')
    )
  }

  void testGetSource() {
    loader.project$.test().values().with {
      assert it.size() == 1
      Project project = it.first()
      assert project
      IndexHtml generator = new IndexHtml(project)
      assert generator.relPath == 'index.html'
      generator.source.with {

        assert it.contains("""
        |<!DOCTYPE html>
        """.stripMargin().trim())

        assert it.contains("""
        |<html>
        |<head>
        |<base href="/" />
        |<title>単語帳Webアプリ</title>
        |<meta charset="UTF-8" />
        |<meta
        |  name="viewport"
        |  content="width=device-width, initial-scale=1"
        |/>
        |</head>
        """.stripMargin().trim())

        assert it.contains("""
        |<body>
        |  <app>Loading...</app>
        |</body>
        |</html>
        """.stripMargin().trim())
      }
    }
  }
}
