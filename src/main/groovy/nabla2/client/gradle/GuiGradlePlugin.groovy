package nabla2.client.gradle

import org.gradle.api.Project
import nabla2.gradle.BasePlugin
import nabla2.client.gui.gradle.GenerateAngularBasedWebClient
import nabla2.client.gui.gradle.GenerateIonicBasedWebClient
import nabla2.client.gui.gradle.GenerateReactBasedWebClient

/**
 * GUIクライアント自動生成Gradleプラグイン
 *
 * @author nabla2.metamodel.generator
 */
class GuiGradlePlugin extends BasePlugin {

  void apply(Project project) {
    register(
      project,
      GenerateAngularBasedWebClient,
      GenerateAngularBasedWebClient.Settings,
    )
    register(
      project,
      GenerateIonicBasedWebClient,
      GenerateIonicBasedWebClient.Settings,
    )
    register(
      project,
      GenerateReactBasedWebClient,
      GenerateReactBasedWebClient.Settings,
    )
  }
}