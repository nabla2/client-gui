package nabla2.client.gui.gradle

import io.reactivex.Observable
import nabla2.client.gui.model.ActionValidator
import nabla2.client.gui.model.ApiCallValidator
import nabla2.client.gui.model.EventFilterValidator
import nabla2.client.gui.model.EventValidator
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.FeatureValidator
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageBehaviorValidator
import nabla2.client.gui.model.PageItemType
import nabla2.client.gui.model.PageItemTypeValidator
import nabla2.client.gui.model.PageItemValidator
import nabla2.client.gui.model.PageValidator
import nabla2.client.gui.model.Project
import nabla2.client.gui.model.ProjectValidator
import nabla2.client.gui.model.ViewModelItemValidator
import nabla2.client.gui.template.react.ActionModuleTs
import nabla2.client.gui.template.react.ApiCallTs
import nabla2.client.gui.template.react.AppComponentTsx
import nabla2.client.gui.template.react.CoreModuleTs
import nabla2.client.gui.template.react.EventModuleTs
import nabla2.client.gui.template.react.FeatureComponentTsx
import nabla2.client.gui.template.react.FeatureModuleTs
import nabla2.client.gui.template.react.FeatureViewModelTs
import nabla2.client.gui.template.react.IndexHtml
import nabla2.client.gui.template.react.IndexTsx
import nabla2.client.gui.template.react.PackageJson
import nabla2.client.gui.template.react.PageBehaviorTs
import nabla2.client.gui.template.react.PageComponentTsx
import nabla2.client.gui.template.react.TsconfigJson
import nabla2.client.gui.template.react.ViewModelTs
import nabla2.client.gui.template.react.WebpackConfigJs
import nabla2.client.gui.template.react.WidgetComponentCss
import nabla2.client.gui.template.react.WidgetComponentTsx
import nabla2.client.gui.template.react.WidgetModuleTs
import nabla2.excel.ExcelWorkbookLoader
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.GeneratorOption
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.Table
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
/**
 * Webクライアント(React)を生成
 *
 * @author nabla2.metamodel.generator
 */
class GenerateReactBasedWebClient extends DefaultTask {

  @InputFiles FileCollection workbooks
  @InputFiles @Optional FileCollection referringTo
  @OutputDirectory @Optional File coreModuleTsDir
  @OutputDirectory @Optional File indexTsxDir
  @OutputDirectory @Optional File appComponentTsxDir
  @OutputDirectory @Optional File widgetModuleTsDir
  @OutputDirectory @Optional File actionModuleTsDir
  @OutputDirectory @Optional File eventModuleTsDir
  @OutputDirectory @Optional File apiCallTsDir
  @OutputDirectory @Optional File widgetComponentTsxDir
  @OutputDirectory @Optional File widgetComponentCssDir
  @OutputDirectory @Optional File featureModuleTsDir
  @OutputDirectory @Optional File featureComponentTsxDir
  @OutputDirectory @Optional File pageComponentTsxDir
  @OutputDirectory @Optional File pageBehaviorTsDir
  @OutputDirectory @Optional File viewModelTsDir
  @OutputDirectory @Optional File featureViewModelTsDir
  File baseDir
  @OutputFile @Optional File errorReport
  String group = 'laplacian generator'
  String description = 'Webクライアント(React)を生成'

  @TaskAction
  void exec() {
    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
      ExcelWorkbookLoader.from(workbook).table$
    }.with { _table$ ->
      errorReport.text = ''
      Throwable error = null
      Map<String, Map<String, String>> commonProps = ModelCommonProperty.from(_table$).toList().blockingGet().inject([:]) { acc, prop ->
        Map<String,String> bucket = acc[prop.entityName.get()]
        if (prop.value.empty) return acc
        if (!bucket) {
          bucket = [:]
          acc[prop.entityName.get()] = bucket
        }
        bucket[prop.propertyName.get()] = prop.value.get()
        acc
      }
      def table$ = _table$.map{ table ->
        table.copyWith(globalMetadata: commonProps)
      }
      validate(table$)
      List<GeneratorOption> optionList = GeneratorOption.from(table$).filter{
        it.optionType.targetTask.identifier.sameAs('GenerateReactBasedWebClient')
      }.toList().blockingGet()
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new PackageJson(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new TsconfigJson(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new WebpackConfigJs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new IndexHtml(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new CoreModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new IndexTsx(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new AppComponentTsx(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new WidgetModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ActionModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new EventModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ApiCallTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      PageItemType.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new WidgetComponentTsx(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      PageItemType.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new WidgetComponentCss(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new FeatureModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new FeatureComponentTsx(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Page.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new PageComponentTsx(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Page.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new PageBehaviorTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ViewModelTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new FeatureViewModelTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
    }
  }

  void validate(Observable<Table> table$) {
    Throwable error
    println 'Checking model files...'
    Observable.fromArray(
      new ProjectValidator().validate(table$),
      new FeatureValidator().validate(table$),
      new ViewModelItemValidator().validate(table$),
      new PageValidator().validate(table$),
      new PageItemValidator().validate(table$),
      new PageItemTypeValidator().validate(table$),
      new PageBehaviorValidator().validate(table$),
      new EventValidator().validate(table$),
      new EventFilterValidator().validate(table$),
      new ActionValidator().validate(table$),
      new ApiCallValidator().validate(table$),
    )
    .flatMap {it}
    .doOnNext{ println it; errorReport << it }
    .doOnError{ it.printStackTrace(); error = it }
    .onErrorResumeNext(Observable.empty())
    .toList()
    .blockingGet().with {
      if (!it.empty) throw new GradleException(
        "There are ${it.size()} errors in model files. see details in ${this.errorReport.path}"
      )
      if (error) throw new GradleException(
        "An error occurred while validating the model files.", error
      )
    }
  }

  static class Settings extends TaskSettings<GenerateReactBasedWebClient> {
    FileCollection workbooks
    FileCollection referringTo
    File baseDir
    File errorReport
    void setWorkbook(File workbook) {
      workbooks = project.files(workbook)
    }
    @Override
    void setup(GenerateReactBasedWebClient task) {
      File baseDir = this.baseDir ?: new File('./')
      task.workbooks = workbooks
      task.referringTo = referringTo ?: project.files([])
      task.baseDir = baseDir
      task.errorReport = errorReport ?: new File('./build/laplacian/errors.md')
      task.coreModuleTsDir = new File(baseDir, 'src')
      task.indexTsxDir = new File(baseDir, 'src')
      task.appComponentTsxDir = new File(baseDir, 'src')
      task.widgetModuleTsDir = new File(baseDir, 'src')
      task.actionModuleTsDir = new File(baseDir, 'src')
      task.eventModuleTsDir = new File(baseDir, 'src')
      task.apiCallTsDir = new File(baseDir, 'src')
      task.widgetComponentTsxDir = new File(baseDir, 'src')
      task.widgetComponentCssDir = new File(baseDir, 'src')
      task.featureModuleTsDir = new File(baseDir, 'src')
      task.featureComponentTsxDir = new File(baseDir, 'src')
      task.pageComponentTsxDir = new File(baseDir, 'src')
      task.pageBehaviorTsDir = new File(baseDir, 'src')
      task.viewModelTsDir = new File(baseDir, 'src')
      task.featureViewModelTsDir = new File(baseDir, 'src')
    }
  }
}