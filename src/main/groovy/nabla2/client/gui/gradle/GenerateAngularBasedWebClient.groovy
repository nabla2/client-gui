package nabla2.client.gui.gradle

import io.reactivex.Observable
import nabla2.client.gui.model.Action
import nabla2.client.gui.model.ActionValidator
import nabla2.client.gui.model.ApiCallValidator
import nabla2.client.gui.model.Event
import nabla2.client.gui.model.EventFilter
import nabla2.client.gui.model.EventFilterValidator
import nabla2.client.gui.model.EventValidator
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.FeatureValidator
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageBehaviorValidator
import nabla2.client.gui.model.PageItemType
import nabla2.client.gui.model.PageItemTypeValidator
import nabla2.client.gui.model.PageItemValidator
import nabla2.client.gui.model.PageValidator
import nabla2.client.gui.model.Project
import nabla2.client.gui.model.ProjectValidator
import nabla2.client.gui.model.ViewModelItemValidator
import nabla2.client.gui.template.angular.ActionModuleTs
import nabla2.client.gui.template.angular.ActionServiceTs
import nabla2.client.gui.template.angular.ApplicationComponentTs
import nabla2.client.gui.template.angular.ApplicationModuleTs
import nabla2.client.gui.template.angular.ApplicationStateTs
import nabla2.client.gui.template.angular.ApplicationStyleScss
import nabla2.client.gui.template.angular.ApplicationTemplateHtml
import nabla2.client.gui.template.angular.BaseModuleTs
import nabla2.client.gui.template.angular.BasePageTs
import nabla2.client.gui.template.angular.BaseWidgetTs
import nabla2.client.gui.template.angular.EventFilterServiceTs
import nabla2.client.gui.template.angular.EventModuleTs
import nabla2.client.gui.template.angular.EventServiceTs
import nabla2.client.gui.template.angular.FeatureCommonAreaStyleScss
import nabla2.client.gui.template.angular.FeatureCommonAreaTemplateHtml
import nabla2.client.gui.template.angular.FeatureCommonAreaTs
import nabla2.client.gui.template.angular.FeatureModuleStateTs
import nabla2.client.gui.template.angular.FeatureModuleTs
import nabla2.client.gui.template.angular.IndexHtml
import nabla2.client.gui.template.angular.MainTs
import nabla2.client.gui.template.angular.PageComponentTs
import nabla2.client.gui.template.angular.PageStyleScss
import nabla2.client.gui.template.angular.PageTemplateHtml
import nabla2.client.gui.template.angular.WidgetComponentTs
import nabla2.client.gui.template.angular.WidgetModuleTs
import nabla2.client.gui.template.angular.WidgetStyleScss
import nabla2.client.gui.template.angular.WidgetTemplateHtml
import nabla2.excel.ExcelWorkbookLoader
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.GeneratorOption
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.Table
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
/**
 * Webクライアント(Angular)を生成
 *
 * @author nabla2.metamodel.generator
 */
class GenerateAngularBasedWebClient extends DefaultTask {

  @InputFiles FileCollection workbooks
  @InputFiles @Optional FileCollection referringTo
  @OutputDirectory @Optional File baseModuleTsDir
  @OutputDirectory @Optional File indexHtmlDir
  @OutputDirectory @Optional File mainTsDir
  @OutputDirectory @Optional File applicationModuleTsDir
  @OutputDirectory @Optional File applicationComponentTsDir
  @OutputDirectory @Optional File applicationTemplateHtmlDir
  @OutputDirectory @Optional File applicationStyleScssDir
  @OutputDirectory @Optional File applicationStateTsDir
  @OutputDirectory @Optional File widgetModuleTsDir
  @OutputDirectory @Optional File baseWidgetTsDir
  @OutputDirectory @Optional File widgetComponentTsDir
  @OutputDirectory @Optional File widgetTemplateHtmlDir
  @OutputDirectory @Optional File widgetStyleScssDir
  @OutputDirectory @Optional File actionModuleTsDir
  @OutputDirectory @Optional File actionServiceTsDir
  @OutputDirectory @Optional File eventModuleTsDir
  @OutputDirectory @Optional File eventServiceTsDir
  @OutputDirectory @Optional File eventFilterServiceTsDir
  @OutputDirectory @Optional File featureModuleTsDir
  @OutputDirectory @Optional File featureModuleStateTsDir
  @OutputDirectory @Optional File featureCommonAreaTsDir
  @OutputDirectory @Optional File featureCommonAreaTemplateHtmlDir
  @OutputDirectory @Optional File featureCommonAreaStyleScssDir
  @OutputDirectory @Optional File pageComponentTsDir
  @OutputDirectory @Optional File basePageTsDir
  @OutputDirectory @Optional File pageTemplateHtmlDir
  @OutputDirectory @Optional File pageStyleScssDir
  File baseDir
  @OutputFile @Optional File errorReport
  String group = 'laplacian generator'
  String description = 'Webクライアント(Angular)を生成'

  @TaskAction
  void exec() {
    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
      ExcelWorkbookLoader.from(workbook).table$
    }.with { _table$ ->
      errorReport.text = ''
      Throwable error = null
      Map<String, Map<String, String>> commonProps = ModelCommonProperty.from(_table$).toList().blockingGet().inject([:]) { acc, prop ->
        Map<String,String> bucket = acc[prop.entityName.get()]
        if (prop.value.empty) return acc
        if (!bucket) {
          bucket = [:]
          acc[prop.entityName.get()] = bucket
        }
        bucket[prop.propertyName.get()] = prop.value.get()
        acc
      }
      def table$ = _table$.map{ table ->
        table.copyWith(globalMetadata: commonProps)
      }
      validate(table$)
      List<GeneratorOption> optionList = GeneratorOption.from(table$).filter{
        it.optionType.targetTask.identifier.sameAs('GenerateAngularBasedWebClient')
      }.toList().blockingGet()
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new BaseModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new IndexHtml(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new MainTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ApplicationModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ApplicationComponentTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ApplicationTemplateHtml(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ApplicationStyleScss(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ApplicationStateTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new WidgetModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new BaseWidgetTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      PageItemType.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new WidgetComponentTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      PageItemType.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new WidgetTemplateHtml(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      PageItemType.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new WidgetStyleScss(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ActionModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Action.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ActionServiceTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new EventModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Event.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new EventServiceTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      EventFilter.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new EventFilterServiceTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new FeatureModuleTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new FeatureModuleStateTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new FeatureCommonAreaTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new FeatureCommonAreaTemplateHtml(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new FeatureCommonAreaStyleScss(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Page.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new PageComponentTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new BasePageTs(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Page.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new PageTemplateHtml(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Page.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new PageStyleScss(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
    }
  }

  void validate(Observable<Table> table$) {
    Throwable error
    println 'Checking model files...'
    Observable.fromArray(
      new ProjectValidator().validate(table$),
      new FeatureValidator().validate(table$),
      new ViewModelItemValidator().validate(table$),
      new PageValidator().validate(table$),
      new PageItemValidator().validate(table$),
      new PageItemTypeValidator().validate(table$),
      new PageBehaviorValidator().validate(table$),
      new EventValidator().validate(table$),
      new EventFilterValidator().validate(table$),
      new ActionValidator().validate(table$),
      new ApiCallValidator().validate(table$),
    )
    .flatMap {it}
    .doOnNext{ println it; errorReport << it }
    .doOnError{ it.printStackTrace(); error = it }
    .onErrorResumeNext(Observable.empty())
    .toList()
    .blockingGet().with {
      if (!it.empty) throw new GradleException(
        "There are ${it.size()} errors in model files. see details in ${this.errorReport.path}"
      )
      if (error) throw new GradleException(
        "An error occurred while validating the model files.", error
      )
    }
  }

  static class Settings extends TaskSettings<GenerateAngularBasedWebClient> {
    FileCollection workbooks
    FileCollection referringTo
    File baseDir
    File errorReport
    void setWorkbook(File workbook) {
      workbooks = project.files(workbook)
    }
    @Override
    void setup(GenerateAngularBasedWebClient task) {
      File baseDir = this.baseDir ?: new File('./')
      task.workbooks = workbooks
      task.referringTo = referringTo ?: project.files([])
      task.baseDir = baseDir
      task.errorReport = errorReport ?: new File('./build/laplacian/errors.md')
      task.baseModuleTsDir = new File(baseDir, 'src')
      task.indexHtmlDir = new File(baseDir, 'src')
      task.mainTsDir = new File(baseDir, 'src')
      task.applicationModuleTsDir = new File(baseDir, 'src')
      task.applicationComponentTsDir = new File(baseDir, 'src')
      task.applicationTemplateHtmlDir = new File(baseDir, 'src')
      task.applicationStyleScssDir = new File(baseDir, 'src')
      task.applicationStateTsDir = new File(baseDir, 'src')
      task.widgetModuleTsDir = new File(baseDir, 'src')
      task.baseWidgetTsDir = new File(baseDir, 'src')
      task.widgetComponentTsDir = new File(baseDir, 'src')
      task.widgetTemplateHtmlDir = new File(baseDir, 'src')
      task.widgetStyleScssDir = new File(baseDir, 'src')
      task.actionModuleTsDir = new File(baseDir, 'src')
      task.actionServiceTsDir = new File(baseDir, 'src')
      task.eventModuleTsDir = new File(baseDir, 'src')
      task.eventServiceTsDir = new File(baseDir, 'src')
      task.eventFilterServiceTsDir = new File(baseDir, 'src')
      task.featureModuleTsDir = new File(baseDir, 'src')
      task.featureModuleStateTsDir = new File(baseDir, 'src')
      task.featureCommonAreaTsDir = new File(baseDir, 'src')
      task.featureCommonAreaTemplateHtmlDir = new File(baseDir, 'src')
      task.featureCommonAreaStyleScssDir = new File(baseDir, 'src')
      task.pageComponentTsDir = new File(baseDir, 'src')
      task.basePageTsDir = new File(baseDir, 'src')
      task.pageTemplateHtmlDir = new File(baseDir, 'src')
      task.pageStyleScssDir = new File(baseDir, 'src')
    }
  }
}