package nabla2.client.gui.gradle

import nabla2.metamodel.model.GeneratorOption
import groovy.transform.Canonical
import groovy.transform.Memoized

/**
 * Webクライアント(React)を生成オプション定義
 *
 * @author nabla2.metamodel.generator
 */
@Canonical
class GenerateReactBasedWebClientOptionSet {

  List<GeneratorOption> allOptions
  Map<String, ?> properties

  /**
   * 環境フラグを表すビルドプロパティを取得する。
   */
  String getEnv() {
    properties.find{it.key.toLowerCase() == 'env'}.with {
      (it == null || it.value.toString().trim().empty) ? 'ANY' : it.value.toString().toUpperCase()
    }
  }

}