package nabla2.client.gui.gradle

import io.reactivex.Observable
import nabla2.client.gui.model.ActionValidator
import nabla2.client.gui.model.ApiCallValidator
import nabla2.client.gui.model.EventFilterValidator
import nabla2.client.gui.model.EventValidator
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.FeatureValidator
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageBehaviorValidator
import nabla2.client.gui.model.PageItemType
import nabla2.client.gui.model.PageItemTypeValidator
import nabla2.client.gui.model.PageItemValidator
import nabla2.client.gui.model.PageValidator
import nabla2.client.gui.model.Project
import nabla2.client.gui.model.ProjectValidator
import nabla2.client.gui.model.ViewModelItemValidator
import nabla2.client.gui.template.ionic.ActionModuleTs
import nabla2.client.gui.template.ionic.ApplicationComponentTs
import nabla2.client.gui.template.ionic.ApplicationModuleTs
import nabla2.client.gui.template.ionic.ApplicationTemplateHtml
import nabla2.client.gui.template.ionic.BasePageTs
import nabla2.client.gui.template.ionic.BaseWidgetTs
import nabla2.client.gui.template.ionic.EnvironmentTs
import nabla2.client.gui.template.ionic.EventModuleTs
import nabla2.client.gui.template.ionic.FeatureModuleTs
import nabla2.client.gui.template.ionic.FeatureViewModelTs
import nabla2.client.gui.template.ionic.GlobalScss
import nabla2.client.gui.template.ionic.IndexHtml
import nabla2.client.gui.template.ionic.MainTs
import nabla2.client.gui.template.ionic.MaterialModuleTs
import nabla2.client.gui.template.ionic.PackageJson
import nabla2.client.gui.template.ionic.PageActionTs
import nabla2.client.gui.template.ionic.PageApiCallTs
import nabla2.client.gui.template.ionic.PageComponentTs
import nabla2.client.gui.template.ionic.PageModuleTs
import nabla2.client.gui.template.ionic.PageTemplateHtml
import nabla2.client.gui.template.ionic.RestClientServiceTs
import nabla2.client.gui.template.ionic.TypeScriptConfigJson
import nabla2.client.gui.template.ionic.ViewModelTs
import nabla2.client.gui.template.ionic.WidgetComponentTs
import nabla2.client.gui.template.ionic.WidgetModuleTs
import nabla2.client.gui.template.ionic.WidgetTemplateHtml
import nabla2.excel.ExcelWorkbookLoader
import nabla2.generator.template.ExternalResourceDownloader
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.ExternalResource
import nabla2.metamodel.model.GeneratorOption
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.Table
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
/**
 * Webクライアント(Ionic)を生成
 *
 * @author nabla2.metamodel.generator
 */
class GenerateIonicBasedWebClient extends DefaultTask {

  @InputFiles FileCollection workbooks
  @InputFiles @Optional FileCollection referringTo
  @OutputDirectory @Optional File environmentTsDir
  @OutputDirectory @Optional File mainTsDir
  @OutputDirectory @Optional File indexHtmlDir
  @OutputDirectory @Optional File applicationModuleTsDir
  @OutputDirectory @Optional File applicationComponentTsDir
  @OutputDirectory @Optional File applicationTemplateHtmlDir
  @OutputDirectory @Optional File globalScssDir
  @OutputDirectory @Optional File basePageTsDir
  @OutputDirectory @Optional File featureModuleTsDir
  @OutputDirectory @Optional File pageComponentTsDir
  @OutputDirectory @Optional File pageModuleTsDir
  @OutputDirectory @Optional File pageActionTsDir
  @OutputDirectory @Optional File pageApiCallTsDir
  @OutputDirectory @Optional File pageTemplateHtmlDir
  @OutputDirectory @Optional File widgetModuleTsDir
  @OutputDirectory @Optional File materialModuleTsDir
  @OutputDirectory @Optional File widgetComponentTsDir
  @OutputDirectory @Optional File widgetTemplateHtmlDir
  @OutputDirectory @Optional File actionModuleTsDir
  @OutputDirectory @Optional File eventModuleTsDir
  @OutputDirectory @Optional File viewModelTsDir
  @OutputDirectory @Optional File featureViewModelTsDir
  @OutputDirectory @Optional File restClientServiceTsDir
  @OutputDirectory @Optional File baseWidgetTsDir
  File baseDir
  @OutputFile @Optional File errorReport
  String group = 'laplacian generator'
  String description = 'Webクライアント(Ionic)を生成'

  @TaskAction
  void exec() {
    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
      ExcelWorkbookLoader.from(workbook).table$
    }.with { _table$ ->
      errorReport.text = ''
      Throwable error = null
      Map<String, Map<String, String>> commonProps = ModelCommonProperty.from(_table$).toList().blockingGet().inject([:]) { acc, prop ->
        Map<String,String> bucket = acc[prop.entityName.get()]
        if (prop.value.empty) return acc
        if (!bucket) {
          bucket = [:]
          acc[prop.entityName.get()] = bucket
        }
        bucket[prop.propertyName.get()] = prop.value.get()
        acc
      }
      def table$ = _table$.map{ table ->
        table.copyWith(globalMetadata: commonProps)
      }
      validate(table$)
      List<GeneratorOption> optionList = GeneratorOption.from(table$).filter{
        it.optionType.targetTask.identifier.sameAs('GenerateIonicBasedWebClient')
      }.toList().blockingGet()
      ExternalResource.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new ExternalResourceDownloader(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new PackageJson(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new EnvironmentTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new MainTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new IndexHtml(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new ApplicationModuleTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new ApplicationComponentTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new ApplicationTemplateHtml(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new GlobalScss(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new BasePageTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new FeatureModuleTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Page.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new PageComponentTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Page.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new PageModuleTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Page.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new PageActionTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Page.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new PageApiCallTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Page.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new PageTemplateHtml(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new WidgetModuleTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new MaterialModuleTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      PageItemType.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new WidgetComponentTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      PageItemType.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new WidgetTemplateHtml(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new ActionModuleTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new EventModuleTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new ViewModelTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Feature.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new FeatureViewModelTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new RestClientServiceTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new BaseWidgetTs(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateIonicBasedWebClientOptionSet(optionList, project.properties)
        new TypeScriptConfigJson(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateIonicBasedWebClientOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
    }
  }

  void validate(Observable<Table> table$) {
    Throwable error
    println 'Checking model files...'
    Observable.fromArray(
      new ProjectValidator().validate(table$),
      new FeatureValidator().validate(table$),
      new ViewModelItemValidator().validate(table$),
      new PageValidator().validate(table$),
      new PageItemValidator().validate(table$),
      new PageItemTypeValidator().validate(table$),
      new PageBehaviorValidator().validate(table$),
      new EventValidator().validate(table$),
      new EventFilterValidator().validate(table$),
      new ActionValidator().validate(table$),
      new ApiCallValidator().validate(table$),
    )
    .flatMap {it}
    .doOnNext{ println it; errorReport << it }
    .doOnError{ it.printStackTrace(); error = it }
    .onErrorResumeNext(Observable.empty())
    .toList()
    .blockingGet().with {
      if (!it.empty) throw new GradleException(
        "There are ${it.size()} errors in model files. see details in ${this.errorReport.path}"
      )
      if (error) throw new GradleException(
        "An error occurred while validating the model files.", error
      )
    }
  }

  static class Settings extends TaskSettings<GenerateIonicBasedWebClient> {
    FileCollection workbooks
    FileCollection referringTo
    File baseDir
    File errorReport
    void setWorkbook(File workbook) {
      workbooks = project.files(workbook)
    }
    @Override
    void setup(GenerateIonicBasedWebClient task) {
      File baseDir = this.baseDir ?: new File('./')
      task.workbooks = workbooks
      task.referringTo = referringTo ?: project.files([])
      task.baseDir = baseDir
      task.errorReport = errorReport ?: new File('./build/laplacian/errors.md')
      task.environmentTsDir = new File(baseDir, 'src')
      task.mainTsDir = new File(baseDir, 'src')
      task.indexHtmlDir = new File(baseDir, 'src')
      task.applicationModuleTsDir = new File(baseDir, 'src')
      task.applicationComponentTsDir = new File(baseDir, 'src')
      task.applicationTemplateHtmlDir = new File(baseDir, 'src')
      task.globalScssDir = new File(baseDir, 'src')
      task.basePageTsDir = new File(baseDir, 'src')
      task.featureModuleTsDir = new File(baseDir, 'src')
      task.pageComponentTsDir = new File(baseDir, 'src')
      task.pageModuleTsDir = new File(baseDir, 'src')
      task.pageActionTsDir = new File(baseDir, 'src')
      task.pageApiCallTsDir = new File(baseDir, 'src')
      task.pageTemplateHtmlDir = new File(baseDir, 'src')
      task.widgetModuleTsDir = new File(baseDir, 'src')
      task.materialModuleTsDir = new File(baseDir, 'src')
      task.widgetComponentTsDir = new File(baseDir, 'src')
      task.widgetTemplateHtmlDir = new File(baseDir, 'src')
      task.actionModuleTsDir = new File(baseDir, 'src')
      task.eventModuleTsDir = new File(baseDir, 'src')
      task.viewModelTsDir = new File(baseDir, 'src')
      task.featureViewModelTsDir = new File(baseDir, 'src')
      task.restClientServiceTsDir = new File(baseDir, 'src')
      task.baseWidgetTsDir = new File(baseDir, 'src')
    }
  }
}