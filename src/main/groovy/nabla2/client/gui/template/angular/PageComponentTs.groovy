package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Action
import nabla2.client.gui.model.Event
import nabla2.client.gui.model.EventFilter
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageBehavior
import nabla2.client.gui.model.PageItem
import nabla2.generator.TextFileTemplate

@Canonical
class PageComponentTs implements TextFileTemplate {

  Page page

  @Override
  String getSource() {
  """\
  |import {Component, OnInit, OnDestroy} from '@angular/core'
  |import {ActivatedRoute, Router} from '@angular/router'
  |import {Http, Response, RequestOptions, Headers} from '@angular/http'
  |import {Observable, Observer, Subject} from 'rxjs'
  |import {AppState} from '../../app-state'
  |import {UIEventHub, ViewModel, ValidationError} from '../../base.module'
  |import {$moduleStateName} from '${moduleStatePath}'
  |import {${actionClassNames.unique().join(', ')}} from '../../action/action.module'
  |import {${eventClassNames.unique().join(', ')}} from '../../event/event.module'
  |import {BasePage} from '../base-page'
  |
  |/**
  | * ${page.name}ページコンポーネント
  | * ${regenerationMark}
  | */ 
  |@Component({
  |  templateUrl : './${pageId}-page.component.html',
  |  styleUrls: ['./${pageId}-page.component.scss']
  |})
  |export class ${page.identifier.upperCamelized.get()}PageComponent extends BasePage implements OnInit, OnDestroy {
  |
  |  /** アプリケーションステート */
  |  private state:AppState = null
  |  /** URLハッシュパス */
  |  private path:string = '/${featureId}/${pageId}'
  |
  |  /**
  |   * コンストラクタ
  |   */
  |  constructor(
  |    router:Router,
  |    viewModel:ViewModel<AppState>,
  |    eventHub:UIEventHub,
  |    private route:ActivatedRoute,
  ${(actionClassNames + eventClassNames).collect{"""
  |    private ${it[0].toLowerCase()}${it.substring(1)}:${it},
  """.trim()}.join('\n')}
  |  ) {
  |    super(router, viewModel, eventHub)
  |  }
  |
  |  /**
  |   * コンポーネント初期化処理
  |   */
  |  ngOnInit() {
  ${pathParamNames.with {it.empty? '' : """
  |    // --- URLハッシュパラメータをバインド ---
  |    const pathParams = {...this.route.parent.snapshot.params, ...this.route.snapshot.params}
  ${it.collect{ paramName ->
     PageItem bound= boundedPageItemFor(paramName)
  """
  |    this.viewModel.update('${itemPathOf(bound)}', pathParams['${bound.identifier.lowerCamelized.get()}']) // ${bound.name}
  """.trim()}.join('\n')}
  """}}
  |
  |    // --- アプリケーションステートを監視 ---
  |    this.viewModel.model\$.subscribe(m => this.state = m)
  |
  |    // --- イベントストリームを監視 ---
  ${pageEvents.entrySet().withIndex().collect {entry, index ->
     List<PageBehavior> behaviors = entry.value
      String eventDesc = entry.key
      String eventName = behaviors.first().event.identifier.lowerCamelized.get()
      """
  |    // ${index+1}.${eventDesc}
  ${behaviors.collect{"""\
  |    // -> ${it.filterCondition.sameAs('NO_FILTER') ? '' : it.filterCondition}${it.actionDesc}
  """.trim()}.join('\n')}
  |    this.${eventName}_${index+1}()
  |
  """.trim()}.join('\n')}
  |  }
  |
  ${pageEvents.entrySet().withIndex().collect { eachEvent, index ->
    String eventDesc = eachEvent.key
    List<PageBehavior> behaviors = eachEvent.value
    Event event = behaviors.first().event
    String eventName = event.identifier.lowerCamelized.get()
  """
  |  /**
  |   * ${index+1}.${eventDesc}
  ${behaviors.collect{"""\
  |   * -> ${it.filterCondition.sameAs('NO_FILTER') ? '' : it.filterCondition}${it.actionDesc}
  """.trim()}.join('\n')}
  |   */
  |  ${eventName}_${index+1}():void {
  |    // ${eventDesc}
  |    const observable = this.${eventName}.observable(
  ${event.name.captureWithName(eventDesc).collect{ entry ->
    String name = entry.key
    String value = entry.value
  """
  |
  """.trim().replace('\n', '\n|      ')}.join('\n')}
  |    ).takeUntil(this.unsubscribe)
  |
  ${behaviors.collect{ behavior ->
    EventFilter filter = behavior.filterCondition.sameAs('NO_FILTER') ? null : behavior.eventFilter
    String filterDesc = filter ? behavior.filterCondition : ''
    String filterName = filter ? filter.identifier.lowerCamelized.get() : ''
    Action action = behavior.action
    String actionDesc = behavior.actionDesc.get()
    String actionName = action.identifier.lowerCamelized.get()
    List<Map.Entry<String, String>> actionParams = action.name.captureWithName(actionDesc)
    List<Map.Entry<String, String>> requiredActionParams = actionParams.findAll{!it.key.contains(':')}
    List<Map.Entry<String, String>> optionalActionParams = actionParams.findAll{it.key.contains(':') && it.value != null}
  """\
  |    // ${filterDesc}${actionDesc}
  |    observable
  ${!filter ? '' : """
  |    .filter(this.${filterName}.filter(
  ${filter.name.captureWithName(filterDesc).collect{ entry ->
    String name = entry.key
    String value = entry.value
  """
  |
  """.trim().replace('\n', '\n|      ')}.join('\n')}
  |    ))
  """}
  |    .do(() => console.log(`${page.name}#${index+1}:${eventDesc}=>${filterDesc}${actionDesc}`))
  |    .subscribe(this.${actionName}.observer(
  ${requiredActionParams.collect{ entry ->
    String name = entry.key
    String value = entry.value
  """
  |
  """.trim().replace('\n', '\n|      ')}.join('\n')}
  ${optionalActionParams.with { params -> params.empty ? '' : """
  |      {${params.collect{it.key.tokenize(':')[1..-1].join(':') + ':' + (it.value == null ? 'null' : it.value.tokenize(':')[1..-1].join(':'))}.join(', ')}}
  """}}
  |    ))
  """.trim()}.join('\n')}
  |  }
  """.trim()}.join('\n')}
  |}
  """.replaceAll(/\n\s*\n/, '\n').stripMargin().trim()
  }

  @Memoized
  static String itemPathOf(PageItem item) {
    PageItem i = item
    List<String> segments = [item.identifier.lowerCamelized.get()]
    while (i.parent) {
      i = i.parent
      segments.add(0, i.identifier.lowerCamelized.get())
    }
    segments.add(0, item.page.feature.identifier.lowerCamelized.get())
    segments.join('.')
  }

  private static String actionClassNameOf(PageBehavior behavior) {
    behavior.action.identifier.upperCamelized.get()
  }

  private static String eventClassNameOf(PageBehavior behavior) {
    behavior.event.identifier.upperCamelized.get()
  }

  private static String filterClassNameOf(PageBehavior behavior) {
    behavior.eventFilter.identifier.upperCamelized.get()
  }

  PageItem boundedPageItemFor(String paramName) {
    page.feature.pages
        .collectMany{ it.pageItems }
        .find{ item -> item.name.sameAs(paramName) }
        .with { PageItem item ->
          if (!item) throw new IllegalStateException(
            "Unresolved param ${paramName} in the base path of the feature: ${page.feature.name.get()}"
          )
          item
        }
  }

  List<String> getPathParamNames() {
    page.feature.basePath.embededParamNames + page.path.embededParamNames
  }

  @Memoized
  Map<String, List<PageBehavior>> getPageEvents() {
    page.pageBehaviors.groupBy{ it.eventDesc.get() }
  }

  @Memoized
  private String nameOf(PageBehavior behavior) {
    int index = page.pageBehaviors.indexOf(behavior)
    String eventName  = behavior.event.identifier.upperCamelized.get()
    "${eventName}_${index + 1}"
  }

  @Memoized
  private getModuleStateName() {
    "${page.feature.identifier.upperCamelized.get()}ModuleState"
  }

  @Memoized
  private getModulePath() {
    "../${page.feature.identifier.lowerCamelized.get()}.module"
  }

  @Memoized
  private getModuleStatePath() {
    "../${page.feature.identifier.lowerCamelized.get()}-module-state"
  }

  @Memoized
  private List<String> getEventClassNames() {
    ( page.pageBehaviors.collect{it.event.identifier.upperCamelized.get()} +
      page.pageBehaviors.collect{it.eventFilter.identifier.upperCamelized.get()}
    ).unique()
  }

  @Memoized
  private List<String> getActionClassNames() {
    page.pageBehaviors.collect{it.action.identifier.upperCamelized.get()}.unique()
  }

  @Override
  String getRelPath() {
    getComponentPathOf(page) + '.ts'
  }

  @Memoized
  String getPageId() {
    page.identifier.dasherized.get()
  }

  @Memoized
  String getFeatureId() {
    page.feature.identifier.dasherized.get()
  }

  @Memoized
  String getComponentName() {
    "${pageId}-page"
  }

  @Memoized
  String getComponentPathOf(Page page) {
    "app/${featureId}/${componentName}/${componentName}.component"
  }
}
