package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.PageItemType
import nabla2.generator.TextFileTemplate

@Canonical
class WidgetTemplateHtml implements TextFileTemplate {

  PageItemType itemType

  @Override
  String getSource() {
  """\
  |<!--
  | ${itemType.name.literal.get().replaceAll(/\s+/, '').replaceAll(/【[^【】]*】/, '')}テンプレートHTML
  |
  | ${regenerationMark}
  |-->
  |${template}
  """.trim().stripMargin()
  }

  private String getTemplate() {
    itemType.template
            .getValue()
            .map{ it.replaceAll('\n', '\n|') }
            .orElse("<div>&lt;${selector}&gt; is not implemeted!!!</div>")
  }

  private String getSelector() {
    'app-' + itemType.identifier.dasherized.get()
  }

  @Override
  String getRelPath() {
    String widgetName = itemType.identifier.dasherized.get()
    "app/${itemType.componentPath.get()}/${widgetName}/${widgetName}.component.html"
  }
}
