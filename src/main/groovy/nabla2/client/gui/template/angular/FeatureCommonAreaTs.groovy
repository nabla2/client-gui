package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Feature
import nabla2.generator.TextFileTemplate

@Canonical
class FeatureCommonAreaTs implements TextFileTemplate {

  Feature feature

  @Override
  String getSource() {
  """\
  |import {Component, NgModule} from '@angular/core'
  |import {CommonModule as NgCommonModule} from '@angular/common'
  |import {RouterModule, Routes} from '@angular/router'
  |import {HttpModule} from '@angular/http'
  |
  |/**
  | * ${feature.name}画面共通部
  | * ${regenerationMark}
  | */
  |@Component({
  |  templateUrl: './common-area.component.html',
  |  styleUrls: ['./common-area.component.scss']
  |}) 
  |export class CommonArea {}
  """.stripMargin().trim()
  }

  String getFeatureName() {
    feature.identifier.dasherized.get()
  }

  @Override
  String getRelPath() {
    "app/${featureName}/common-area/common-area.component.ts"
  }
}
