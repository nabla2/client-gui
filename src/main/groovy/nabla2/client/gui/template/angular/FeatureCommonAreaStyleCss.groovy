package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Feature
import nabla2.generator.TextFileTemplate

@Canonical
class FeatureCommonAreaStyleCss implements TextFileTemplate {

  Feature feature

  @Override
  String getSource() {
  """\
  |/**
  | * ${feature.name}スタイル定義
  | * ${regenerationMark}
  | */
  """.stripMargin().trim()
  }

  String getFeatureName() {
    feature.identifier.dasherized.get()
  }

  @Override
  String getRelPath() {
    "app/${featureName}/common-area/common-area.component.css"
  }
}
