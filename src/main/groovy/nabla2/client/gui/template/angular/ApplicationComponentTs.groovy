package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.PageItem
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ApplicationComponentTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import { Component } from '@angular/core';
  |
  |@Component({
  |  selector: 'app-root',
  |  templateUrl: './app.component.html',
  |  styleUrls: ['./app.component.scss']
  |})
  |export class AppComponent {}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'app/app.component.ts'
  }
}
