package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Feature
import nabla2.generator.TextFileTemplate

@Canonical
class FeatureCommonAreaTemplateHtml implements TextFileTemplate {

  Feature feature

  @Override
  String getSource() {
  """\
  |<!-- --------------------------------------
  |${feature.name}画面共通部テンプレートHTML
  |${regenerationMark}
  |--------------------------------------- -->
  |<h2>${feature.name}</h2>
  |<router-outlet></router-outlet>
  """.stripMargin().trim()
  }

  String getFeatureName() {
    feature.identifier.dasherized.get()
  }

  @Override
  String getRelPath() {
    "app/${featureName}/common-area/common-area.component.html"
  }
}
