package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class BaseWidgetTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import {UIEventHub, ViewModel} from '../base.module'
  |import {AppState} from '../app-state'
  |import {Router} from '@angular/router'
  |
  |/**
  | * ウィジェットコンポーネントの基底クラス
  | * ${regenerationMark}
  | */
  |export abstract class BaseWidget {
  |
  |  /**
  |   * コンストラクタ
  |   *
  |   * @param uiEventHub イベントハブ
  |   * @param viewModel ビューモデル
  |   * @param router 画面遷移定義
  |   */
  |  constructor(
  |    protected uiEventHub:UIEventHub,
  |    protected viewModel:ViewModel<AppState>,
  |    protected router:Router,
  |  ) {}
  |
  |  /**
  |   * 画面項目物理名
  |   */
  |  abstract get path():string
  |
  |  /**
  |   * コンポーネントで発生したDOMイベントをイベントハブに通知する。
  |   * @param event コンポーネント上で発生したDOMイベント
  |   * @param index 子番号(テーブルカラム、リストアイテムなどで使用する)
  |   */
  |  on(event:Event, index?:number):void {
  |    this.uiEventHub.next(this.path, event, index)
  |  }
  |
  |  /**
  |   * コンポーネントで発生したDOMイベントをイベントハブに通知するとともに、
  |   * その内容をビューモデルに反映する。
  |   * @param event コンポーネント上で発生したDOMイベント
  |   */
  |  update(event:Event):void {
  |    const element:HTMLFormElement = <HTMLFormElement>event.target
  |    this.on(event)
  |    this.viewModel.update(this.path, element.value)
  |  }
  |}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'app/widget/base-widget.ts'
  }
}
