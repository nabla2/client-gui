package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class IndexHtml implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |<!doctype html>
  |<html>
  |<head>
  |  <meta charset="utf-8">
  |  <title>${project.name}</title>
  |  <base href="/">
  |
  |  <meta name="viewport" content="width=device-width, initial-scale=1">
  |  <link rel="icon" type="image/x-icon" href="favicon.ico">
  |</head>
  |<body>
  |  <app-root>Loading...</app-root>
  |</body>
  |</html>
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "index.html"
  }
}
