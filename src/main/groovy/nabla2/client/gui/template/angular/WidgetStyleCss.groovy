package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.PageItemType
import nabla2.generator.TextFileTemplate

@Canonical
class WidgetStyleCss implements TextFileTemplate {

  PageItemType itemType

  @Override
  String getSource() {
  """\
  |/**
  | * ${itemType.name.literal.get().replaceAll(/\s+/, '').replaceAll(/【[^【】]*】/, '')}スタイルCSS
  | *
  | * ${regenerationMark}
  | */
  |${itemType.style.value.orElse('')}
  """.trim().stripMargin()
  }

  @Override
  String getRelPath() {
    String widgetName = itemType.identifier.dasherized.get()
    "app/${itemType.componentPath.get()}/${widgetName}/${widgetName}.component.css"
  }
}
