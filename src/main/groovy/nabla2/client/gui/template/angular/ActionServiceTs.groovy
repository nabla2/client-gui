package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Action
import nabla2.generator.TextFileTemplate

@Canonical
class ActionServiceTs implements TextFileTemplate {

  Action action

  @Override
  String getSource() {
  """\
  |import {Observer, Observable} from 'rxjs'
  |import {Injectable} from '@angular/core'
  |import {DatePipe} from '@angular/common'
  |import {Router} from '@angular/router'
  |import {Http, Response, Headers, RequestOptions, RequestOptionsArgs} from '@angular/http'
  |import {AppState} from '../app-state'
  |import {ViewModel, UIEvent, UIEventHub} from '../base.module'
  |import {environment as ENV} from '../../environments/environment'
  |
  |/**
  | * ${action.name.literal.map{it.replace('\n', '\n * ')}.orElse('')}
  | * ${regenerationMark}
  | */
  |@Injectable()
  |export class ${className} {
  |
  |  /**
  |   * コンストラクタ
  |   */
  |  constructor(
  |    private http:Http,
  |    private router:Router,
  |    private viewModel:ViewModel<AppState>,
  |    private eventHub:UIEventHub,
  |  ) {} 
  |
  |  /**
  |   * オブザーバを返す。
  |   */
  |  observer(${signature}):Observer<any> {
  |    ${action.observer.get().replace('\n', '\n|    ')}
  |  } 
  |}
  """.stripMargin().trim()
  }

  String getSignature() {
    List<String> requiredParams = action.name.params.findAll{!it.contains(':')}
    List<String> optionalParams = action.name.params.findAll{it.contains(':')}

    (requiredParams.findAll{!it.contains(':')}.collect{
      String typeName = it.tokenize('|')[0]
      String name     = it.tokenize('|')[1]
      "${name}:${paramTypeFromName(typeName)}".trim()
    } + (optionalParams.empty ? [] : ["opts:any = {}"])).join(', ')
  }

  String getClassName() {
    action.identifier.upperCamelized.get()
  }

  String paramTypeFromName(String paramName) {
      /*
    ParameterType paramType = action.project.parameterTypes.find{
      it.name.sameAs(paramName)
    }
    if (!paramType) throw new IllegalStateException(
      "Unknown parameter type : ${paramName}"
    )
    paramType.type
    */
      ''
  }

  @Override
  String getRelPath() {
    "app/action/${action.identifier.dasherized.get()}.service.ts"
  }
}
