package nabla2.client.gui.template.angular

import groovy.json.JsonSlurper
import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageItem
import nabla2.generator.TextFileTemplate

import java.util.function.Function
import java.util.stream.Collectors

@Canonical
class PageTemplateHtml implements TextFileTemplate {

  Page page

  @Override
  String getSource() {
  """\
  |<!-- --------------------------------
  |${page.name.get()}テンプレート
  |${regenerationMark}
  |--------------------------------- -->
  ${topLevelItems.collect{ item ->
    renderItem(item)
  }.join('\n')}
  """.stripMargin().trim().replaceAll(/\n\s*\n/, '\n')
  }

  String renderItem(PageItem item, int shift = 0) {

    if (item.type.dataOnly.truthy) return ''
    String s = ' ' * shift
    Map<String, Object> attrs = attributesOf(item)
    boolean isTemplate = item.parent.with {
      it && it.type.isIterator.truthy
    }
    String key = item.identifier.lowerCamelized.get()
    String value = isTemplate \
                 ? "each" \
                 : "state?.${accessPathOf(item)}"
    String itemPath = itemPathOf(item)
  """\
  |${s}${isTemplate ? '<ng-template let-each>' : ''}
  |${s}<${tagNameOf(item)}
  |${s}  path = "${itemPath}"
  |${s}  key = "${key}"
  |${s}  [value] = "${value}"
  ${item.type.name.params.any{it.endsWith('validationErrors')} ? """
  |${s}  [validationErrors] = "errorMessages\$('${itemPath}') | async"
  """ : ''}
  ${attrs.keySet().contains('title') ? '' : """
  |${s}  title = "${item.name}"
  """.trim()}
  ${attrs.isEmpty() ? '>' : '\n' + attrs.collect{ propName, propValue ->
  (propValue instanceof PageItem) ? """
  |${s}  [${propName}] = "state?.${accessPathOf(propValue)}"
  """.trim() : """
  |${s}  ${propName} = ${propValue}
  """.trim()}.join('\n') + ' >'}
  ${item.children.collect { child ->
    renderItem(child, shift + 2)
  }.join('\n')}
  |${s}</${tagNameOf(item)}>
  |${s}${isTemplate ? '</ng-template>' : ''}
  """.trim()
  }

  @Memoized
  static String itemPathOf(PageItem item) {
    PageItem i = item
    List<String> segments = [item.identifier.lowerCamelized.get()]
    while (i.parent) {
      i = i.parent
      segments.add(0, i.identifier.lowerCamelized.get())
    }
    segments.add(0, item.page.feature.identifier.lowerCamelized.get())
    segments.join('.')
  }

  static String tagNameOf(PageItem item) {
    'app-' + item.type.identifier.dasherized.get()
  }

  Map<String, Object> attributesOf(PageItem item) {
    Map<String, String> params = item.type.name.params.stream().collect(Collectors.toMap(
      {p -> p.tokenize(':')[0]} as Function<String, String>,
      {p -> p.tokenize(':')[1]} as Function<String, String>,
    ))
    item.type.name.capture(item.itemTypeName.get()).stream()
    .filter{ p -> p != null }
    .collect(Collectors.toMap(
      {p -> params.get(p.tokenize(':')[0])} as Function<String, String>,
      {p -> pageItemOrJson(p.tokenize(':')[1..-1].join(':'))} as Function<String, String>,
    ))
  }

  Object pageItemOrJson(String name) {
    page.pageItems.find{ it.name.sameAs(name) }.with {
      if (it) return it
      new JsonSlurper().parseText(name)
      return name
    }
  }

  static String accessPathOf(PageItem item) {
    PageItem i = item
    List<String> buff = []
    buff.add(0, i.identifier.lowerCamelized.get())

    while (i.parent) {
      i = i.parent
      buff.add(0, i.identifier.lowerCamelized.get())
    }
    buff.add(0, item.page.feature.identifier.lowerCamelized.get())
    buff.join('?.')
  }

  List<PageItem> getTopLevelItems() {
    page.pageItems.findAll{ !it.parent }
  }

  @Override
  String getRelPath() {
    getComponentPathOf(page) + '.html'
  }

  @Memoized
  String getPageId() {
    page.identifier.dasherized.get()
  }

  @Memoized
  String getFeatureId() {
    page.feature.identifier.dasherized.get()
  }

  @Memoized
  String getComponentName() {
    "${pageId}-page"
  }

  @Memoized
  String getComponentPathOf(Page page) {
    "app/${featureId}/${componentName}/${componentName}.component"
  }
}
