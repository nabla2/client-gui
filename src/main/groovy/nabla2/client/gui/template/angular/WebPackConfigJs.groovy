package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class WebPackConfigJs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |/**
  | * Webpackモジュールバンドラ設定
  | *
  | * ${regenerationMark}
  | */
  |const webpack = require('webpack')
  |const HtmlWebpackPlugin = require('html-webpack-plugin')
  |const ExtractTextPlugin = require('extract-text-webpack-plugin')
  |
  |module.exports =
  |{ entry:
  |  { app: './src/main/js/main.ts' }
  |, output:
  |  { filename : '[name].js'
  |  , path : __dirname + '/src/main/resources/static/'
  |  }
  |, resolve:
  |  { extensions: ['.ts', '.js']
  |  }
  |, module:
  |  { rules:
  |    [ { test   :/\\.ts\$/
  |      , loaders:[ 'awesome-typescript-loader'
  |                , 'angular2-template-loader'
  |                ]
  |      }
  |    , { test   :/\\.html\$/
  |      , loader :'html-loader'
  |      }  
  |    , { test   :/\\.css\$/
  |      , loaders:['to-string-loader', 'css-loader']
  |      }
  |    ]
  |  }
  |, plugins:
  |  [ new HtmlWebpackPlugin({template:'src/main/js/index.html'})
  |  ] 
  |}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'webpack.config.js'
  }
}
