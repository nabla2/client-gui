package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.PageItem
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ApplicationStateTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import {ValidationError} from './base.module'
  ${project.features.collect{
    String path = it.identifier.dasherized.get()
    String name = moduleNameOf(it)
  """\
  |import {${name}State, ${name}InitialState} from './${path}/${path}-module-state'
  """.trim()}.join('\n')}
  |
  |/**
  | * アプリケーション表示モデル定義
  | *
  | * ${regenerationMark}
  | */
  |export interface AppState {
  ${project.features.collect{feature -> """
  |  ${feature.identifier.lowerCamelized.get()} : ${moduleNameOf(feature)}State //${feature.name}
  """.trim()}.join('\n')}
  |  notificationMessage : string //メッセージ通知
  |  validationErrors : ValidationError[] //入力精査エラー
  |}
  |
  |/**
  | * アプリケーション表示モデル初期ステート
  | */
  |export const AppInitialState:AppState = {
  ${project.features.collect{feature -> """
  |  ${feature.identifier.lowerCamelized.get()} : ${moduleNameOf(feature)}InitialState, //${feature.name}
  """.trim()}.join('\n')}
  |  notificationMessage : '', //メッセージ通知
  |  validationErrors : [], //入力精査エラー
  |}
  """.stripMargin().trim()
  }

  @Memoized
  static String moduleNameOf(Feature feature) {
    "${feature.identifier.upperCamelized.get()}Module"
  }

  static String pathOf(Feature feature) {
    feature.basePath.embedParams{ paramName ->
      feature.pages
             .collectMany{ it.pageItems }
             .find{ it.name.sameAs(paramName) }
             .identifier.lowerCamelized
             .map{':' + it}
             .orElseThrow{new IllegalStateException(
               "Unresolved param ${paramName} in the base path of the feature: ${feature.name.get()}"
             )}
    }
    .orElse(feature.identifier.dasherized.get())
    .with{ "/${it}/".replaceAll(/\/+/,'/') }
  }

  private static String renderInitialStateOf(PageItem item, int shift = 0) {
    String key = item.identifier.lowerCamelized.get()
    String pad = ' ' * shift
    Optional<String> defaultValue = item.defaultValue.value
    String typeOfValue = defaultValue.map{typeOf(it)}.orElse('any')
    String dataType = item.type.dataType.value.orElse(typeOfValue)

    if (dataType == 'null') {
      return ''
    }
    if (!dataType || dataType == 'any') {
      return "|${pad}${key} : ${defaultValue.orElse('""')},"
    }
    if (dataType == 'array' || dataType.endsWith('[]')) {
      return "|${pad}${key} : ${defaultValue.orElse('[]')},"
    }
    if (dataType == 'object') {
      if (item.children.empty) {
        return "|${pad}${key} : ${defaultValue.orElse('{}')},"
      }
      return """
      |${pad}${key} : {
      ${item.children.collect{"""
      ${renderInitialStateOf(it, shift + 2)}
      """.trim()}.join('\n').trim()}
      |${pad}},
      """.trim()
    }
    return "|${pad}${key} : ${defaultValue.orElse('null')},"
  }

  static String typeOf(String v) {

    return v.startsWith('"')       ? 'string' \
         : v.startsWith('{')       ? 'object' \
         : v.startsWith('[')       ? 'array' \
         : v.startsWith('null')    ? 'null' \
         : v.matches(/true|false/) ? 'boolean' \
         : v.matches(/[.0-9]+/)    ? 'number' \
                                   : 'any'
  }

  @Override
  String getRelPath() {
    'app/app-state.ts'
  }
}
