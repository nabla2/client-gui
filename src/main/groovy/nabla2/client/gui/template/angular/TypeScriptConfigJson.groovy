package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class TypeScriptConfigJson implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |{ "//" : "${regenerationMark}"
  |, "compilerOptions":
  |  { "target"                         : "es5"
  |  , "module"                         : "commonjs"
  |  , "moduleResolution"               : "node"
  |  , "sourceMap"                      : true
  |  , "emitDecoratorMetadata"          : true
  |  , "experimentalDecorators"         : true
  |  , "lib"                            : ["es2017", "dom"]
  |  , "noImplicitAny"                  : true
  |  , "suppressImplicitAnyIndexErrors" : true
  |  }
  |}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'tsconfig.json'
  }
}
