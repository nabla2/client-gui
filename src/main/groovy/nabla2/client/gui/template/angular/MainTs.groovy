package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.PageItem
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class MainTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import { enableProdMode } from '@angular/core';
  |import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
  |
  |import { AppModule } from './app/app.module';
  |import { environment } from './environments/environment';
  |
  |if (environment.production) {
  |  enableProdMode();
  |}
  |
  |platformBrowserDynamic().bootstrapModule(AppModule);
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'main.ts'
  }
}
