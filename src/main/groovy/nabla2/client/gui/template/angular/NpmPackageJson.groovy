package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class NpmPackageJson implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |{ "//"      : "${regenerationMark}"
  |, "name"    : "${project.identifier}"
  |, "version" : "1.0.0"
  |, "scripts" :
  |  { "build" : "webpack"
  |  , "watch" : "webpack --watch"
  |  }
  |, "dependencies":
  |  { "@angular/core"                     : "*"
  |  , "@angular/compiler"                 : "*"
  |  , "@angular/common"                   : "*"
  |  , "@angular/http"                     : "*"
  |  , "@angular/platform-browser"         : "*"
  |  , "@angular/platform-browser-dynamic" : "*"
  |  , "@angular/router"                   : "*"
  |  , "@angular/forms"                    : "*"
  |  , "core-js"                           : "*"
  |  , "rxjs"                              : "*"
  |  , "zone.js"                           : "*"
  |  , "deep-extend"                       : "*"
  |  }
  |, "devDependencies":
  |  { "@types/node"                 : "*"
  |  , "@types/core-js"              : "*"
  |  , "@types/deep-extend"          : "*"
  |  , "typescript"                  : "*"
  |  , "webpack"                     : "*"
  |  , "angular2-template-loader"    : "*"
  |  , "awesome-typescript-loader"   : "*"
  |  , "style-loader"                : "*"
  |  , "css-loader"                  : "*"
  |  , "to-string-loader"            : "*"
  |  , "html-loader"                 : "*"
  |  , "html-webpack-plugin"         : "*"
  |  , "extract-text-webpack-plugin" : "*"
  |  }
  |}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "package.json"
  }
}
