package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ApplicationStyleCss implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |/**
  | * アプリケーションスタイル定義
  | * ${regenerationMark}
  | */
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "app/app.component.css"
  }
}
