package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Page
import nabla2.generator.TextFileTemplate

@Canonical
class PageStyleScss implements TextFileTemplate {

  Page page

  @Override
  String getSource() {
  """\
  |/**
  | * ${page.name.get()}スタイル定義
  | * ${regenerationMark}
  | */
  """.stripMargin().trim().replaceAll(/\n\s*\n/, '\n')
  }

  @Override
  String getRelPath() {
    getComponentPathOf(page) + '.scss'
  }

  @Memoized
  String getPageId() {
    page.identifier.dasherized.get()
  }

  @Memoized
  String getFeatureId() {
    page.feature.identifier.dasherized.get()
  }

  @Memoized
  String getComponentName() {
    "${pageId}-page"
  }

  @Memoized
  String getComponentPathOf(Page page) {
    "app/${featureId}/${componentName}/${componentName}.component"
  }
}
