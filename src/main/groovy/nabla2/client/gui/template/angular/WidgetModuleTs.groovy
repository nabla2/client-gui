package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class WidgetModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import {NgModule} from '@angular/core'
  |import {CommonModule} from '@angular/common'
  ${project.pageItemTypes.collect{
    String componentName = it.identifier.dasherized.get()
  """\
  |import {${it.identifier.upperCamelized.get()}} from './${componentName}/${componentName}.component'
  """.trim()}.sort().unique().join('\n')}
  |
  |/**
  | * ${project.name} Widgetモジュール
  | * ${regenerationMark}
  | */
  |@NgModule(
  |{ imports: [
  |    CommonModule,
  |  ]
  |, exports: [
  ${project.pageItemTypes.collect{"""\
  |    ${it.identifier.upperCamelized.get()},
  """.trim()}.sort().unique().join('\n')}
  |  ]
  |, declarations: [
  ${project.pageItemTypes.collect{"""\
  |    ${it.identifier.upperCamelized.get()},
  """.trim()}.sort().unique().join('\n')}
  |  ]
  |})
  |export class WidgetModule {}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "app/widget/widget.module.ts"
  }
}
