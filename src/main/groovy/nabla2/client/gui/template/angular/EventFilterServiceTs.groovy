package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.EventFilter
import nabla2.generator.TextFileTemplate

@Canonical
class EventFilterServiceTs implements TextFileTemplate {

  EventFilter eventFilter

  @Override
  String getSource() {
  """\
  |import {Observable} from 'rxjs'
  |import {Injectable} from '@angular/core'
  |import {UIEvent, UIEventHub, ViewModel} from '../base.module'
  |import {AppState} from '../app-state'
  |/**
  | * ${eventFilter.name.literal.get()}
  | * ${regenerationMark}
  | */
  |@Injectable()
  |export class ${className} {
  |
  |  /**
  |   * コンストラクタ
  |   */
  |  constructor(
  |    private uiEventHub:UIEventHub,
  |    private viewModel:ViewModel<AppState>
  |  ) {}
  |
  |  /**
  |   * イベントのフィルタ条件を返す
  |   */
  |  filter(${signature}):(v:any) => boolean {
  |    ${eventFilter.filterFunction.get().replaceAll('\n', '\n|    ')}
  |  }
  |}
  """.trim().stripMargin()
  }

  String getSignature() {
    eventFilter.name.params.collect{
      String typeName = it.tokenize('|')[0]
      String name     = it.tokenize('|')[1]
      "${name}:${paramTypeFromName(typeName)}".trim()
    }.join(', ')
  }

  private String getClassName() {
    eventFilter.identifier.upperCamelized.get()
  }

  String paramTypeFromName(String paramName) {

      ''
  }

  @Override
  String getRelPath() {
    "app/event/${eventFilter.identifier.dasherized.get()}.service.ts"
  }
}
