package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageItem
import nabla2.generator.TextFileTemplate

@Canonical
class FeatureModuleTs implements TextFileTemplate {

  Feature feature

  @Override
  String getSource() {
  """\
  |import {Component, NgModule} from '@angular/core'
  |import {CommonModule as NgCommonModule} from '@angular/common'
  |import {RouterModule, Routes} from '@angular/router'
  |import {HttpModule} from '@angular/http'
  |
  |import {CommonArea} from './common-area/common-area.component'
  ${feature.pages.collect{"""\
  |import {${componentNameOf(it)}} from '${componentPathOf(it)}'
  """.trim()}.join('\n')}
  |import {WidgetModule} from '../widget/widget.module'
  |
  |/**
  | * 機能内ルート定義
  | */
  |const routes:Routes =
  |[ { path: '${modulePath}'
  |  , component: CommonArea
  |  , children: [
  ${feature.pages.collect{"""\
  |      { path: '${pathOf(it)}', component:${componentNameOf(it)} }, //${it.name}
  """.trim()}.join('\n')}
  ${!initialPage ? '' : """\
  |      { path: '**', redirectTo: '${pathOf(initialPage)}' }, //${initialPage.name}(デフォルト遷移先)
  """.trim()}
  |    ]
  |  }
  |]
  |
  |/**
  | * ${feature.name} モジュール
  | * ${regenerationMark}
  | */
  |@NgModule(
  |{ imports:
  |  [ NgCommonModule
  |  , HttpModule
  |  , WidgetModule
  |  , RouterModule.forChild(routes)
  |  ]
  |, declarations:
  |  [ CommonArea
  ${feature.pages.collect{"""\
  |  , ${componentNameOf(it)}
  """.trim()}.join('\n')}
  |  ]
  |})
  |export class ${moduleName} {}
  """.stripMargin().trim()
  }

  @Memoized
  private Page getInitialPage() {
    feature.pages.find{ it.initialPage.truthy }
  }

  static String componentNameOf(Page page) {
    "${page.identifier.upperCamelized.get()}PageComponent"
  }

  static String componentPathOf(Page page) {
    String name = page.identifier.dasherized.get()
    "./${name}-page/${name}-page.component"
  }

  private String getModuleName() {
    "${feature.identifier.upperCamelized.get()}Module"
  }

  String getModulePath() {
    feature.basePath.embedParams{ paramName ->
      feature.pages
             .collectMany{ it.pageItems }
             .find{ it.name.sameAs(paramName) }
             .identifier.lowerCamelized
             .map{':' + it}
             .orElseThrow{new IllegalStateException(
               "Unresolved param ${paramName} in the base path of the feature: ${feature.name.get()}"
             )}
    }
    .orElse(feature.identifier.dasherized.get())
    .with{ "${it}".replaceAll(/\/+/,'/').replaceAll(/(^\/|\/$)/, '') }
  }

  String pathOf(Page page) {
    page.path.embedParams{ paramName ->
       feature.pages
             .collectMany{ it.pageItems }
             .find{ it.name.sameAs(paramName) }
             .identifier.lowerCamelized
             .map{':' + it}
             .orElseThrow{new IllegalStateException(
               "Unresolved param ${paramName} in the path of the page: ${feature.name.get()}"
             )}
    }
    .orElse(page.identifier.dasherized.get())
    .with{ "${it}".replaceAll(/\/+/,'/').replaceAll(/(^\/|\/$)/, '') }
  }

  String getFeatureName() {
    feature.identifier.dasherized.get()
  }

  @Override
  String getRelPath() {
    "app/${featureName}/${featureName}.module.ts"
  }
}
