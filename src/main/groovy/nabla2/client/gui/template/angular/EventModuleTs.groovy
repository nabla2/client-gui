package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Event
import nabla2.client.gui.model.EventFilter
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class EventModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """
  |import {Observable} from 'rxjs'
  |import {NgModule} from '@angular/core'
  |import {CommonModule} from '@angular/common'
  |import {BaseModule} from '../base.module'
  ${project.events.collect { event -> """
  |import {${classNameOf(event)}} from './${event.identifier.dasherized.get()}.service'
  |export {${classNameOf(event)}} from './${event.identifier.dasherized.get()}.service'
  """.trim()}.join('\n')}
  ${project.eventFilters.collect { filter -> """
  |import {${classNameOf(filter)}} from './${filter.identifier.dasherized.get()}.service'
  |export {${classNameOf(filter)}} from './${filter.identifier.dasherized.get()}.service'
  """.trim()}.join('\n')}
  |
  |/**
  | * イベント部品Angularモジュール
  | * ${regenerationMark}
  | */
  |@NgModule(
  |{ imports: [
  |    CommonModule,
  |    BaseModule,
  |  ]
  |, providers: [
  ${(project.events.collect{classNameOf(it)} + project.eventFilters.collect{classNameOf(it)}).collect {name -> """
  |    ${name},
  """.trim()}.join('\n')}
  |  ]
  |})
  |export class EventModule{}
  """.trim().stripMargin()
  }

  static String classNameOf(Event event) {
    event.identifier.upperCamelized.get()
  }

  static String classNameOf(EventFilter filter) {
    filter.identifier.upperCamelized.get()
  }

  @Override
  String getRelPath() {
    'app/event/event.module.ts'
  }
}
