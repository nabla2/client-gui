package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Action
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ActionModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """
  |import {Observer} from 'rxjs'
  |import {NgModule} from '@angular/core'
  |import {CommonModule, DatePipe} from '@angular/common'
  ${project.actions.collect { action -> """
  |import {${classNameOf(action)}} from './${action.identifier.dasherized.get()}.service'
  """.trim()}.join('\n')}
  |
  ${project.actions.collect { action -> """
  |export {${classNameOf(action)}} from './${action.identifier.dasherized.get()}.service'
  """.trim()}.join('\n')}
  |
  |/**
  | * アクション部品Angularモジュール
  | * ${regenerationMark}
  | */
  |@NgModule(
  |{ imports: [
  |    CommonModule,
  |  ]
  |, providers: [
  ${project.actions.collect { action -> """
  |    ${classNameOf(action)},
  """.trim()}.join('\n')}
  |    DatePipe,
  |  ]
  |})
  |export class ActionModule{}
  """.trim().stripMargin()
  }

  private static String classNameOf(Action action) {
    action.identifier.upperCamelized.get()
  }

  @Override
  String getRelPath() {
    "app/action/action.module.ts"
  }
}
