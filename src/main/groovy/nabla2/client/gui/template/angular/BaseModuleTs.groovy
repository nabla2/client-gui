package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class BaseModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import {Observable, Subject, BehaviorSubject, Observer, Subscription} from 'rxjs'
  |import {NgModule, Injectable, NgZone} from '@angular/core'
  |import {CommonModule} from '@angular/common'
  |import {Router, NavigationEnd} from '@angular/router'
  |import * as merge from 'deep-extend'
  |
  |@Injectable()
  |export class UIEventHub {
  |
  |  get all\$():Observable<UIEvent<any>> {
  |    return uiEventHub\$
  |  }
  |  get page\$():Observable<UIEvent<any>> {
  |    return this.all\$.filter(it => it.type === 'page')
  |  }
  |  get prevPage\$():BehaviorSubject<UIEvent<NavigationEnd>> {
  |    return prevPage\$
  |  }
  |  get click\$():Observable<UIEvent<Event>> {
  |    return this.all\$.filter(it => it.type === 'click')
  |  } 
  |  get apiResponse\$(): Observable<UIEvent<Response>> {
  |    return this.all\$.filter(it => it.type === 'api-response') 
  |  }
  |  next(path:string, event:any, typeOrIndex?:string|number, ):void {
  |    console.log('event:', path, event, typeOrIndex)
  |    uiEventHub\$.next(
  |      (typeOrIndex == null)              ? {path:path, type:event.type, event:event} :
  |      (typeof(typeOrIndex) === 'number') ? {path:path, type:event.type, event:event, index:typeOrIndex}
  |                                         : {path:path, type:typeOrIndex, event:event}
  |    )
  |  }
  |  constructor(private zone:NgZone) {
  |    this.page\$.pairwise().subscribe(([prev, curr]) => this.prevPage\$.next(prev))
  |  }
  |  runOutsideZone(observable:()=>Observable<any>):Observable<any> {
  |    const zone = this.zone
  |    return Observable.create((observer:Observer<any>) => {
  |      let subscription:Subscription
  |      zone.runOutsideAngular(() => {
  |        subscription = observable().subscribe({
  |          next(n)    { zone.run(() => observer.next(n)) },
  |          error(e)   { zone.run(() => observer.error(e)) },
  |          complete() { zone.run(() => observer.complete()) },
  |        })
  |      })
  |      return () => {
  |        if (subscription) subscription.unsubscribe
  |      }
  |    })
  |  }
  |  timeout(msec:number):Observable<any> {
  |    return this.runOutsideZone(() => Observable.timer(msec))
  |  }
  |  interval(msec:number):Observable<any> {
  |    return this.runOutsideZone(() => Observable.interval(msec))
  |  }
  |}
  |
  |const uiEventHub\$ = new Subject<UIEvent<any>>()
  |const prevPage\$ = new BehaviorSubject<UIEvent<NavigationEnd>>(null)
  |
  |export interface UIEvent<T> {
  |  path   : string,
  |  type   : string,
  |  event  : T,
  |  index? : number,
  |}
  |
  |export interface ValidationError {
  |  message: string
  |  fields: string[]
  |}
  |
  |@Injectable()
  |export class ViewModel<TModel> {
  |
  |  private _model\$:BehaviorSubject<TModel> = null
  |
  |  private initial:TModel = null
  |
  |  get model\$():Observable<TModel> {
  |    return this._model\$
  |  }
  |
  |  initialize(model:TModel):void {
  |    this.initial = model
  |    this._model\$ = new BehaviorSubject<TModel>(model)
  |  }
  |
  |  get(path:string, model?:TModel):any {
  |    const current = model || this._model\$.getValue()
  |    if (!path) return current
  |    const segments = path.split('.')
  |    let node = current
  |    while (segments.length){
  |      const key = segments.shift()
  |      node = node[key]
  |    }
  |    return node
  |  }
  |
  |  reset(path:string):void {
  |    this.update(path, this.get(path, this.initial))
  |  }
  |
  |  update(path:string, diff:any):void {
  |    const current = this._model\$.getValue()
  |    if (!path) {
  |      this._model\$.next(merge(current, diff))
  |      return
  |    }
  |    const segments = path.split('.')
  |    let root = {}
  |    let node = root
  |    while (segments.length){
  |      const key = segments.shift()
  |      if (!segments.length) {
  |        node[key] = diff
  |      }
  |      else if (!node.hasOwnProperty(key)) {
  |        node[key] = {} 
  |      }
  |      node = node[key]
  |    }
  |    const newState = merge(JSON.parse(JSON.stringify(current)), root)
  |
  |    console.log('from:', current, 'diff:', root, '--> newState:', newState)
  |    this._model\$.next(newState)
  |    return
  |  }
  |
  |  value\$<T>(path:string, defaultValue?:T):Observable<T> {
  |    return this.model\$.map(() => this.get(path) || defaultValue)
  |  }
  |}
  |
  |/**
  | * 基盤部品モジュール
  | * ${regenerationMark}
  | */
  |@NgModule(
  |{ imports: [
  |    CommonModule,
  |  ]
  |, providers: [
  |    UIEventHub,
  |    ViewModel,  
  |  ]
  |})
  |export class BaseModule {
  |  constructor(
  |    private router:Router,
  |    private uiEventHub:UIEventHub,
  |  ) {
  |    router.events.subscribe(e => {
  |      if (e instanceof NavigationEnd) {
  |        uiEventHub.next(e.urlAfterRedirects, new Event('page'))
  |      }
  |    })
  |  }
  |}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'app/base.module.ts'
  }
}
