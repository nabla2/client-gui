package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.PageItem
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ApplicationModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import {NgModule} from '@angular/core'
  |import {BrowserModule} from '@angular/platform-browser'
  |import {RouterModule, Routes} from '@angular/router'
  |import {AppComponent} from './app.component';
  |import {AppState, AppInitialState} from './app-state' 
  |import {BaseModule, ViewModel, ValidationError} from './base.module'
  |import {ActionModule} from './action/action.module'
  |import {EventModule} from './event/event.module'
  ${project.features.collect{
    String path = it.identifier.dasherized.get()
    String name = moduleNameOf(it)
  """\
  |import {${name}} from './${path}/${path}.module'
  """.trim()}.join('\n')}
  |
  |const routes:Routes = [${
  project.features.find{it.initial.truthy}.with { it == null ? '' : """
  |  { path: '**', redirectTo: '${pathOf(it)}' }, //${it.name}(デフォルト遷移先)
  """}}
  |]
  |
  |/**
  | * アプリケーションモジュール
  | * ${regenerationMark}
  | */
  |@NgModule(
  |{ imports:
  |  [ BrowserModule
  ${project.features.collect{"""\
  |  , ${it.identifier.upperCamelized.get()}Module
  """.trim()}.join('\n')}
  |  , RouterModule.forRoot(routes,{useHash:true})
  |  , BaseModule
  |  , ActionModule
  |  , EventModule
  |  ]
  |, declarations :[AppComponent]
  |, bootstrap    :[AppComponent]
  |})
  |export class AppModule {
  |  constructor(viewModel:ViewModel<AppState>) {
  |    viewModel.initialize(AppInitialState)
  |  }
  |}
  """.stripMargin().trim()
  }

  @Memoized
  static String moduleNameOf(Feature feature) {
    "${feature.identifier.upperCamelized.get()}Module"
  }

  static String pathOf(Feature feature) {
    feature.basePath.embedParams{ paramName ->
      feature.pages
             .collectMany{ it.pageItems }
             .find{ it.name.sameAs(paramName) }
             .identifier.lowerCamelized
             .map{':' + it}
             .orElseThrow{new IllegalStateException(
               "Unresolved param ${paramName} in the base path of the feature: ${feature.name.get()}"
             )}
    }
    .orElse(feature.identifier.dasherized.get())
    .with{ "/${it}/".replaceAll(/\/+/,'/') }
  }

  @Override
  String getRelPath() {
    'app/app.module.ts'
  }
}
