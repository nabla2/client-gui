package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Feature
import nabla2.generator.TextFileTemplate

@Canonical
class BasePageTs implements TextFileTemplate {

  Feature feature

  @Override
  String getSource() {
  """\
  |import {Observable, Observer, Subject} from 'rxjs'
  |import {Component, OnInit, OnDestroy} from '@angular/core'
  |import {ActivatedRoute, Router} from '@angular/router'
  |import {Http, Response, RequestOptions, Headers} from '@angular/http'
  |import {UIEventHub, ViewModel, ValidationError} from '../base.module'
  |import {AppState} from '../app-state'
  |
  |/**
  | * 画面コンポーネントの基底クラス
  | * ${regenerationMark}
  | */
  |export abstract class BasePage implements OnDestroy {
  |
  |  /**
  |   * コンポーネント破棄時にObservableのsubscribeを停止するために使用する。
  |   */
  |  protected unsubscribe:Subject<void> = new Subject<void>()
  |
  |  /**
  |   * コンストラクタ
  |   * @param router ルータサービス
  |   * @param viewModel ビューモデル管理
  |   * @param eventHub イベントハブ
  |   */
  |  constructor(
  |    protected router:Router,
  |    protected viewModel:ViewModel<AppState>,
  |    protected eventHub:UIEventHub,
  |  ) {}
  |
  |  /**
  |   * コンポーネント破棄時に全てのObservableのsubscriptionを停止する。
  |   */
  |  ngOnDestroy() {
  |    this.unsubscribe.next()
  |    this.unsubscribe.complete()
  |  }
  |
  |  /**
  |   * REST APIコールの共通エラーハンドラ
  |   */
  |  protected handleRestServiceError = (paramMap:any) => (res:Response):void => {
  |    console.log('REST API ERROR:', res)
  |    if (res.status >= 500) {
  |      this.router.navigate(['/', 'system-error'])
  |    }
  |    const body = res.json()
  |    if (!body.errors) {
  |      const errors:ValidationError[] = [
  |        {message:body.error, fields:[]}
  |      ]
  |      this.viewModel.update('validationErrors', errors)
  |    }
  |    else {
  |      const errors:ValidationError[] = body.errors.map((e:any) => {
  |        const fields:string[] = Array.isArray(e.field)
  |                              ? e.field.map((it:string) => paramMap[it])
  |                              : paramMap[e.field]
  |        return {
  |          message :e.defaultMessage,
  |          fields  :fields,
  |        }
  |      })
  |      this.viewModel.update('validationErrors', errors)
  |    }
  |  }
  |
  |  /**
  |   * 指定されたパラメータパスに関する精査エラーを取得する。
  |   * @param path パラメータパス
  |   */
  |  errorMessages\$(path:string):Observable<string[]> {
  |    return this.viewModel.model\$.map(
  |      m => m.validationErrors
  |            .filter(e => e.fields.includes(path))
  |            .map(e => e.message)
  |    )
  |  }
  |}
  """.stripMargin().trim()
  }

  String getFeatureName() {
    feature.identifier.dasherized.get()
  }

  @Override
  String getRelPath() {
    "app/${featureName}/base-page.ts"
  }
}
