package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.PageItemType
import nabla2.generator.TextFileTemplate

@Canonical
class WidgetComponentTs implements TextFileTemplate {

  PageItemType itemType

  @Override
  String getSource() {
  """\
  |import {${coreModules.join(', ')}} from '@angular/core'
  |import {Router} from '@angular/router'
  |import {Observable, Subject, BehaviorSubject} from 'rxjs'
  |import {UIEventHub, ViewModel} from '../../base.module'
  |import {AppState} from '../../app-state'
  |import {BaseWidget} from '../base-widget'
  |
  |/**
  | * ${itemType.name.literal.get().replaceAll(/\s+/, '').replaceAll(/【[^【】]*】/, '')}コンポーネント定義
  | * ${regenerationMark}
  | */
  |@Component({
  |  selector: '${selector}',
  |  templateUrl: './${widgetName}.component.html',
  |  styleUrls: ['./${widgetName}.component.scss']
  |})
  |export class ${className} extends BaseWidget {
  ${itemType.isIterator.truthy ? """
  |  /**
  |   * 子要素テンプレート
  |   */
  |  @ContentChildren(TemplateRef) templateRefs:QueryList<TemplateRef<any>>
  | 
  |  templates:TemplateRef<any>[]
  |
  |  ngAfterContentInit() {
  |    this.templates = this.templateRefs.toArray()
  |  }
  """ : ''}
  |  /**
  |   * 画面項目パス
  |   */
  |  @Input() path:string = ''
  |
  |  /**
  |   * 画面項目物理名
  |   */
  |  @Input() key:string = ''
  |
  |  /**
  |   * 画面項目値
  |   */
  |  @Input() value:${itemType.dataType} = null
  ${itemType.name.params.any{ it.endsWith(':title') } ? '' :
  """
  |  /**
  |   * 画面項目名
  |   */
  |  @Input() title:string = ''
  """}
  ${itemType.name.params.collect{"""
  |  /**
  |   * ${it.tokenize(':')[0]}
  |   */
  |  @Input() ${it.tokenize(':')[1]}:any = ''
  """.trim()}.join('\n')}
  |
  |  constructor(
  |    uiEventHub:UIEventHub,
  |    viewModel:ViewModel<AppState>,
  |    router:Router,
  |  ) {
  |    super(uiEventHub, viewModel, router)
  |  }
  |
  ${itemType.customProperty.value.map {p -> """\
  |  ${p.replaceAll(/\n/, '\n  ')}
  |  """.trim().toString()}.orElse('')
  }
  |}
  """.replaceAll(/\n\s*\n/, '\n').stripMargin().trim()
  }

  List<String> getCoreModules() {
    ['Component', 'Input', 'ChangeDetectionStrategy'] + (
      itemType.isIterator.truthy \
        ? ['ContentChildren', 'TemplateRef', 'QueryList'] \
        : []
    )
  }

  String getClassName() {
    itemType.identifier.className.get()
  }

  String getSelector() {
    'app-' + widgetName
  }

  String getWidgetName() {
    itemType.identifier.dasherized.get()
  }

  @Override
  String getRelPath() {
    "app/${itemType.componentPath.get()}/${widgetName}/${widgetName}.component.ts"
  }
}
