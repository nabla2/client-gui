package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import nabla2.client.gui.model.Event
import nabla2.generator.TextFileTemplate

@Canonical
class EventServiceTs implements TextFileTemplate {

  Event event

  @Override
  String getSource() {
  """\
  |import {Observable, Observer, Subscription} from 'rxjs'
  |import {Injectable, NgZone} from '@angular/core'
  |import {UIEvent, UIEventHub, ViewModel} from '../base.module'
  |import {AppState} from '../app-state'
  |/**
  | * ${event.name.literal.get()}
  | * ${regenerationMark}
  | */
  |@Injectable()
  |export class ${className} {
  |
  |  /**
  |   * コンストラクタ
  |   */
  |  constructor(
  |    private uiEventHub:UIEventHub,
  |    private viewModel:ViewModel<AppState>,
  |    private zone:NgZone,
  |  ) {}
  |
  |  /**
  |   * オブザーバブルを返す。
  |   */
  |  observable(${signature}):Observable<any> {
  |    ${event.observable.get().replaceAll('\n', '\n|    ')}
  |  }
  |}
  """.trim().stripMargin()
  }

  String getSignature() {
    event.name.params.collect{
      String typeName = it.tokenize('|')[0]
      String name     = it.tokenize('|')[1]
      "${name}:${paramTypeFromName(typeName)}".trim()
    }.join(', ')
  }

  private String getClassName() {
    event.identifier.upperCamelized.get()
  }

  String paramTypeFromName(String paramName) {
    ""
  }

  @Override
  String getRelPath() {
    "app/event/${event.identifier.dasherized.get()}.service.ts"
  }
}
