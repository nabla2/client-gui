package nabla2.client.gui.template.angular

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageItem
import nabla2.generator.TextFileTemplate

@Canonical
class FeatureModuleStateTs implements TextFileTemplate {

  Feature feature

  @Override
  String getSource() {
  """\
  |import {Component, NgModule} from '@angular/core'
  |import {CommonModule as NgCommonModule} from '@angular/common'
  |import {RouterModule, Routes} from '@angular/router'
  |import {HttpModule} from '@angular/http'
  |
  |/**
  | * ${feature.name}表示モデル定義
  | * @author nabla2.metamodel.generator
  | */
  |export interface ${moduleStateName} {
  ${renderInterfaceOf(itemPathTreeOf(feature), 2)}
  |}
  |
  |/**
  | * ${feature.name}表示モデル初期ステート
  | */
  |export const ${moduleInitialStateName}:${moduleStateName} = {
  ${renderInitialStateOf(itemPathTreeOf(feature), 2)}
  |}
  """.stripMargin().trim()
  }

  static String renderInterfaceOf(Map<String, Object> tree, int shift = 0) {
    String pad = ' ' * shift
    tree.collect{ _key, node ->
      boolean isArray = _key.endsWith('[]')
      String key = _key.replace('[]', '')
      if (node instanceof Map) {
        return """
        |${pad}${key} : {
        ${renderInterfaceOf(node, shift + 2)}
        |${pad}}${isArray ? '[]' : ''}
        """.trim()
      }
      PageItem item = (PageItem) node
      String dataType = item.type.dataType.get()

      if (!dataType || dataType == 'null') {
        return ''
      }
      else if (dataType == 'array') {
        return "|${pad}${key}: [] //${item.name.get()}"
      }
      else {
        return "|${pad}${key}: ${dataType} //${item.name.get()}"
      }
    }.join('\n').replaceAll(/\n\s*\n/, '\n').trim()
  }

  static String renderInitialStateOf(Map<String, Object> tree, int shift = 0) {
    String pad = ' ' * shift
    tree.collect{ _key, node ->
      boolean isArray = _key.endsWith('[]')
      String key = _key.replace('[]', '')
      if (node instanceof Map) {
        if (isArray) {
          return "|${pad}${key} : [],"
        }
        return """
        |${pad}${key} : {
        ${renderInitialStateOf(node, shift + 2)}
        |${pad}},
        """.trim()
      }
      PageItem item = (PageItem) node
      String dataType = item.type.dataType.get()

      if (!dataType || dataType == 'null') {
        return ''
      }
      String head = "${pad}${key}"
      return "|${head}: ${item.defaultValue.value.map{it.replaceAll(/\n/, '\n   ' + ' ' * head.length())}.orElse('null')}, //${item.name.get()}"

    }.join('\n').replaceAll(/\n\s*\n/, '\n').trim()
  }

  static Map<String, Object> itemPathTreeOf(Feature feature) {
    Map<String, Object> tree = [:]
    feature.pages.collectMany{it.pageItems}.each { PageItem pageItem ->
      Map<String, Object> node = tree
      List<String> path = pathOf(pageItem)
      path.eachWithIndex { String key, depth ->
        boolean leaf = (path.size()-1 == depth)
        boolean empty = !node.containsKey(key)
        if (leaf) {
          if (empty) node[key] = pageItem
          return
        }
        if (!leaf && (empty || node[key] instanceof PageItem)) {
          node[key] = [:]
        }
        node = node[key]
      }
    }
    tree
  }

  static List<String> pathOf(PageItem pageItem) {
    List<String> result = []
    PageItem item = pageItem
    while(true) {
      String key = item.identifier.lowerCamelized.get()
      boolean isArray = item.type.dataType.get().with{
        it == 'array' || it.endsWith('[]') || item.defaultValue.value.map{it.startsWith('[')}.orElse(false)
      }
      result.add(0, "${key}${isArray ? '[]' : ''}")
      item = item.parent
      if (!item) break
    }
    result
  }

  @Memoized
  private Page getInitialPage() {
    feature.pages.find{ it.initialPage.truthy }
  }

  private static String componentNameOf(Page page) {
    "${page.identifier.upperCamelized.get()}PageComponent"
  }

  private String componentPathOf(Page page) {
    "./${page.identifier.dasherized.get()}-page.component"
  }

  private String getComponentName() {
    "${feature.identifier.upperCamelized.get()}Component"
  }

  private String getModuleName() {
    "${feature.identifier.upperCamelized.get()}Module"
  }

  private String getModuleStateName() {
    "${feature.identifier.upperCamelized.get()}ModuleState"
  }


  private String getModuleInitialStateName() {
    "${feature.identifier.upperCamelized.get()}ModuleInitialState"
  }

  String getModulePath() {
    feature.basePath.embedParams{ paramName ->
      feature.pages
             .collectMany{ it.pageItems }
             .find{ it.name.sameAs(paramName) }
             .identifier.lowerCamelized
             .map{':' + it}
             .orElseThrow{new IllegalStateException(
               "Unresolved param ${paramName} in the base path of the feature: ${feature.name.get()}"
             )}
    }
    .orElse(feature.identifier.dasherized.get())
    .with{ "${it}".replaceAll(/\/+/,'/').replaceAll(/(^\/|\/$)/, '') }
  }

  String pathOf(Page page) {
    page.path.embedParams{ paramName ->
       feature.pages
             .collectMany{ it.pageItems }
             .find{ it.name.sameAs(paramName) }
             .identifier.lowerCamelized
             .map{':' + it}
             .orElseThrow{new IllegalStateException(
               "Unresolved param ${paramName} in the path of the page: ${feature.name.get()}"
             )}
    }
    .orElse(page.identifier.dasherized.get())
    .with{ "${it}".replaceAll(/\/+/,'/').replaceAll(/(^\/|\/$)/, '') }
  }

  String getFeatureName() {
    feature.identifier.dasherized.get()
  }

  @Override
  String getRelPath() {
    "app/${featureName}/${featureName}-module-state.ts"
  }
}
