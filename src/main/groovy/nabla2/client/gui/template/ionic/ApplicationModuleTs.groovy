package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ApplicationModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import { BrowserModule } from '@angular/platform-browser'
  |import { ErrorHandler, NgModule } from '@angular/core'
  |import { StoreModule } from '@ngrx/store'
  |import { EffectsModule } from '@ngrx/effects'
  |import { StoreDevtoolsModule } from '@ngrx/store-devtools'
  |import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular'
  |import { SplashScreen } from '@ionic-native/splash-screen'
  |import { StatusBar } from '@ionic-native/status-bar'
  |import { ViewModel } from '../view-model'
  |import { reducer, ActionCreator } from '../services/action.service'
  |import { UIEventHub } from '../services/event.service'
  |import { RestClient } from '../services/rest-client.service'
  |import { App } from './app.component'
  ${project.features.collect{ feature -> """\
  |import { ${feature.className}Module } from '../${feature.componentDir}/${feature.ngIdentifier}.module'
  """.trim()}.join('\n')}
  |
  |/**
  | * アプリケーションモジュール
  | * ${regenerationMark}
  | */
  |@NgModule({
  |  declarations: [
  |    App,
  |  ],
  |  imports: [
  |    BrowserModule,
  |    IonicModule.forRoot(App),
  |    StoreModule.forRoot(
  |      {viewModel: reducer},
  |      {initialState: {
  |        viewModel: ViewModel.getInitialState()
  |      }},
  |    ),
  |    EffectsModule.forRoot([]),
  |    StoreDevtoolsModule.instrument({
  |      maxAge: 25 //  Retains last 25 states
  |    }),
  ${project.features.collect{feature -> """\
  |    ${feature.className}Module,
  """.trim()}.join('\n')}
  |  ],
  |  bootstrap: [IonicApp],
  |  entryComponents: [
  |    App,
  |  ],
  |  providers: [
  |    ActionCreator,
  |    UIEventHub,
  |    RestClient,
  |    StatusBar,
  |    SplashScreen,
  |    {provide: ErrorHandler, useClass: IonicErrorHandler}
  |  ]
  |})
  |export class AppModule {}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'app/app.module.ts'
  }
}
