package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class MainTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |/**
  | * アプリケーションエントリーポイント
  | * ${regenerationMark}
  | */
  |import 'rxjs/Rx'
  |import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'
  |import { AppModule } from './app.module'
  |
  |platformBrowserDynamic().bootstrapModule(AppModule)
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'app/main.ts'
  }
}
