package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class GlobalScss implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |/**
  | * アプリケーションスタイル定義
  | * ${regenerationMark}
  | */
  |// Angular Material Theme
  |@import '../node_modules/@angular/material/prebuilt-themes/indigo-pink'
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "app/ionic.app.scss"
  }
}
