package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.gradle.GenerateIonicBasedWebClientOptionSet
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class BasePageTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |import { Observable, Subject } from 'rxjs'
    |import { Component, Injector } from '@angular/core'
    |import { Store } from '@ngrx/store'
    |import { IonicPage, NavController } from 'ionic-angular'
    |import { ViewModel } from './view-model'
    |import { UIEvent, UIEventHub } from './services/event.service'
    |
    |/**
    | * ページコンポーネントの共通基底クラス
    | * ${regenerationMark}
    | */
    |export abstract class BasePage {
    |
    |  /** 監視中のリアクティブストリームを一括解除するためのサブジェクト */
    |  protected unload\$:Subject<void>
    |
    |  /** 表示モデル */
    |  viewModel:ViewModel
    |
    |  /**
    |   * 指定された表示モデルパスにバインドされた項目に表示するエラーメッセージを取得する。
    |   * @param modelPath 表示モデルパス
    |   */
    |  errorMessages(modelPath:string):{modelPath:string, message:string}[] {
    |    if (!this.viewModel) return []
    |    return this.viewModel.errorMessages.filter(e => e.modelPath === modelPath)
    |  }
    |
    |  /**
    |   * コンストラクタ
    |   * @param navCtrl 画面遷移コントローラ
    |   * @param store Reduxストア
    |   */
    |  constructor(
    |    protected navCtrl: NavController,
    |    protected store: Store<{viewModel:ViewModel}>,
    |    protected eventHub: UIEventHub,
    |    protected injector: Injector,
    |  ) {
    |  }
    |
    |  /**
    |   * リアクティブストリームの監視を終了する。
    |   */
    |  ionViewDidLeave():void {
    |    this.unload\$.next()
    |    this.unload\$.complete()
    |  }
    |}    
    """.stripMargin().trim()
  }

  static String classNameOf(Feature feature) {
    feature.identifier.upperCamelized.get()
  }

  @Override
  String getRelPath() {
    "base-page.ts"
  }
}
