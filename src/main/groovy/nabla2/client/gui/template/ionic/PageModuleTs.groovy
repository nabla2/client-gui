package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.Page
import nabla2.generator.TextFileTemplate

@Canonical
class PageModuleTs implements TextFileTemplate {

  Page page

  @Override
  String getSource() {
    """\
    |import { NgModule } from '@angular/core'
    |import { EffectsModule } from '@ngrx/effects'
    |import { IonicPageModule } from 'ionic-angular'
    |import { ${page.className} } from './${page.ngIdentifier}.component.page'
    |import { ${page.className}Effects } from './${page.ngIdentifier}.effects.page'
    |import { WidgetModule } from '../../../../widgets/widget.module'
    |
    |/**
    | * ${page.name}モジュール
    | * ${regenerationMark}
    | */
    |@NgModule({
    |  declarations: [
    |    ${page.className},
    |  ],
    |  imports: [
    |    IonicPageModule.forChild(${page.className}),
    |    EffectsModule.forFeature([${page.className}Effects]),
    |    WidgetModule,
    |  ],
    |  entryComponents: [
    |    ${page.className},
    |  ]
    |})
    |export class ${page.className}Module {}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "features/${page.feature.ngIdentifier}/pages/${page.ngIdentifier}/${page.ngIdentifier}.component.page.module.ts"
  }
}
