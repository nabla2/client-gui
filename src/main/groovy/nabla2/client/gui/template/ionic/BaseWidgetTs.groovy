package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class BaseWidgetTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |import { Input } from '@angular/core'
    |import { Store } from '@ngrx/store'
    |import { NavController } from 'ionic-angular'
    |import { UIEventHub } from '../services/event.service'
    |import { ActionCreator } from '../services/action.service'
    |import { ViewModel } from '../view-model'
    |
    |/**
    | * ウィジェットの共通基底クラス
    | * ${regenerationMark}
    | */
    |export abstract class BaseWidget {
    |  /**
    |   * 項目識別子
    |   */
    |  @Input() id:string
    |
    |  /**
    |   * 項目論理名
    |   */
    |  @Input() title:string
    |
    |  constructor(
    |    public navCtrl: NavController,
    |    public store: Store<{viewModel:ViewModel}>,
    |    public eventHub: UIEventHub,
    |    public actionCreator: ActionCreator,
    |  ) {
    |  }
    |
    |  get htmlId():string {
    |    return this.id
    |  }
    |
    |  /**
    |   * コンポーネント内で発生したイベントを通知する。
    |   * @param eventType イベント種別
    |   * @param index 序数(リスト項目の場合のみ)
    |   */
    |  on(eventType:string|{type:string}, index?:number) {
    |    const type = (typeof eventType === 'string') ? eventType : eventType.type
    |    this.eventHub.next({
    |      id: this.id,
    |      type: type,
    |      source: {},
    |      index: index,
    |    })
    |  }
    |}
    """.stripMargin().trim()
  }

  static String classNameOf(Feature feature) {
    feature.identifier.upperCamelized.get()
  }

  @Override
  String getRelPath() {
    "widgets/base-widget.ts"
  }
}
