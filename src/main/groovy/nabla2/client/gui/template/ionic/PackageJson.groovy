package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

/**
 * additional components:
 * yarn add @ngrx/effects @ngrx/store @ngrx/store-devtools @angular/material @angular/cdk @angular/animations
 */
@Canonical
class PackageJson implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |{
    |  "name": "ionic-client",
    |  "version": "1.0.0",
    |  "author": "${regenerationMark}",
    |  "homepage": "http://ionicframework.com/",
    |  "private": true,
    |  "scripts": {
    |    "clean": "ionic-app-scripts clean",
    |    "build": "ionic-app-scripts build",
    |    "lint": "ionic-app-scripts lint",
    |    "ionic:build": "ionic-app-scripts build",
    |    "ionic:serve": "ionic-app-scripts serve",
    |    "e2e": "webdriver-manager update && protractor"
    |  },
    |  "dependencies": {
    |    "@angular/animations": "^5.1.2",
    |    "@angular/cdk": "^5.0.2",
    |    "@angular/common": "5.1.2",
    |    "@angular/compiler": "5.1.2",
    |    "@angular/compiler-cli": "5.1.2",
    |    "@angular/core": "5.1.2",
    |    "@angular/forms": "5.1.2",
    |    "@angular/http": "5.1.2",
    |    "@angular/material": "^5.0.2",
    |    "@angular/platform-browser": "5.1.2",
    |    "@angular/platform-browser-dynamic": "5.1.2",
    |    "@ionic-native/core": "4.5.2",
    |    "@ionic-native/splash-screen": "4.5.2",
    |    "@ionic-native/status-bar": "4.5.2",
    |    "@ionic/storage": "2.1.3",
    |    "@ngrx/effects": "^4.1.1",
    |    "@ngrx/store": "^4.1.1",
    |    "@ngrx/store-devtools": "^4.1.1",
    |    "downloadjs": "^1.4.7",
    |    "ionic-angular": "3.9.2",
    |    "ionicons": "3.0.0",
    |    "rxjs": "5.5.6",
    |    "sw-toolbox": "3.6.0",
    |    "zone.js": "0.8.18"
    |  },
    |  "devDependencies": {
    |    "@ionic/app-scripts": "3.1.6",
    |    "@types/jasmine": "^2.8.2",
    |    "@types/jasminewd2": "^2.0.3",
    |    "jasmine": "^2.8.0",
    |    "jasmine-spec-reporter": "^4.2.1",
    |    "protractor": "^5.2.2",
    |    "ts-node": "^4.1.0",
    |    "typescript": "2.6.2",
    |    "webdriver-manager": "^12.0.6"
    |  },
    |  "description": "An Ionic project"
    |}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "package.json"
  }
}
