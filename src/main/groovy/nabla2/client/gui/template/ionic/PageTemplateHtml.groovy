package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageItem
import nabla2.generator.TextFileTemplate

@Canonical
class PageTemplateHtml implements TextFileTemplate {

  Page page

  @Override
  String getSource() {
    """\
    |<!-- --------------------------------
    |${page.name}テンプレート
    |${regenerationMark}
    |--------------------------------- -->
    |<ion-header>
    |  <ion-navbar>
    |    <ion-title>${page.name}</ion-title>
    |  </ion-navbar>
    |</ion-header>
    |
    |<ion-content padding>
    ${page.topLevelPageItems.collect{ topLevelItem -> """\
    ${renderAsTag(topLevelItem, 2)} 
    """.trim()}.join('\n').replaceAll(/\n\s*\n/, '\n')}   
    |</ion-content>
    """.stripMargin().trim()
  }

  String renderAsTag(PageItem item, int offset=0) {
    String PAD = ' ' * offset
    """\
    |${PAD}<${item.ngTagName}
    |${PAD}  title = "${item.name}"
    |${PAD}  id = "${item.id}"
    ${item.type.listItem.truthy ? """\
    |${PAD}  [index] = "each.index"
    """.trim() : ''}
    ${item.type.propWithBinding.with{ boundProp ->
      if (!boundProp) return ''
      def boundModel = item.props.find{it.key == boundProp.name}
      if (!boundModel) throw new IllegalStateException(
        "Unknown model item name: ${boundProp.description}\n${item}"
      )
      def boundModelPath = boundModel.value
                          .replaceAll(/^viewModel\./, '')
                          .replaceAll(/\[]/, ".' + each.index + '")
    """\
    |${PAD}  [viewModelPath] = "'${boundModelPath}'"
    |${PAD}  [errorMessages] = "errorMessages('${boundModelPath}')"
    """.trim()}}
    ${item.props.collect{ prop -> """\
    |${PAD}  [${prop.key}] = "${prop.value.replace('.', '?.').replace('[]', '[each.index]').replace('\n', "\n|${PAD}    ")}"
    """.trim()}.join('\n')}>
    ${item.children.with {children -> children.empty ? '' : """\
    ${children.collect{"""\
    |${PAD}  <ng-template let-each>
    ${renderAsTag(it, offset+2)}
    |${PAD}  </ng-template>
    """.trim()}.join('\n')}
    """}}
    |${PAD}</${item.ngTagName}>
    """
  }

  @Override
  String getRelPath() {
    "features/${page.feature.ngIdentifier}/pages/${page.ngIdentifier}/${page.ngIdentifier}.page.html"
  }
}
