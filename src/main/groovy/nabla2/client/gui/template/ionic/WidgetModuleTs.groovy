package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Event
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class WidgetModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |import { NgModule } from '@angular/core'
    |import { CommonModule } from '@angular/common'
    |import { IonicModule } from 'ionic-angular'
    |import { MaterialModule } from './material.module' 
    ${project.pageItemTypes.collect{ widget -> """\
    |import { ${widget.className} } from './${widget.componentPath}/${widget.ngIdentifier}/${widget.ngIdentifier}.component'
    """.trim()}.join('\n')}
    |
    |// ** a workaround for https://github.com/ionic-team/ionic/issues/6923
    |import { Directive, ContentChildren, QueryList, Self } from '@angular/core'
    |import { Subscription } from 'rxjs'
    |import { Segment, SegmentButton } from 'ionic-angular'
    |
    |const widgets:any[] = [
    ${project.pageItemTypes.collect{ widget -> """\
    |  ${widget.className},
    """.trim()}.join('\n')}
    |]
    |
    |// TODO: Hotfix for dynamic ion-segment-buttons issue (https://github.com/driftyco/ionic/issues/6923).
    |@Directive({
    |    selector: 'ion-segment'
    |})
    |export class IonSegmentHotfix {
    |    private subscriptions: Subscription;
    |
    |    @ContentChildren(SegmentButton)
    |    buttons: QueryList<SegmentButton>;
    |
    |    constructor(@Self() private segment: Segment) {
    |    }
    |
    |    ngAfterContentInit() {
    |        this.subscriptions = this.buttons.changes.subscribe(() => this.onChildrenChanged());
    |    }
    |
    |    ngOnDestroy() {
    |        if (this.subscriptions) {
    |            this.subscriptions.unsubscribe();
    |            this.subscriptions = null;
    |        }
    |    }
    |
    |    private onChildrenChanged() {
    |        setTimeout(() => {
    |            this.segment.ngAfterContentInit();
    |            this.segment._inputUpdated();
    |        });
    |    }
    |}
    |
    |/**
    | * カスタムウィジェットモジュール
    | * ${regenerationMark}
    | */
    |@NgModule({
    |  imports:[
    |    CommonModule,
    |    IonicModule,
    |    MaterialModule,
    |  ],
    |  declarations: widgets.concat(IonSegmentHotfix),
    |  entryComponents: widgets,
    |  exports: widgets.concat(IonSegmentHotfix),
    |})
    |export class WidgetModule {}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'widgets/widget.module.ts'
  }
}
