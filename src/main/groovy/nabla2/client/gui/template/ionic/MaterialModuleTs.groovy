package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class MaterialModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |import { NgModule } from '@angular/core'
    |
    |import {
    |  MatButtonModule,
    |  MatMenuModule,
    |  MatToolbarModule,
    |  MatIconModule,
    |  MatCardModule,
    |  MatSelectModule,
    |  MatCheckboxModule,
    |  MatProgressBarModule,
    |  MatProgressSpinnerModule,
    |  MatRadioModule,
    |  MatTooltipModule,
    |  MatFormFieldModule,
    |  MatInputModule,
    |} from '@angular/material'
    |
    |/**
    | * Angular-Material Modules Bundle
    | * ${regenerationMark}
    | */
    |@NgModule({
    |  imports: [
    |    MatButtonModule,
    |    MatMenuModule,
    |    MatToolbarModule,
    |    MatIconModule,
    |    MatCardModule,
    |    MatSelectModule,
    |    MatCheckboxModule,
    |    MatProgressBarModule,
    |    MatProgressSpinnerModule,
    |    MatRadioModule,
    |    MatTooltipModule,
    |    MatFormFieldModule,
    |    MatInputModule,
    |  ],
    |  exports: [
    |    MatButtonModule,
    |    MatMenuModule,
    |    MatToolbarModule,
    |    MatIconModule,
    |    MatCardModule,
    |    MatSelectModule,
    |    MatCheckboxModule,
    |    MatProgressBarModule,
    |    MatProgressSpinnerModule,
    |    MatRadioModule,
    |    MatTooltipModule,
    |    MatFormFieldModule,
    |    MatInputModule,
    |  ],
    |})
    |export class MaterialModule {}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'widgets/material.module.ts'
  }
}
