package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.PageItemType
import nabla2.client.gui.model.PageItemTypeProp
import nabla2.generator.TextFileTemplate
import static nabla2.metamodel.datatype.StrictJavaIdentifier.*

@Canonical
class WidgetComponentTs implements TextFileTemplate {

  PageItemType type

  static List<String> INJECTABLES = []

  @Override
  String getSource() {
    """\
    ${type.hasChildren.truthy ? """\
    |import { Component, Input, ChangeDetectionStrategy, TemplateRef, QueryList, ContentChildren } from '@angular/core'
    """:"""\
    |import { Component, Input, ChangeDetectionStrategy } from '@angular/core'
    """.trim()}
    |import { Store } from '@ngrx/store'
    |import { NavController } from 'ionic-angular'
    |import { UIEventHub } from '../../../services/event.service'
    |import { ActionCreator } from '../../../services/action.service'
    |import { ViewModel } from '../../../view-model'
    |import { BaseWidget } from '../../base-widget'
    ${type.additionalDeclarations.value.map{ declarations -> """\
    |${declarations.replace('\n', '\n|')}
    """.trim()}.orElse('')}
    |/**
    | * ${type.name.literal.get().replaceAll(/\n/, '\n| *')}
    | * ${regenerationMark}
    | */
    |@Component({
    |  changeDetection: ChangeDetectionStrategy.OnPush,
    |  selector: '${type.ngTagName}',
    |  templateUrl: './${type.ngIdentifier}.html',
    |  styles: [`
    |    ${type.style.value.orElse('').replace('\n', '\n|    ')}
    |  `]
    |})
    |export class ${type.className} extends BaseWidget {
    ${type.hasChildren.truthy ? """\
    |  /**
    |   * 子要素
    |   */
    |  children:TemplateRef<any>[]
    |
    |  @ContentChildren(TemplateRef) templateRefs:QueryList<TemplateRef<any>>
    |
    |  ngAfterContentInit() {
    |    this.children = this.templateRefs.toArray()
    |  }
    """ : ''}
    ${type.props.findAll{!hasCustomAccessorOf(it)}.collect{ prop -> """\
    |  /**
    |   * ${prop.description}
    |   */
    |  @Input() ${prop.name}:${prop.type.replace('\n', '\n|    ')}
    |
    """.trim()}.join('\n')}
    |  /**
    |   * コンストラクタ
    |   * @param navCtrl 画面遷移管理サービス
    |   * @param store Reduxストア
    |   */
    |  constructor(
    |    public navCtrl: NavController,
    |    public store: Store<{viewModel:ViewModel}>,
    |    public eventHub: UIEventHub,
    |    public actionCreator: ActionCreator,
    ${type.additionalModulesNames.findAll{ INJECTABLES.contains(it)}.collect{ injectable -> """\
    |    public ${lowerCamelize(injectable)}: ${upperCamelize(injectable)},
    """.trim()}.join('\n')}
    |  ) {
    |    super(navCtrl, store, eventHub, actionCreator);
    |  }
    ${type.listItem.truthy ? """\
    |  get htmlId():string {
    |    return `\${this.id}[\${this.index}]`
    |  }
    """ : ''}
    ${type.customProperty.value.map{ customProperty -> """\
    |  ${customProperty.replaceAll('\n', '\n|' + ' '*2)}
    """.trim()}.orElse('')}
    ${type.propWithBinding ? """\
    |  /**
    |   * このコンポーネントの状態をバインドされた表示項目の状態に反映する。
    |   * @param event イベント
    |   */
    |  update(event:Event) {
    |    const node = <HTMLFormElement>event.target
    |    const value = node.value
    |    this.store.dispatch(this.actionCreator.updateViewModel(
    |      this.viewModelPath,
    |      (m) => value,
    |      {id:this.id, type:event.type, source:event},
    |      `update by \${this.id}`,
    |    ))
    |    this.on(event)
    |  }
    """ : ''}
    |}
    """.replaceAll(/\n\s*\n/, '\n').stripMargin().trim()
  }

  boolean hasCustomAccessorOf(PageItemTypeProp prop) {
    type.customProperty.value.map{ code ->
      code.contains("set ${prop.name}(")
    }.orElse(false)
  }

  @Override
  String getRelPath() {
    "widgets/${type.componentPath}/${type.ngIdentifier}/${type.ngIdentifier}.component.ts"
  }
}
