package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Page
import nabla2.generator.TextFileTemplate

@Canonical
class PageApiCallTs implements TextFileTemplate {

  Page page

  @Override
  String getSource() {
    """\
    |import { ApiCall } from '../../../../services/rest-client.service'
    |import { ViewModel } from '../../../../view-model'
    |import * as ENV from '../../../../environment'
    |
    |/**
    | * ${page.name}ページ内API呼び出し定義
    | * ${regenerationMark}
    | */
    |export const apiCalls:{[key:string]:ApiCall} = ${page.apiDefinitionJs()}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "features/${page.feature.ngIdentifier}/pages/${page.ngIdentifier}/${page.ngIdentifier}.api-call.page.ts"
  }
}
