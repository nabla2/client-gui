package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Feature
import nabla2.generator.TextFileTemplate

@Canonical
class FeatureModuleTs implements TextFileTemplate {

  Feature feature

  @Override
  String getSource() {
    """\
    |import { NgModule } from '@angular/core'
    ${feature.pages.collect{ page -> """\
    |import { ${page.className}Module } from './pages/${page.ngIdentifier}/${page.ngIdentifier}.component.page.module'
    """.trim()}.join('\n')}
    |export { ${feature.className}ViewModel, ${feature.className}InitialState } from './${feature.ngIdentifier}-view-model'
    |
    |/**
    | * ${feature.name} モジュール
    | * ${regenerationMark}
    | */
    |@NgModule({
    |  declarations: [
    |  ],
    |  imports: [
    |  ],
    |  exports: [
    ${feature.pages.collect{ page -> """\
    |    ${page.className}Module,
    """.trim()}.join('\n')}
    |  ],
    |  entryComponents: [
    |  ]
    |})
    |export class ${feature.className}Module {}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "${feature.componentDir}/${feature.ngIdentifier}.module.ts"
  }
}
