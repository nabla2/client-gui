package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class TypeScriptConfigJson implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |{ "//" : "${regenerationMark}"
  |, "compilerOptions":
  |  { "allowSyntheticDefaultImports"   : true
  |  , "declaration"                    : false
  |  , "emitDecoratorMetadata"          : true
  |  , "experimentalDecorators"         : true
  |  , "lib"                            : ["es2015", "dom"]
  |  , "module"                         : "es2015"
  |  , "moduleResolution"               : "node"
  |  , "sourceMap"                      : true
  |  , "target"                         : "es5"
  |  , "typeRoots"                      : ["node_modules/@types"]
  |  , "noImplicitAny"                  : true
  |  , "suppressImplicitAnyIndexErrors" : true
  |  }
  |, "exclude": ["e2e"]
  |}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'tsconfig.json'
  }
}
