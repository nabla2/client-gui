package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.*
import nabla2.generator.TextFileTemplate

@Canonical
class PageComponentTs implements TextFileTemplate {

  Page page

  @Override
  String getSource() {
    """\
    |import { Observable, Subject } from 'rxjs'
    |import { Component, Injector } from '@angular/core'
    |import { Store } from '@ngrx/store'
    |import { IonicPage, NavController } from 'ionic-angular'
    |import { ViewModel } from '../../../../view-model'
    |import { BasePage } from '../../../../base-page'
    |import { UIEvent, UIEventHub } from '../../../../services/event.service'
    |import { apiCalls } from './${page.ngIdentifier}.api-call.page'
    |
    |/**
    | * ${page.name}ページコンポーネント
    | * ${regenerationMark}
    | */
    |@IonicPage({
    |  name:'${page.ngIdentifier}',
    |  segment:'${page.routingPath.replaceAll(/(^\/|:)/, '')}',
    |})
    |@Component({
    |  selector: 'page-${page.ngIdentifier}',
    |  templateUrl: '${page.ngIdentifier}.page.html'
    |})
    |export class ${page.className} extends BasePage {
    |
    |  /** 画面名 */
    |  pageName:string = '${page.ngIdentifier}'
    |
    |  /** 機能名 */
    |  featureName:string = '${page.feature.ngIdentifier}'
    |
    |  /**
    |   * コンストラクタ
    |   * @param navCtrl 画面遷移コントローラ
    |   * @param store Reduxストア
    |   */
    |  constructor(
    |    public navCtrl: NavController,
    |    public store: Store<{viewModel:ViewModel}>,
    |    public eventHub: UIEventHub,
    |    public injector: Injector,
    |  ) {
    |    super(navCtrl, store, eventHub, injector)
    |  }
    |
    |  /**
    |   * メンバ変数の初期化とリアクティブストリームの監視を開始する。
    |   */
    |  ionViewWillEnter() {
    |    this.unload\$ = new Subject<void>()
    |    this.store.select('viewModel')
    |              .takeUntil(this.unload\$)
    |              .subscribe(v => this.viewModel = v)
    |    this.subscribe([
    ${page.eventNames.collect{ eventName -> """\
    |      `${eventName.join(' ')}`,
    """.trim()}.join('\n')}
    |    ])
    |  }
    |
    |  /**
    |   * この画面のイベント定義に従って対応するイベントストリームの監視を開始する。
    |   * @param eventNames この画面のイベント論理名
    |   */
    |  subscribe(eventNames:string[]) {
    |    eventNames.forEach(eventName => {
    |      this.event(eventName)
    |      .takeUntil(this.unload\$)
    |      .subscribe(e => this.store.dispatch({
    |        type  :`\${this.featureName}/\${this.pageName}/\${eventName}`,
    |        event :e,
    |        desc  :eventName,
    |      }))
    |    })
    |    this.eventHub.next({
    |      type   :'page_initialized',
    |      id     :`\${this.featureName}/\${this.pageName}`,
    |      source :{page:this.pageName, feature:this.featureName}
    |    })
    |  }
    |
    |  /**
    |   * ページ内イベント定義
    |   * @param type ページ内イベント論理名
    |   */
    |  event(type:string):Observable<UIEvent<any>> {
    |    switch (type) {
    ${page.eventNames.collect{ eventName ->
      PageBehavior behavior = page.pageBehaviors.find {
        it.eventDesc.sameAs(eventName[0]) &&
        (it.eventFilter ? it.filterCondition.sameAs(eventName[1]) : eventName.size() == 1)
      }
      if (!behavior) throw new IllegalStateException(
        "Unknown event: ${eventName}"
      )
    """\
    |      case `${eventName.join(' ')}` :
    |        return this.eventHub.${behavior.event.identifier.lowerCamelized.get()}(
    |          ${behavior.eventParams.collect{it.value}.join(',\n|' + ' '*12)}
    |        )
    ${behavior.eventFilter ? """\
    |       .filter(this.eventHub.${behavior.eventFilter.identifier.lowerCamelized.get()}(
    |          ${behavior.filterParams.collect{it.value}.join(',\n|' + ' '*10)}
    |       ))
    """ : ''}
    """.trim()}.join('\n').replaceAll(/\([|\s\n]+\)/, '()')}
    |      default:
    |        return Observable.never()
    |    }
    |  }
    |}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "features/${page.feature.ngIdentifier}/pages/${page.ngIdentifier}/${page.ngIdentifier}.component.page.ts"
  }
}
