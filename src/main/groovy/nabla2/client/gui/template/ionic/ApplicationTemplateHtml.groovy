package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ApplicationTemplateHtml implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |<!-- --------------------------------------
  |アプリケーションテンプレートHTML
  |${regenerationMark}
  |--------------------------------------- -->
  |<ion-nav [root]="rootPage"></ion-nav>
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "app/app.html"
  }
}
