package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Action
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ActionModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |import { Injectable, Injector } from '@angular/core'
    |import { Action } from '@ngrx/store'
    |import { App, NavController, ModalController } from 'ionic-angular'
    |import { ViewModel } from '../view-model'
    |import { UIEvent, UIEventHub } from './event.service'
    |import { ApiCall, RestClient } from './rest-client.service'
    |import * as download from 'downloadjs'
    |
    |/**
    | * アクションオブジェクト
    | * ${regenerationMark}
    | */
    |export class UIAction implements Action {
    |  type : string
    |  desc : string
    |  event: UIEvent<any>
    |  [extraProps: string]: any
    ${project.actions.collect{ action ->
      String TYPE_TOKEN = typeTokenOf(action)
    """
    |  static ${TYPE_TOKEN} = 'core/${TYPE_TOKEN}'
    """.trim()}.join('\n')}
    |}
    |
    |/**
    | * アクションクリエータ定義
    | */
    |@Injectable()
    |export class ActionCreator {
    |
    |  /**
    |   * コンストラクタ
    |   */
    |  constructor(
    |    public eventHub:UIEventHub,
    |    public restClient:RestClient,
    |    public app:App,
    |    public modalCtrl:ModalController,
    |  ) {}
    |
    |  /**
    |   * 画面遷移コントローラ
    |   */
    |  get nav():NavController {
    |    return this.app.getRootNavs()[0]
    |  }
    |
    |  /**
    |   * モーダル画面コントローラ
    |   */
    |  get modal():ModalController {
    |    return this.modalCtrl
    |  }
    |
    ${project.actions.collect{ action ->
      List<Prop> props = propsOf(action)  
      String TYPE_TOKEN = typeTokenOf(action)  
    """\
    |  /**
    ${action.comment.empty ? '' : """\
    |   * ${action.comment.get().replace('\n', '\n|   * ')}
    """.trim()}
    ${props.collect{ prop -> """\
    |   * @param ${prop.key} ${prop.title}
    """.trim()}.join('\n')}
    |   * @param event このアクションの起因イベント
    |   * @param desc このアクションの詳細說明(デバッグ用)
    |   */
    |  ${action.identifier.lowerCamelized.get()} = (
    |    ${(props.collect{"${it.key}:${it.type}".toString()} + ['event:UIEvent<any>', 'desc:string']).join(', ')}
    |  ):UIAction => {
    |    return {
    |      type: UIAction.${TYPE_TOKEN},
    ${props.empty ? '' : props.collect{ prop -> """\
    |      ${prop.key}: ${prop.key},
    """.trim()}.join('\n')}
    |      event: event,
    |      desc: desc,
    |      nav: () => this.nav,
    |      modal: () => this.modal,
    |      restClient: this.restClient,
    |    }
    |  }
    |
    """.trim()}.join('\n')}
    |}
    |
    |/**
    | * アクション実行定義
    | * @param model アプリケーションステート
    | * @param action アクションオブジェクト
    | */
    |export function reducer(model:ViewModel = ViewModel.getInitialState(), action:UIAction) {
    |  switch (action.type) {
    ${project.actions.collect{ action ->
      List<Prop> props = propsOf(action)
    """\
    |    // ${action.name.literal.get().replace('\n', '\n|    // ')}
    |    case UIAction.${typeTokenOf(action)}:
    |      return ((model, {${props.collect{it.key}.join(', ')}}):ViewModel => {
    |        ${action.reducer.get().replace('\n', '\n|'+' '*8)}
    |      })(model, action)
    |
    """.trim()}.join('\n')}
    |
    |    default:
    |      return model
    |  }
    |}
    """.stripMargin().trim()
  }

  static class Prop {
    final String title
    final String key
    final String type
    Prop(String definition) {
      List<String> tokens = definition.tokenize(':')
      this.title = tokens[0]
      this.key   = tokens[1]
      this.type  = tokens[2..-1].join(':')
    }
  }

  static List<Prop> propsOf(Action action) {
    action.name.params.collect{ param ->
      new Prop(param)
    }
  }

  static String typeTokenOf(Action action) {
    action.identifier.dasherized.get().toUpperCase().replace('-', '_')
  }

  @Override
  String getRelPath() {
    "services/action.service.ts"
  }
}
