package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class EnvironmentTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |/**
    | * 環境定数定義
    | * ${regenerationMark}
    | */
    |
    ${project.services.collect{ service -> """\
    |/**
    | * ${service.name}エンドポイント
    | */
    |export const ${service.prefixSymbol}_ENDPOINT = '${service.endPoint}'
    """.trim()}.join('\n')}
    """.replaceAll(/\n\s*\n/, '\n').stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'environment.ts'
  }
}
