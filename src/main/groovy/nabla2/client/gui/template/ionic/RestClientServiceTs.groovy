package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class RestClientServiceTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |import { Injectable } from '@angular/core'
    |import { ViewModel } from '../view-model'
    |import { UIEventHub } from './event.service'
    |
    |/**
    | * REST APIクライアントサービス
    | * ${regenerationMark}
    | */
    |@Injectable()
    |export class RestClient {
    |
    |  /**
    |   * コンストラクタ
    |   * @param eventHub イベントハブ
    |   */
    |  constructor(
    |    public eventHub:UIEventHub
    |  ) {}
    |
    |  /**
    |   * APIリクエストを送信する。
    |   * @param props 送信定義
    |   * @param model 画面表示モデル
    |   */
    |  send(call:ApiCall, model:ViewModel):ApiCall {
    |    const usesBody = (call.props.method !== 'GET')
    |    const headers:Headers = new Headers()
    |    headers.append('Accept', 'application/json, text/plain, */*')
    |    headers.append('Content-Type', 'application/json')
    |    fetch(call.props.url(model), {
    |      method: call.props.method,
    |      headers: headers,
    |      credentials: 'same-origin',
    |      ... usesBody && {body: JSON.stringify(call.props.payload(model))}
    |    }).then(response => {
    |      this.eventHub.next({
    |        type: 'api_response',
    |        id: call.props.id,
    |        source: {
    |          apiCall: call,
    |          response: response,
    |        }
    |      })
    |    }).catch(e => {
    |      console.log(`An error occured while calling the api: \${call.props.id}`)
    |      throw e
    |    })
    |    return call
    |  }
    |}
    |
    |export class ApiCallProps {
    |  /** API呼び出し名 */
    |  id: string
    |  /** HTTPメソッド */
    |  method: string
    |  /** APIエンドポイントURL */
    |  url: (model:ViewModel) => string
    |  /** リクエストボディ内容を返す関数 */
    |  payload: (model:ViewModel) => any
    |  /** レスポンスボディを画面の表示モデルに反映する関数 */
    |  modelUpdate: (model:ViewModel, body:any) => ViewModel
    |  /** レスポンスボディ中のエラーメッセージを画面の表示モデルに反映する関数 */
    |  errorUpdate: (model:ViewModel, body:any) => ViewModel
    |}
    |
    |export class ApiCall {
    |
    |  props:ApiCallProps
    |
    |  constructor(props:ApiCallProps) {
    |    this.props = props
    |  }
    |
    |  get id():string {
    |    return this.props.id
    |  }
    |
    |  /**
    |   * APIのレスポンス内容を元に画面表示モデルを更新した結果を返す。
    |   * 変更元表示モデルの状態は不変である。
    |   * @param model 変更元表示モデル
    |   */
    |  updateWithResponse(model:ViewModel, body:any):ViewModel {
    |    return this.props.modelUpdate(model, body)
    |  }
    |
    |  /**
    |   * APIからユーザエラーが返された場合に、そのレスポンス内容を反映したモデルを返す。
    |   * 変更元表示モデルの状態は不変である。
    |   * @param model 変更元表示モデル
    |   */
    |  updateWithUserError(model:ViewModel, body:any):ViewModel {
    |    return this.props.errorUpdate(model, body)
    |  }
    |}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "./services/rest-client.service.ts"
  }
}
