package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ApplicationComponentTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import { Component } from '@angular/core'
  |import { Platform } from 'ionic-angular'
  |import { StatusBar } from '@ionic-native/status-bar'
  |import { SplashScreen } from '@ionic-native/splash-screen'
  |import { ${rootPage.className} } from '../${rootPage.componentDir}/${rootPage.ngIdentifier}.component.page'
  |
  |/**
  | * アプリケーションコンポーネント
  | * ${regenerationMark}
  | */
  |@Component({
  |  templateUrl: 'app.html'
  |})
  |export class App {
  |
  |  rootPage:any = ${rootPage.className}
  |
  |  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
  |    platform.ready().then(() => {
  |      statusBar.styleDefault()
  |      splashScreen.hide()
  |    })
  |  }
  |}
  """.stripMargin().trim()
  }

  @Memoized
  Page getRootPage() {
    Page rootPage = project.initialFeature?.initialPage
    if (!rootPage) throw new IllegalStateException(
      "It is needed to decide the initial feature which has a initial page."
    )
    rootPage
  }

  @Override
  String getRelPath() {
    'app/app.component.ts'
  }
}
