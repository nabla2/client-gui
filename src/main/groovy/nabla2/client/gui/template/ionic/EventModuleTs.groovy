package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Event
import nabla2.client.gui.model.EventFilter
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class EventModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |import { Subject, Observable, BehaviorSubject } from 'rxjs'
    |import { Injectable } from '@angular/core'
    |import { ApiCall } from './rest-client.service'
    |import { ViewModel } from '../view-model'
    |import { Store } from '@ngrx/store'
    |
    |/**
    | * 画面上で発火するイベントオブジェクト
    | * @author nabla2.metamodel.generator
    | */
    |export class UIEvent<T extends (Event | ApiResponse)> {
    |  type   :string
    |  id     :string
    |  source :T
    |  index? :number
    |}
    |
    |export class ApiResponse {
    |  apiCall   :ApiCall
    |  response  :Response
    |  jsonBody? :any
    |}
    |
    |/**
    | * イベントハブ
    | */
    |@Injectable()
    |export class UIEventHub {
    |
    |  private _event\$:Subject<UIEvent<any>> = new Subject<UIEvent<any>>()
    |  private model\$:BehaviorSubject<ViewModel> = new BehaviorSubject<ViewModel>(null)
    |
    |  constructor(
    |    public store: Store<{viewModel:ViewModel}>,
    |  ) {
    |    this.store.select('viewModel').subscribe(v => this.model\$.next(v))
    |    this.click\$ = this.event\$.filter(e => e.type === 'click').map(e => <UIEvent<Event>>e)
    |    this.change\$ = this.event\$.filter(e => e.type === 'change').map(e => <UIEvent<Event>>e)
    |    this.pageInitialized\$ = this.event\$.filter(e => e.type === 'page_initialized').map(e => <UIEvent<Event>>e)
    |    this.apiResponse\$ = this.event\$.filter(e => e.type === 'api_response').map(e => <UIEvent<ApiResponse>>e)
    |    this.jsonResponse\$ = this.apiResponse\$.filter(e => {
    |      const response = e.source.response
    |      const status = response.status
    |      const contentType = response.headers.get("content-type")
    |      return contentType && contentType.includes('json')
    |        && (200 <= status && status < 300 || 400 <= status && status < 500)
    |    })
    |    .flatMap(e => {
    |      const response = e.source.response
    |      const apiCall = e.source.apiCall
    |      return response.json().then(json => {
    |        return {
    |          ...e,
    |          source: {
    |            apiCall: apiCall,
    |            response: response,
    |            jsonBody: json,
    |          },
    |        }
    |      })
    |    })
    |    .share()
    |  }
    |
    |  next(event:UIEvent<any>):void {
    |    this._event\$.next(event)
    |  }
    |
    |  /**
    |   * 全てのイベントからなるストリーム
    |   */
    |  get event\$():Observable<UIEvent<any>> {
    |    return this._event\$
    |  }
    |
    |  /**
    |   * 現在の表示モデルを取得する
    |   */
    |  get model():ViewModel {
    |    return this.model\$.getValue()
    |  }
    |
    |  /**
    |   * 画面の各ウィジェットをクリックした場合に発火するイベントストリーム
    |   */
    |  click\$:Observable<UIEvent<Event>>
    |
    |  /**
    |   * 画面の各ウィジェットの内容を変更した場合に発火するイベントストリーム
    |   */
    |  change\$:Observable<UIEvent<Event>>
    |
    |  /**
    |   * 各画面の初期処理が完了した時点で発火するイベントストリーム
    |   */
    |  pageInitialized\$:Observable<UIEvent<Event>>
    |
    |  /**
    |   * APIリクエストに対するレスポンスが返された時点で発火するイベントストリーム
    |   */
    |  apiResponse\$:Observable<UIEvent<ApiResponse>>
    |
    |  /**
    |   * APIリクエストに対し、content-type='appliction/json'のレスポンスが返された場合に
    |   * json bodyのパース処理完了後に発火するイベントストリーム
    |   */
    |  jsonResponse\$:Observable<UIEvent<ApiResponse>>
    |
    ${project.events.collect{event -> """\
    |  /**
    |   * ${event.name.literal.get()}
    ${event.comment.value.map{ comment -> """\
    |   *
    |   * ${comment.replace('\n', '\n|   * ')}
    """.trim()}.orElse('')}
    |   */
    |  ${event.identifier.lowerCamelized.get()} = (${paramsFor(event)}):Observable<any> => {
    |    ${event.observable.get().replaceAll('\n', '\n|    ')}
    |  }
    """.trim()}.join('\n')}
    |
    ${project.eventFilters.collect{filter -> """\
    |  /**
    |   * ${filter.name.literal.get()}
    ${filter.comment.value.map{ comment -> """\
    |   *
    |   * ${comment.replace('\n', '\n|   * ')}
    """.trim()}.orElse('')}
    |   */
    |  ${filter.identifier.lowerCamelized.get()} = (${paramsFor(filter)}):(event:UIEvent<Event>) => boolean => {
    |    return event => {
    |      const model = this.model
    |      ${filter.filterFunction.get().replaceAll('\n', '\n|' + ' '*6)}
    |    }
    |  }
    """.trim()}.join('\n')}
    |}
    """.stripMargin().trim()
  }

  static String paramsFor(EventFilter filter) {
    filter.name.params.collect{it.tokenize(':').with{it[1] + ':' + it[2..-1].join(':')}}.join(', ')
  }

  static String paramsFor(Event event) {
    event.name.params.collect{it.tokenize(':').with{it[1] + ':' + it[2..-1].join(':')}}.join(', ')
  }

  @Override
  String getRelPath() {
    'services/event.service.ts'
  }
}
