package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.PageItemType
import nabla2.generator.TextFileTemplate

@Canonical
class WidgetTemplateHtml implements TextFileTemplate {

  PageItemType type

  @Override
  String getSource() {
    """\
    |<!--
    | ${type.name.literal.get().replaceAll(/\s+/, '').replaceAll(/【[^【】]*】/, '')}テンプレートHTML
    |
    | ${regenerationMark}
    |-->
    |${template}
    """.trim().stripMargin()
  }

  String getTemplate() {
    type.template
        .getValue()
        .map{ it.replaceAll('\n', '\n|') }
        .orElse("<div>&lt;${selector}&gt; is not implemeted!!!</div>")
  }

  private String getSelector() {
    'app-' + type.identifier.dasherized.get()
  }

  @Override
  String getRelPath() {
    String widgetName = type.identifier.dasherized.get()
    "widgets/${type.componentPath.get()}/${widgetName}/${widgetName}.html"
  }
}
