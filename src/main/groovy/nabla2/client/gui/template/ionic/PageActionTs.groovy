package nabla2.client.gui.template.ionic

import groovy.transform.Canonical
import nabla2.client.gui.model.Action
import nabla2.client.gui.model.Page
import nabla2.generator.TextFileTemplate

@Canonical
class PageActionTs implements TextFileTemplate {

  Page page

  @Override
  String getSource() {
    """\
    |import { Observable } from 'rxjs'
    |import { Effect, Actions } from '@ngrx/effects'
    |import { Injectable } from '@angular/core'
    |import { App } from 'ionic-angular'
    |import { UIAction, ActionCreator } from '../../../../services/action.service'
    |import { ViewModel } from '../../../../view-model'
    |import { apiCalls } from './${page.ngIdentifier}.api-call.page'
    ${page.pagesMovesTo.collect{ toPage -> """\
    |import { ${toPage.className} } from '../../../../features/${toPage.feature.ngIdentifier}/pages/${toPage.ngIdentifier}/${toPage.ngIdentifier}.component.page'
    """.trim()}.join('\n')}
    |
    |/**
    | * ${page.name}ページアクション定義
    | * ${regenerationMark}
    | */
    |@Injectable()
    |export class ${page.className}Effects {
    |
    |  /** 画面名 */
    |  pageName:string = '${page.ngIdentifier}'
    |
    |  /** 機能名 */
    |  featureName:string = '${page.feature.ngIdentifier}'
    |
    |  /**
    |   * コンストラクタ
    |   * @param navCtrl 画面遷移コントローラ
    |   *  
    |   */
    |  constructor(
    |    public action\$: Actions,
    |    public app: App,
    |    public actionCreator: ActionCreator,
    |  ) {
    |  }
    |
    |  /**
    |   * ページ内アクション定義
    |   */
    |  @Effect() effect():Observable<UIAction> {
    |    return this.epic(<Observable<any>>this.action\$)
    |  }
    |
    |  private epic = ${epicDefinitionJs}
    |}
    """.stripMargin().trim()
  }

  String getEpicDefinitionJs() {
    """\
    |(action\$:Observable<UIAction>):Observable<UIAction> => {
    |  return action\$
    |  .flatMap((action:UIAction) => {
    |    switch (action.type) {
    ${page.eventNames.collect{ eventName -> """\
    |      case `${page.eventPathFor(eventName)}`:
    |        return [
    ${page.findBehaviorsByEventDesc(eventName).collect{ behavior ->
      Action action = behavior.action
    """\
    |          this.actionCreator.${action.identifier.lowerCamelized.get()}(
    ${behavior.actionParams.with { params -> (params.empty) ? '' : """\
    |            ${params.collect{it.value}.join(',\n|' + ' '*12)},
    """.trim()}}
    |            action.event,
    |            `${behavior.actionDesc}`,
    |          ),
    """.trim()}.join('\n')}
    |        ]
    """.trim()}.join('\n')}
    |      default:
    |        return []
    |    }
    |  })
    |}
    """.stripMargin()
  }

  @Override
  String getRelPath() {
    "features/${page.feature.ngIdentifier}/pages/${page.ngIdentifier}/${page.ngIdentifier}.effects.page.ts"
  }
}
