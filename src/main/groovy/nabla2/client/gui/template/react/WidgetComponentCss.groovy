package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.PageItemType
import nabla2.generator.TextFileTemplate

@Canonical
class WidgetComponentCss implements TextFileTemplate {

  PageItemType type

  @Override
  String getSource() {
  """\
  |/**
  | * ${type.name.literal.get().replaceAll(/\s+/, '').replaceAll(/【[^【】]*】/, '')}スタイルCSS
  | *
  | * ${regenerationMark}
  | */
  |${type.style.value.orElse('')}
  """.trim().stripMargin()
  }

  @Override
  String getRelPath() {
    "widgets/${type.componentPath}/${type.className}.css"
  }
}
