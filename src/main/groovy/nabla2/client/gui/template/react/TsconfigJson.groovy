package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class TsconfigJson implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |{
    |  "//": "${regenerationMark}",
    |  "compilerOptions": {
    |    "outDir": "./dist/",
    |    "sourceMap": true,
    |    "noImplicitAny": true,
    |    "module": "commonjs",
    |    "target": "es2015",
    |    "jsx": "react"
    |  },
    |  "include": [
    |    "./src/**/*"
    |  ]
    |}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "tsconfig.json"
  }
}
