package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.PageItemType
import nabla2.generator.TextFileTemplate

@Canonical
class WidgetComponentTsx implements TextFileTemplate {

  PageItemType type

  @Override
  String getSource() {

    if (type.listItem.truthy) {
      """\
      |import * as React from 'react'
      |import {WidgetProps, IndexedWidgetProps} from '../../core'
      |import * as app from '../../widget'
      |const S = require('./${type.className}.css')
      |
      |export interface ${type.className}Props extends WidgetProps {
      ${type.additionalProps.collect{prop -> """\
      |  ${prop.name}: ${prop.type} // ${prop.description}
      """.trim()}.join('\n')}
      |}
      |
      |/**
      | * ${type.name.literal.get().replaceAll(/\n/, '\n| *')}
      | * ${regenerationMark}
      | */
      |export const ${type.className} = ({propsAt, index}:IndexedWidgetProps<${type.className}Props>):JSX.Element => {
      |  if (index == null) return null
      |  const props = propsAt(index)
      |  const {${type.propsNames.findAll{it != 'index'}.join(', ')}} = props
      |  ${type.template.get().trim().replaceAll(/\n/, '\n|  ')}
      |}
      """.stripMargin().trim()
    }
    else if (type.state.empty) {
      """\
      |import * as React from 'react'
      |import {WidgetProps} from '../../core'
      |import * as app from '../../widget'
      |const S = require('./${type.className}.css')
      |
      |export interface ${type.className}Props extends WidgetProps {
      ${type.additionalProps.collect{prop -> """\
      |  ${prop.name}: ${prop.type} // ${prop.description}
      """.trim()}.join('\n')}
      |}
      |
      |/**
      | * ${type.name.literal.get().replaceAll(/\n/, '\n| *')}
      | * ${regenerationMark}
      | */
      |export const ${type.className} = (props:${type.className}Props):JSX.Element => {
      |  const {${type.propsNames.join(', ')}} = props
      |  ${type.template.get().trim().replaceAll(/\n/, '\n|  ')}
      |}
      """.stripMargin().trim()
    }
    else {
      """\
      |import * as React from 'react'
      |import {Observable, BehaviorSubject} from 'rxjs'
      |import {RxComponent, WidgetProps} from '../../core'
      |import * as app from '../../widget'
      |const S = require('./${type.className}.css')
      |
      |export interface ${type.className}Props extends WidgetProps {
      ${type.additionalProps.collect{prop -> """\
      |  ${prop.name}: ${prop.type} // ${prop.description}
      """.trim()}.join('\n')}
      |  viewModelPath: string // ステートがバインドされている表示モデルのパス
      |  errorMessages: {modelPath:string, message:string}[] // エラーメッセージ
      |}
      |
      |/**
      | * ${type.name.literal.get().replaceAll(/\n/, '\n| *')}
      | * ${regenerationMark}
      | */
      |const render = (props\$:Observable<${type.className}Props>) => {
      |  const state\$ = new BehaviorSubject<string>(null)
      |
      |  return props\$.combineLatest(state\$).map(([props, state]) => {
      |    const {${type.propsNames.join(', ')}} = props
      |    ${type.template.get().trim().replaceAll(/\n/, '\n|    ')}
      |  })
      |}
      |export const ${type.className} = RxComponent(render)
      """.stripMargin().trim()
    }
  }

  @Override
  String getRelPath() {
    "widgets/${type.componentPath}/${type.className}.tsx"
  }
}
