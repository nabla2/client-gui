package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Feature
import nabla2.generator.TextFileTemplate

@Canonical
class FeatureComponentTsx implements TextFileTemplate {

  Feature feature

  @Override
  String getSource() {
    """\
    |import * as React from 'react'
    |import {Route, Redirect, Switch} from 'react-router-dom'
    ${feature.pages.collect{ page -> """\
    |import {${page.className}} from './pages/${page.ngIdentifier}/${page.className}'
    """.trim()}.join('\n')}
    |import {AppProps} from '../../core'
    |import {ViewModel} from '../../view-model'
    |
    |/**
    | * ${feature.name}機能モジュール
    | * ${regenerationMark}
    | */
    |export const ${feature.className} = (props:AppProps) => {
    |  return <div>
    |    <h3>${feature.name}</h3>
    |    <Switch>
    ${feature.initialPage ? """
    |    <Redirect from="${feature.routingPath}" exact to="${feature.initialPage.routingPath}" />
    """ : ''}
    ${feature.pages.collect{ page -> """\
    |    <Route path='${page.routingPath}' exact render={(route) => {
    |      let merged:ViewModel = props.model
    |      const params = route.match.params
    ${page.embeddedParamsBoundModels.collect{viewModel -> """\
    |      merged = ViewModel.update('${viewModel.modelPath}', params.${viewModel.itemKey}, merged)
    """.trim()}.join('\n')} 
    |      return <${page.className} {...props} model={merged} feature='${feature.ngIdentifier}' page='${page.ngIdentifier}' route={route} />
    |    }} />
    """.trim()}.join('\n')}
    |  </Switch>
    |  </div>
    |}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "./${feature.componentDir}/${feature.className}.tsx"
  }
}
