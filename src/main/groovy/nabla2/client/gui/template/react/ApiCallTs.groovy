package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.ApiCall
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ApiCallTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |import {ViewModel} from '../view-model'
    |import {UIEvent} from '../event'
    |
    |/**
    | * REST APIクライアントユーティリティ
    | * ${regenerationMark}
    | */
    ${apiCallClientJs}
    """.stripMargin().trim()
  }

  static String getApiCallClientJs() {
    """\
    |export interface ApiCallProps {
    |  /** API呼び出し名 */
    |  id: string
    |  /** HTTPメソッド */
    |  method: string
    |  /** APIエンドポイントURL */
    |  url: (model:ViewModel) => string
    |  /** リクエストボディ内容を返す関数 */
    |  payload: (model:ViewModel) => any
    |  /** レスポンスボディを画面の表示モデルに反映する関数 */
    |  modelUpdate: (model:ViewModel, body:any) => ViewModel
    |  /** レスポンスボディ中のエラーメッセージを画面の表示モデルに反映する関数 */
    |  errorUpdate: (model:ViewModel, body:any) => ViewModel
    |}
    |
    |export class ApiCall {
    |
    |  props:ApiCallProps
    |
    |  constructor(props:ApiCallProps) {
    |    this.props = props
    |  }
    |
    |  get id():string {
    |    return this.props.id
    |  }
    |
    |  /**
    |   * APIリクエストを送信する。
    |   * @param model 画面表示モデル
    |   */
    |  send(model:ViewModel):void {
    |    const usesBody = (this.props.method !== 'GET')
    |    const headers:Headers = new Headers()
    |    headers.append('Accept', 'application/json, text/plain, */*')
    |    headers.append('Content-Type', 'application/json')
    |    fetch(this.props.url(model), {
    |      method: this.props.method,
    |      headers: headers,
    |      credentials: 'same-origin',
    |      ... usesBody && {body: JSON.stringify(this.props.payload(model))}
    |    }).then(response => {
    |      UIEvent.Hub.next({
    |        type: 'api_response',
    |        id: this.props.id,
    |        source: {
    |          apiCall: this,
    |          response: response,
    |        }
    |      })
    |    }).catch(e => {
    |      console.log(`An error occured while calling the api: \${this.props.id}`)
    |      throw e
    |    })
    |  }
    |
    |  /**
    |   * APIのレスポンス内容を元に画面表示モデルを更新した結果を返す。
    |   * 変更元表示モデルの状態は不変である。
    |   * @param model 変更元表示モデル
    |   */
    |  updateWithResponse(model:ViewModel, body:any):ViewModel {
    |    return this.props.modelUpdate(model, body)
    |  }
    |
    |  /**
    |   * APIからユーザエラーが返された場合に、そのレスポンス内容を反映したモデルを返す。
    |   * 変更元表示モデルの状態は不変である。
    |   * @param model 変更元表示モデル
    |   */
    |  updateWithUserError(model:ViewModel, body:any):ViewModel {
    |    return this.props.errorUpdate(model, body)
    |  }
    |}
    """.trim()
  }

  @Override
  String getRelPath() {
    "core/ApiCall.ts"
  }
}
