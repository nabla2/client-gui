package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Event
import nabla2.client.gui.model.EventFilter
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class EventModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |import {Subject, Observable} from 'rxjs'
    |import {ApiCall, WidgetProps, PageProps, AppProps} from './core'
    |import {ViewModel} from './view-model'
    |
    |/**
    | * 画面上で発火するイベントオブジェクト
    | * @author nabla2.metamodel.generator
    | */
    |export interface UIEvent<T extends (AppProps | ApiResponse)> {
    |  type   :string
    |  id     :string
    |  source :T
    |  index? :number
    |}
    |
    |export interface ApiResponse {
    |  apiCall   :ApiCall
    |  response  :Response
    |  jsonBody? :any
    |}
    |
    |/**
    | * 画面上で発火するイベントのリアクティブストリームを扱うユーティリティ
    | */
    |export namespace UIEvent {
    |  /**
    |   * イベントハブ
    |   */
    |  export class Hub {
    |    static _event\$:Subject<UIEvent<any>> = new Subject<UIEvent<any>>()
    |    public static next(event:UIEvent<any>):void {
    |      this._event\$.next(event)
    |    }
    |    /**
    |     * 全てのイベントからなるストリーム
    |     */
    |    static get event\$():Observable<UIEvent<any>> {
    |      return this._event\$
    |    }
    |  }
    |
    |  /**
    |   * 画面の各ウィジェットをクリックした場合に発火するイベントストリーム
    |   */
    |  export const click\$:Observable<UIEvent<WidgetProps>> =
    |    Hub.event\$.filter(e => e.type === 'click').map(e => <UIEvent<WidgetProps>>e)
    |
    |  /**
    |   * 画面の各ウィジェットの内容を変更した場合に発火するイベントストリーム
    |   */
    |  export const change\$:Observable<UIEvent<WidgetProps>> =
    |    Hub.event\$.filter(e => e.type === 'change').map(e => <UIEvent<WidgetProps>>e)
    |
    |  /**
    |   * 各画面の初期処理が完了した時点で発火するイベントストリーム
    |   */
    |  export const pageInitialized\$:Observable<UIEvent<PageProps>> =
    |    Hub.event\$.filter(e => e.type === 'page_initialized').map(e => <UIEvent<PageProps>>e)
    |
    |  /**
    |   * APIリクエストに対するレスポンスが返された時点で発火するイベントストリーム
    |   */
    |  export const apiResponse\$:Observable<UIEvent<ApiResponse>> =
    |    Hub.event\$.filter(e => e.type === 'api_response').map(e => <UIEvent<ApiResponse>>e)
    |
    |  /**
    |   * APIリクエストに対し、content-type='appliction/json'のレスポンスが返された場合に
    |   * json bodyのパース処理完了後に発火するイベントストリーム
    |   */
    |  export const jsonResponse\$:Observable<UIEvent<ApiResponse>> =
    |    apiResponse\$.filter(e => {
    |      const response = e.source.response
    |      const status = response.status
    |      const contentType = response.headers.get("content-type")
    |      return contentType && contentType.includes('json')
    |        && (200 <= status && status < 300 || 400 <= status && status < 500)
    |    })
    |    .flatMap(e => {
    |      const response = e.source.response
    |      const apiCall = e.source.apiCall
    |      return response.json().then(json => {
    |        return {
    |          ...e,
    |          source: {
    |            apiCall: apiCall,
    |            response: response,
    |            jsonBody: json,
    |          },
    |        }
    |      })
    |    })
    |    .share() 
    |
    ${project.events.collect{event -> """\
    |  /**
    |   * ${event.name.literal.get()}
    ${event.comment.value.map{ comment -> """\
    |   *
    |   * ${comment.replace('\n', '\n|   * ')}
    """.trim()}.orElse('')}
    |   */
    |  export const ${event.identifier.lowerCamelized.get()} = (${paramsFor(event)}):Observable<any> => {
    |    const model\$ = ViewModel.model\$
    |    ${event.observable.get().replaceAll('\n', '\n|    ')}
    |  }
    """.trim()}.join('\n')}
    |
    ${project.eventFilters.collect{filter -> """\
    |  /**
    |   * ${filter.name.literal.get()}
    ${filter.comment.value.map{ comment -> """\
    |   *
    |   * ${comment.replace('\n', '\n|   * ')}
    """.trim()}.orElse('')}
    |   */
    |  export const ${filter.identifier.lowerCamelized.get()} = (${paramsFor(filter)}):(event:UIEvent<AppProps>) => boolean => {
    |    return event => {
    |      const model = ViewModel.model\$.getValue()
    |      ${filter.filterFunction.get().replaceAll('\n', '\n|' + ' '*6)}
    |    }
    |  }
    |
    """.trim()}.join('\n')}
    |}
    """.stripMargin().trim()
  }

  static String paramsFor(EventFilter filter) {
    filter.name.params.collect{it.tokenize(':').with{it[1] + ':' + it[2..-1].join(':')}}.join(', ')
  }


  static String paramsFor(Event event) {
    event.name.params.collect{it.tokenize(':').with{it[1] + ':' + it[2..-1].join(':')}}.join(', ')
  }

  @Override
  String getRelPath() {
    'event.ts'
  }
}
