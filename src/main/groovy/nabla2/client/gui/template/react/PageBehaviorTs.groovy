package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Action
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageBehavior
import nabla2.generator.TextFileTemplate

@Canonical
class PageBehaviorTs implements TextFileTemplate {

  Page page

  @Override
  String getSource() {

    page.apiCalls.find{!it.validMapping}.with {
      if (it) throw new IllegalStateException(
        "Incompatiple mapping between the view model item:${it.viewModelItem.name} " +
        "and the api data item ${it.dataItem.name}"
      )
    }

    """\
    |import {Observable} from 'rxjs'
    |import {UIEvent} from '../../../../event'
    |import {UIAction} from '../../../../action'
    |import {ApiCall} from '../../../../core'
    |import {ViewModel} from '../../../../view-model'
    |
    |/**
    | * ページ内API呼び出し定義
    | */
    |const apiCalls = ${page.apiDefinitionJs()}
    |
    |/**
    | * ページ内アクション定義
    | * ${regenerationMark}
    | */
    |export const epic = ${epicDefinitionJs}
    |
    |/**
    | * ページ内イベント定義
    | */
    |export const event = (type:string):Observable<UIEvent<any>> => {
    |  switch (type) {
    ${page.eventNames.collect{ eventName ->
      PageBehavior behavior = page.pageBehaviors.find {
        it.eventDesc.sameAs(eventName[0]) &&
        (it.eventFilter ? it.filterCondition.sameAs(eventName[1]) : eventName.size() == 1)
      }
      if (!behavior) throw new IllegalStateException(
        "Unknown event: ${eventName}"
      )
    """\
    |    case '${eventName.join(' ')}' :
    |      return UIEvent.${behavior.event.identifier.lowerCamelized.get()}(
    |        ${behavior.eventParams.collect{it.value}.join(',\n|' + ' '*10)}
    |      )
    ${behavior.eventFilter ? """\
    |     .filter(UIEvent.${behavior.eventFilter.identifier.lowerCamelized.get()}(
    |        ${behavior.filterParams.collect{it.value}.join(',\n|' + ' '*10)}
    |     ))
    """ : ''}
    """.trim()}.join('\n').replaceAll(/\([|\s\n]+\)/, '()')}
    |    default:
    |      return Observable.never()
    |  }
    |}
    """.replaceAll(/\n(\s*\n)+/, '\n').stripMargin().trim()
  }

  String getEpicDefinitionJs() {
    """\
    |(action\$:Observable<UIAction>):Observable<UIAction> => {
    |  return action\$
    |  .flatMap((action:UIAction) => {
    |    switch (action.type) {
    ${page.eventNames.collect{ eventName -> """\
    |      case '${page.eventPathFor(eventName)}':
    |        return [
    ${page.findBehaviorsByEventDesc(eventName).collect{ behavior ->
      Action action = behavior.action
    """\
    |          UIAction.${action.identifier.lowerCamelized.get()}(
    ${behavior.actionParams.with { params -> (params.empty) ? '' : """\
    |            ${params.collect{it.value}.join(',\n|' + ' '*12)},
    """.trim()}}
    |            action.event,
    |            '${behavior.actionDesc}',
    |          ),
    """.trim()}.join('\n')}
    |        ]
    """.trim()}.join('\n')}
    |      default:
    |        return []
    |    }
    |  })
    |}
    """.stripMargin()
  }

  @Override
  String getRelPath() {
    "${page.componentDir}/${page.className}Behavior.ts"
  }
}
