package nabla2.client.gui.template.react

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.ViewModelItem
import nabla2.generator.TextFileTemplate

@Canonical
class FeatureViewModelTs implements TextFileTemplate {

  Feature feature

  @Override
  String getSource() {
  """\
  |/**
  | * ${feature.name}機能表示モデル
  | * ${regenerationMark}
  | */
  |export interface ${className} {
  ${topLevelItems.collect{ item -> """\
  ${renderInterfaceOf(item)}
  """.trim()}.join('\n')}
  |}
  |
  |/**
  | * ${feature.name}機能表示モデル初期状態
  | */
  |export const ${initialStateName}:${className} = {
  ${topLevelItems.collect{ item -> """\
  ${renderInitialStateOf(item)},
  """.trim()}.join('\n')}
  |}
  """.stripMargin().trim()
  }

  static String renderInitialStateOf(ViewModelItem item, int offset = 2, withIdentifier = true) {
    String pad = ' ' * offset
    String header = withIdentifier ? (item.identifier + ': ')
                                   : ''
    if (item.children.empty) {
      """\
      |${pad}${header}${initialValueOf(item)}, // ${item.name}
      """.trim()
    }
    else {
      boolean isArray = item.dataType.get().trim().startsWith('[')
      return isArray ? """\
      |${pad}${header}${initialValueOf(item).replace('\n', "\n|${pad}")}, // ${item.name}
      """.trim() : """\
      |${pad}${header}{ // ${item.name}
      ${item.children.collect{child -> renderInitialStateOf(child, offset + 2).trim()}.join('\n')},
      |${pad}}
      """.trim()
    }
  }

  static String initialValueOf(ViewModelItem item) {
    if (!item.defaultValue.empty) {
      return item.defaultValue
    }
    String dataType = item.dataType.get().trim()
    if (dataType.startsWith('[')) {
      return '[]'
    }
    if (dataType.startsWith('{')) {
      return '{}'
    }
    switch (dataType) {
      case 'string':
        return "''"
      case 'number':
        return "0"
      case 'boolean':
        return "false"
      default:
        return 'null'
    }
  }

  static String renderInterfaceOf(ViewModelItem item, int offset = 2, withIdentifier = true) {
    String pad = ' ' * offset
    String header = withIdentifier ? (item.identifier + ': ')
                                   : ''
    if (item.children.empty) {
      """\
      |${pad}${header}${item.dataType} // ${item.name}
      """.trim()
    }
    else {
      boolean isArray = item.dataType.get().trim().startsWith('[')
      return isArray ? """\
      |${pad}${header}// ${item.name}
      ${item.children.first().with{child -> renderInterfaceOf(child, offset + 2, false)}}[]
      """.trim() : """\
      |${pad}${header}{ // ${item.name}
      ${item.children.collect{child -> renderInterfaceOf(child, offset + 2).trim()}.join('\n')}
      |${pad}}
      """.trim()
    }
  }

  @Memoized
  List<ViewModelItem> getTopLevelItems() {
    feature.viewModel.findAll{ it.parent == null }
  }

  @Memoized
  String getClassName() {
    feature.identifier.upperCamelized.get() + 'ViewModel'
  }

  @Memoized
  String getInitialStateName() {
    feature.identifier.upperCamelized.get() + 'InitialState'
  }

  @Override
  String getRelPath() {
    "${feature.componentDir}/${feature.ngIdentifier}-view-model.ts"
  }
}
