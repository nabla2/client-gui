package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class IndexHtml implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |<!DOCTYPE html>
  |<!-- ${regenerationMark} -->
  |<html>
  |<head>
  |  <meta charset="utf-8" />
  |  <title>${project.name}</title>
  |</head>
  |<body>
  |  <div id="main">Loading...</div>
  |  <script src="./dist/bundle.js"></script>
  |</body>
  |</html>
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'index.html'
  }
}
