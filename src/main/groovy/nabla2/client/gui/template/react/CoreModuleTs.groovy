package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class CoreModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import * as React from 'react'
  |import {AnyAction, Dispatch} from 'redux'
  |import {Observable, BehaviorSubject, Subject, Subscription} from 'rxjs'
  |
  |import {ViewModel} from './view-model'
  |import {UIEvent} from './event'
  |import {UIAction} from './action'
  |
  |/**
  | * 共通モジュール
  | * ${regenerationMark}
  | */
  |
  |export {ApiCall} from './core/ApiCall'
  |
  |export interface AppProps {
  |  model    :ViewModel
  |  dispatch :Dispatch<any>
  |}
  |
  |export interface PageProps extends AppProps {
  |  feature :string
  |  page    :string
  |  route   :{history:any, location:any}
  |}
  |
  |export interface PageState {
  |  unload\$: Subject<any>
  |}
  |
  |export interface WidgetProps extends PageProps {
  |  id        :string
  |  title     :string
  |  on        :(eventType:string, index?:number) => void
  |  update    :(value:any, eventType:string, index?:number) => void
  |  children? :any //cannot narrow
  |}
  |
  |export interface IndexedWidgetProps<T extends WidgetProps> {
  |  index?  :number
  |  propsAt :(index:number) => T
  |}
  |
  |/**
  | * 画面コンポーネント
  | */
  |export abstract class PageComponent extends React.Component<PageProps, PageState> {
  |  /**
  |   * コンストラクタ
  |   * @param props タグ経由で渡された属性値
  |   */
  |  constructor(props:PageProps) {
  |    super(props)
  |    this.state = {
  |      unload\$: new Subject()
  |    }
  |  }
  |
  |  /**
  |   * この画面のイベント定義に従って対応するイベントストリームの監視を開始する。
  |   * @param events イベント定義
  |   * @param eventNames この画面のイベント論理名
  |   */
  |  subscribe(events:(name:string) => Observable<UIEvent<any>>, eventNames:string[]) {
  |    eventNames.forEach(eventName => {
  |      events(eventName)
  |      .takeUntil(this.state.unload\$)
  |      .subscribe(e => this.props.dispatch({
  |        type  :`\${this.props.feature}/\${this.props.page}/\${eventName}`,
  |        event :e,
  |        desc  :eventName,
  |      }))
  |    })
  |    UIEvent.Hub.next({
  |      type   :'page_initialized',
  |      id     :`\${this.props.feature}/\${this.props.page}`,
  |      source :this.props,
  |    })
  |  }
  |
  |  /**
  |   * 画面内の各UI部品に引き継ぐプロパティを生成する。
  |   */
  |  commonProps():any {
  |    const dispatch = this.props.dispatch
  |    const model = this.props.model
  |    const props:any = {
  |      ...this.props,
  |      on(eventType:string, index?:number) {
  |        UIEvent.Hub.next({
  |          id: this.id,
  |          type: eventType,
  |          source: this,
  |          index: index,
  |        })
  |      },
  |      update(v:any, eventType:string, index?:number) {
  |        const e = {
  |          id: this.id,
  |          type: eventType,
  |          source: this,
  |          index: index,
  |        }
  |        dispatch(
  |          UIAction.updateViewModel(
  |            this.viewModelPath, (m) => v, e, `update model using \${this.id}`
  |          )
  |        )
  |      }
  |    }
  |    return props
  |  }
  |
  |  /**
  |   * ページコンポーネントおよび配下の各コンポーネントが監視しているストリームを全て終端させる。
  |   */
  |  componentWillUnmount():void {
  |    this.state.unload\$.next()
  |    this.state.unload\$.complete()
  |  }
  |}
  |
  |/**
  | * BehaviorSubjectにより内部ステートを管理する関数型コンポーネントを作成するユーティリティ
  | * Observable<props:any> => Observable<JSX.Element> からコンポーネントを作成する
  | * @param render
  | */
  |export const RxComponent = <P extends {}>(render:(props\$:Observable<P>) => Observable<JSX.Element>):((props:P) => JSX.Element) =>  {
  |
  |  interface ComponentState {
  |    vdom         :JSX.Element
  |    props\$      :BehaviorSubject<P>
  |    subscription :Subscription
  |  }
  |
  |  class ObservableBackedComponent extends React.Component<P, ComponentState> {
  |
  |    constructor(props:P, context:any) {
  |      super(props, context)
  |      const props\$ = new BehaviorSubject<P>(props)
  |      this.state = {
  |        vdom         :null,
  |        props\$       :props\$,
  |        subscription :null,
  |      }
  |    }
  |
  |    get componentState():ComponentState {
  |      return this.state
  |    }
  |
  |    componentWillMount():void {
  |      const subscription = render(this.state.props\$).subscribe(vdom => this.setState({vdom:vdom}))
  |      this.setState({subscription:subscription})
  |    }
  |
  |    componentWillReceiveProps(nextProps:P):void {
  |      this.componentState.props\$.next(nextProps)
  |    }
  |
  |    componentWillUnmount():void {
  |      this.componentState.subscription.unsubscribe()
  |      this.componentState.props\$.unsubscribe()
  |    }
  |
  |    render():JSX.Element {
  |      return this.state.vdom
  |    }
  |  }
  |
  |  return (props:P) => {
  |    return React.createElement(ObservableBackedComponent, props)
  |  }
  |}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "core.ts"
  }
}

