package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class ViewModelTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |import {BehaviorSubject} from 'rxjs'
    ${project.features.collect{feature -> """\
    |import {${classNameOf(feature)}ViewModel, ${classNameOf(feature)}InitialState} from './${feature.componentDir}/${feature.ngIdentifier}.module'
    """.trim()}.join('\n')}
    |/**
    | * アプリケーション表示モデル
    | * ${regenerationMark}
    | */
    |export interface ViewModel {
    ${project.features.collect{feature -> """\
    |  ${feature.identifier}: ${classNameOf(feature)}ViewModel
    """.trim()}.join('\n')}
    |  errorMessages: {modelPath:string, message:string}[]
    |  codeDefinitions: {[category:string]:{label:string,value:string}[]}
    |} 
    |
    |/**
    | * アプリケーションステータス管理用ユーティリティ
    | */
    |export namespace ViewModel {
    |
    |  /**
    |   * アプリケーション初期状態
    |   */
    |  const InitialState:ViewModel = {
    ${project.features.collect{feature -> """\
    |    ${feature.identifier}: ${classNameOf(feature)}InitialState,
    """.trim()}.join('\n')}
    |    errorMessages: [],
    |    codeDefinitions: {},
    |  }
    |
    |  /**
    |   * 表示モデルのリアクティブストリーム
    |   */
    |  export const model\$:BehaviorSubject<ViewModel> = new BehaviorSubject(InitialState)
    |
    |  /**
    |   * データ断面(パス毎に管理)
    |   */
    |  const SNAPSHOTS:{[path:string]:any} = {}
    |
    |  /**
    |   * アプリケーション初期状態を取得する
    |   */
    |  export const getInitialState = ():ViewModel => {
    |    return JSON.parse(JSON.stringify(InitialState))
    |  }
    |
    |  /**
    |   * アプリケーション初期状態の指定されたパス上の値を取得する
    |   * @param path パス
    |   */
    |  export const getInitialValue = (path:string):any => {
    |    const state = get(path, InitialState)
    |    return JSON.parse(JSON.stringify(state))
    |  }
    |
    |  /**
    |   * 指定されたモデルの指定されたパス上の値を取得する
    |   *
    |   * @param path パス
    |   * @param model モデル
    |   * @param nullSafeValue 対応するパス上に値が存在しない場合に返される値。デフォルトではnullが返される。
    |   */
    |  export const get = (path:string, model:ViewModel, nullSafeValue:any=null):any => {
    |    if (!path) return model
    |    if (model == null) return nullSafeValue
    |    const segments = path.split('.')
    |    let node:any = model
    |    while (segments.length){
    |      const key = segments.shift()
    |      node = node[key]
    |      if (node == null) return nullSafeValue
    |    }
    |    return node
    |  }
    |
    |  /**
    |   * 指定されたモデルの指定されたパス上の値が更新されたモデルを返す。
    |   * 更新後のモデルは別インスタンス(変更部分以外はシャローコピー)となる。
    |   * @param path パス
    |   * @param diff 更新内容
    |   * @param current モデル
    |   */
    |  export const update = (path:string, diff:any, current:ViewModel):ViewModel => {
    |    if (!path) throw new Error(
    |      'path must not be blank: ' + path
    |    )
    |    const segments = path.split('.')
    |    let root:any = {...current}
    |    let node:any = root
    |    while (segments.length){
    |      const key = segments.shift()
    |      if (!segments.length) {
    |        node[key] = diff
    |      }
    |      else if (!node.hasOwnProperty(key)) {
    |        node[key] = {}
    |      }
    |      node = node[key]
    |    }
    |    return root
    |  }
    |
    |  /**
    |   * 複数のパス上の値を一括更新する。
    |   * 更新後のモデルは別インスタンス(変更部分以外はシャローコピー)となる。
    |   * @param diffs 変更パスと変更値にからなるマップ
    |   * @param current モデル
    |   */
    |  export const updates = (diffs:{[path:string]: any}, current:ViewModel):ViewModel => {
    |    return Object.keys(diffs).reduce((updated, path) => {
    |      const diff = diffs[path]
    |      return ViewModel.update(path, diff, updated)
    |    }, current)
    |  }
    |
    |  /**
    |   * 指定されたパス上のデータ断面を取得する。取得された断面はディープコピーとなる。
    |   * @param path 断面を取得するパス
    |   * @param model 対象モデル
    |   */
    |  export const takeSnapshot = (path:string, model:ViewModel):void => {
    |    const snapshot = JSON.parse(JSON.stringify(get(path, model)))
    |    SNAPSHOTS[path] = snapshot
    |  }
    |
    |  /**
    |   * 指定されたパス上の直近のデータ断面に状態を戻す。
    |   * データ断面が存在しない場合は、アプリケーション初期状態に戻す。
    |   * @param path 対象のパス
    |   * @param current 対象モデル
    |   */
    |  export const revertToLatestSnapshot = (path:string, current:ViewModel):ViewModel => {
    |    return update(path, getSnapshotOf(path), current)
    |  }
    |
    |  /**
    |   * 指定されたパス上の直近のデータ断面に状態を戻す。
    |   * データ断面が存在しない場合は、アプリケーション初期状態を返す。
    |   * @param path 対象のパス
    |   */
    |  export const getSnapshotOf = (path:string):any => {
    |    const snapshot = SNAPSHOTS[path]
    |    return (typeof snapshot === 'undefined')
    |         ? getInitialValue(path)
    |         : JSON.parse(JSON.stringify(snapshot))
    |  }
    |
    |  /**
    |   * 指定されたパス上の直近のデータ断面と現在の状態との間に際があるかどうかを返す。
    |   * @param path 対象のパス
    |   * @param current 現在のモデル
    |   */
    |  export const updatedFromLatestSnapshot = (path:string, current:ViewModel):boolean => {
    |    return JSON.stringify(get(path, current)) != JSON.stringify(getSnapshotOf(path))
    |  }
    |}
    """.stripMargin().trim()
  }

  static String classNameOf(Feature feature) {
    feature.identifier.upperCamelized.get()
  }

  @Override
  String getRelPath() {
    "view-model.ts"
  }
}
