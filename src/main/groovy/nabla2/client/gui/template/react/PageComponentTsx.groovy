package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageItem
import nabla2.generator.TextFileTemplate

@Canonical
class PageComponentTsx implements TextFileTemplate {

  Page page

  @Override
  String getSource() {
    """\
    |import * as React from 'react'
    |import {PageComponent, PageProps} from '../../../../core'
    |import {${widgetTagNames.join(', ')}} from '../../../../widget'
    |import {event} from './${page.className}Behavior'
    |
    |/**
    | * ${page.name}ページコンポーネント
    | * ${regenerationMark}
    | */
    |export class ${page.className} extends PageComponent {
    |  /**
    |   * コンストラクタ
    |   * @param props タグ経由で渡された属性値
    |   */
    |  constructor(props:PageProps) {
    |    super(props)
    |  }
    |
    |  /**
    |   * 画面描画前の初期処理。
    |   * ここでは各イベントストリームの監視を開始する。
    |   */
    |  componentWillMount():void {
    |    this.subscribe(event, [
    ${page.eventNames.collect{ eventName -> """\
    |      '${eventName.join(' ')}',
    """.trim()}.join('\n')}
    |    ])
    |  }
    |
    |  /**
    |   * 描画するVirtualDomを返す。
    |   */
    |  render():JSX.Element {
    |    const viewModel = this.props.model
    |    return <div>
    ${page.topLevelPageItems.collect{ topLevelItem -> """\
    ${renderAsTag(topLevelItem, 6)} 
    """.trim()}.join('\n').replaceAll(/\n\s*\n/, '\n')}
    |    </div>
    |  }
    |}
    """.stripMargin().trim()
  }

  List<String> getWidgetTagNames() {
    page.pageItems.collect{it.tagName}.unique()
  }

  String renderAsTag(PageItem item, int shift = 0) {
    String pad = ' ' * shift
    String tagName = item.tagName
    if (item.type.listItem.truthy) {
      """\
      |${pad}<${tagName}
      |${pad}  propsAt = {(index) => ({
      |${pad}    ...this.commonProps(),
      ${item.props.collect{ prop ->  """\
      |${pad}    ${prop.key}: (index < 0) ? null : ${prop.value.replace('[]', '[index]').replace('\n', "\n|${pad}      ")},
      """}.join('\n')}
      ${item.type.propWithBinding.with{ boundProp ->
        if (!boundProp) return ''
        String boundPath = item.props.find{it.key == boundProp.name}.value
      """\
      |${pad}    viewModelPath: '${boundPath.replaceAll(/^viewModel\./, '')}',
      |${pad}    errorMessages: viewModel.errorMessages.filter(e =>
      |${pad}      e.modelPath === '${boundPath.replaceAll(/^viewModel\./, '')}'
      |${pad}    ),
      """.trim()}}
      |${pad}    title:'${item.name}',
      |${pad}    id:'${item.id}',
      |${pad}  })} ${item.children.empty ? '/>' : '>'}
      ${item.children.with {it.empty ? '' : it.collect { child -> """\
      ${renderAsTag(child, shift + 2)}
      """.trim()}.join('\n')}}
      ${item.children.empty ? '' : """\
      |${pad}</${tagName}>
      """.trim()} 
      """
    }
    else {
      """\
      |${pad}<${tagName}
      |${pad}  {...this.commonProps()}
      ${item.props.collect{ prop -> """\
      |${pad}  ${prop.key} = {${prop.value.replace('\n', "\n|${pad}      ")}}
      """}.join('\n')}
      ${item.type.propWithBinding.with{ boundProp ->
        if (!boundProp) return ''
        String boundPath = item.props.find{it.key == boundProp.name}.value
      """\
      |${pad}  viewModelPath = '${boundPath.replaceAll(/^viewModel\./, '')}'
      |${pad}  errorMessages = {viewModel.errorMessages.filter(e =>
      |${pad}    e.modelPath === "${boundPath.replaceAll(/^viewModel\./, '')}"
      |${pad}  )}
      """.trim()}}
      |${pad}  title='${item.name}'
      |${pad}  id='${item.id}' ${item.children.empty ? '/>' : '>'}
      ${item.children.with {it.empty ? '' : it.collect { child -> """\
      ${renderAsTag(child, shift + 2)}
      """.trim()}.join('\n')}}
      ${item.children.empty ? '' : """\
      |${pad}</${tagName}>
      """.trim()} 
      """
    }
  }

  @Override
  String getRelPath() {
    "${page.componentDir}/${page.className}.tsx"
  }
}
