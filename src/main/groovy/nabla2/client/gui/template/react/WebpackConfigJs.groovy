package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class WebpackConfigJs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |// "${regenerationMark}",
    |const path = require('path')
    |
    |const config = {
    |  entry: ['./src/index.tsx'],
    |  output: {
    |    path: path.resolve(__dirname, 'dist'),
    |    publicPath: '/dist/',
    |    filename: 'bundle.js',
    |  },
    |  devtool: 'source-map',
    |  resolve: {
    |    extensions: ['.ts', '.tsx', '.js', '.json']
    |  },
    |  module: {
    |    rules: [{
    |      test: /\\.tsx?\$/,
    |      use : [ {loader: 'ts-loader'}],
    |      exclude: /node_modules/,
    |    }, {
    |      test: /\\.css\$/,
    |      use: [
    |        { loader: "style-loader" },
    |        { loader: "css-loader", options:{modules:true} }
    |      ]
    |    }]
    |  },
    |  devServer: {
    |    port: 3000
    |  }
    |}
    |module.exports = config
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "webpack.config.js"
  }
}
