package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class IndexTsx implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import 'rxjs/Rx'
  |import * as React from 'react'
  |import * as ReactDOM from 'react-dom'
  |import {createStore, combineReducers, applyMiddleware, Dispatch} from 'redux'
  |import {connect, Provider} from 'react-redux'
  |import {createEpicMiddleware, combineEpics} from 'redux-observable'
  |import {composeWithDevTools} from 'redux-devtools-extension'
  |import createHistory from 'history/createHashHistory'
  |import {HashRouter, withRouter} from 'react-router-dom'
  |import {reducer} from './action'
  |import {ViewModel} from './view-model'
  |import {App} from './App'
  ${project.features.collect{feature -> """\
  |import {epics as ${feature.identifier}_epics} from './${feature.componentDir}/${feature.ngIdentifier}.module'
  """.trim()}.join('\n')}
  |// ${regenerationMark}
  |const history = createHistory()
  |const epicMiddleware = createEpicMiddleware(combineEpics(
  ${project.features.collect{"...${it.identifier}_epics"}.join(', ')}
  |))
  |const store = createStore(
  |  reducer,
  |  composeWithDevTools(applyMiddleware(epicMiddleware)),
  |)
  |store.subscribe(() => ViewModel.model\$.next(store.getState()))
  |const mapStateToProps = (state:ViewModel) => {
  |  return {model :state}
  |}
  |const mapDispatchToProps = (dispatch:Dispatch<any>) => {
  |  return {dispatch :dispatch}
  |}
  |const AppConnectedState = withRouter(connect(mapStateToProps, mapDispatchToProps)(App))
  |
  |ReactDOM.render(
  |  <Provider store={store}>
  |    <HashRouter>
  |      <AppConnectedState />
  |    </HashRouter>
  |  </Provider>,
  |  document.getElementById('main')
  |)
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "index.tsx"
  }
}
