package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class PackageJson implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |{
    |  "//": "${regenerationMark}",
    |  "scripts": {
    |    "build": "webpack --display-error-details",
    |    "start": "webpack-dev-server"
    |  },
    |  "dependencies": {
    |    "history": "^4.7.2",
    |    "react": "^16.0.0",
    |    "react-dom": "^16.0.0",
    |    "react-redux": "^5.0.6",
    |    "react-router-dom": "^4.2.2",
    |    "redux": "^3.7.2",
    |    "redux-observable": "^0.16.0",
    |    "rxjs": "^5.5.0"
    |  },
    |  "devDependencies": {
    |    "@types/history": "^4.6.1",
    |    "@types/react": "^16.0.17",
    |    "@types/react-dom": "^16.0.2",
    |    "@types/react-redux": "^5.0.10",
    |    "@types/react-router-dom": "^4.0.8",
    |    "typescript": "^2.5.3",
    |    "webpack": "^3.8.1",
    |    "webpack-dev-server": "^2.9.3",
    |    "ts-loader": "^3.0.5",
    |    "css-loader": "^0.28.7",
    |    "style-loader": "^0.19.0",
    |    "redux-devtools-extension": "^2.13.2"
    |  }
    |}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "package.json"
  }
}
