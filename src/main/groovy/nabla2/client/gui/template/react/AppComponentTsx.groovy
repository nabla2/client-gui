package nabla2.client.gui.template.react

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class AppComponentTsx implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |import * as React from 'react'
  |import {createStore, Reducer, Action, applyMiddleware, Dispatch} from 'redux'
  |import {connect, Provider} from 'react-redux'
  |import {createEpicMiddleware} from 'redux-observable'
  |import {Route, Redirect, Switch} from 'react-router-dom'
  |import {AppProps} from './core'
  |import {ViewModel} from './view-model'
  ${project.features.collect{feature -> """\
  |import {${feature.className}} from './${feature.componentDir}/${feature.ngIdentifier}.module'
  """.trim()}.join('\n')}
  |
  |/**
  | * アプリケーションコンポーネント
  | * ${regenerationMark}
  | */
  |export const App = (props:AppProps) => {
  |  return <Switch>
  ${project.initialFeature ? """\
  |    <Redirect exact path="/" to="/${project.initialFeature.identifier}" />
  """.trim() : ''}
  ${project.features.collect{feature -> """\
  |    <Route path='/${feature.identifier}' render={() => {
  |      return <${feature.className} {...props} />
  |    }}/>
  """.trim()}.join('\n')}
  |  </Switch>
  |}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "App.tsx"
  }
}
