package nabla2.client.gui.template.react

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.client.gui.model.Feature
import nabla2.generator.TextFileTemplate

@Canonical
class FeatureModuleTs implements TextFileTemplate {

  Feature feature

  @Override
  String getSource() {
  """\
  |// ${regenerationMark}
  |import {Observable} from 'rxjs'
  |import {AnyAction} from 'redux'
  ${feature.pages.collect{page -> """\
  |import {epic as ${page.identifier}_epic} from './pages/${page.ngIdentifier}/${page.className}Behavior'
  """.trim()}.join('\n')}
  |
  |/**
  | * ${feature.name}機能モジュール
  | * ${regenerationMark}
  | */
  |export {${feature.className}} from './${feature.className}'
  |export {${feature.className}ViewModel, ${feature.className}InitialState} from './${feature.ngIdentifier}-view-model'
  |export const epics:((action\$:Observable<AnyAction>) => Observable<AnyAction>)[] = [
  ${feature.pages.collect{page -> """\
  |  ${page.identifier}_epic,
  """.trim()}.join('\n')}
  |]
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "${feature.componentDir}/${feature.ngIdentifier}.module.ts"
  }

}
