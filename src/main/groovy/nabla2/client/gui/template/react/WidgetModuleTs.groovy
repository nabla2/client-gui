package nabla2.client.gui.template.react

import groovy.transform.Canonical
import nabla2.client.gui.model.Project
import nabla2.generator.TextFileTemplate

@Canonical
class WidgetModuleTs implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  /**
   * ウィジェットモジュール定義
   * ${regenerationMark}
   */
  ${project.pageItemTypes.collect{ type -> """\
  |export {${type.tagName}} from './widgets/${type.componentPath}/${type.tagName}'
  """.trim()}.join('\n')}
  """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "widget.ts"
  }
}
