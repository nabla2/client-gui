package nabla2.client.gui.model

import groovy.transform.Memoized
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.datatype.UrlWithPlaceholder
import nabla2.webapi.model.Operation
import nabla2.webapi.model.Report

trait PageTrait<T> {

  abstract Project getProject()
  abstract Feature getFeature()
  abstract StrictJavaIdentifier getIdentifier()
  abstract UrlWithPlaceholder getPath()
  abstract SingleLineText getName()
  abstract List<PageItem> getPageItems()
  abstract List<PageBehavior> getPageBehaviors()
  abstract List<ApiCall> getApiCalls()

  /**
   * ページコンポーネントのクラス名。
   * @return クラス名(upper-camel識別子 + 'Page')
   */
  @Memoized
  String getClassName() {
    identifier.upperCamelized.get() + 'Page'
  }

  /**
   * この画面の関連コンポーネントが配置されるディレクトリパスを返す。
   * @return 関連コンポーネントが配置されるディレクトリパス
   */
  @Memoized
  String getComponentDir() {
    "${feature.componentDir}/pages/${ngIdentifier}"
  }

  /**
   * Angularの標準的な形式の識別子(ハイフン区切り)を返す。
   * @return 識別子(angular標準)
   */
  @Memoized
  String getNgIdentifier() {
    identifier.dasherized.get()
  }

  /**
   * このページの画面項目の内、最上位(=親項目を持たない)項目を返す。
   * @return 画面内の最上位項目
   */
  @Memoized
  List<PageItem> getTopLevelPageItems() {
    pageItems.findAll{ it.topLevel }
  }

  /**
   * この画面がアプリケーション全体の初期遷移先画面であるかどうかを返す。
   * @return アプリケーション全体の初期遷移先画面でればtrue
   */
  boolean isLandingPage() {
    this.feature.initial.truthy && this.initialPage.truthy
  }

  /**
   * この画面のルーティング用のパスを返す
   * @return ルーティング用パス
   */
  @Memoized
  String getRoutingPath() {
    String pagePath = this.path.embedParams {
      ':' + feature.findViewModelItem(it).orElseThrow{ new IllegalArgumentException(
        "The path parameter: ${it} could not be mapped to any model items.${this}"
      )}.itemKey
    }.get()
    "${feature.routingPath}/${pagePath}".replaceAll(/\/\/+/, '/')
  }

  /**
   * パス中の論理名による埋込パラメータを、対応する画面モデルの物理名に置換した文字列を返す。
   * @return 埋め込みパラメータ付きページパス
   */
  String getBindingPath() {
    String pagePath = this.path.embedParams {
      '\\$\\{m.' + feature.findViewModelItem(it).orElseThrow{ new IllegalArgumentException(
        "The path parameter: ${it} could not be mapped to any model items.${this}"
      )}.modelPath + '\\}'
    }.get()
    "${feature.bindingPath}/${pagePath}".replaceAll(/\/\/+/, '/')
  }

  /**
   * 与えられた文字列がJSON表記と考えられるかどうかを返す。
   * 対象文字の先頭部分のみから判断するため、実際にパースできるかどうかはわからない。
   * @param str 検査対象文字列
   * @return JSON表記と考えられる場合はtrue
   */
  boolean mayBeJsonLiteral(String str) {
    str && str.matches(/^([\[{"].*|[-.0-9eE]+|true|false|null)$/)
  }

  /**
   * この画面内で使用されている全ての項目種別の一覧を返す。
   * @return 画面内で使用される項目種別の一覧
   */
  @Memoized
  List<PageItemType> getPageItemTypes() {
    pageItems.collect{ it.type }.unique()
  }

  /**
   * 与えられた項目名に合致する画面内の項目オブジェクトを取得する。
   * 項目名は (親項目名).(項目名)、親項目が無い場合は (項目名) の形式で与える。
   * @param itemName 項目名
   * @param debugInfo デバッグ情報
   * @return 条件に合致する画面項目
   */
  PageItem findPageItem(String itemPath, Object debugInfo = null) {
    List<String> tokens = itemPath.tokenize('.')
    String parentName = (tokens.size() > 1) ? tokens.first() : null
    String childName = tokens.last()
    pageItems.find{
     it.name.sameAs(childName) && (parentName ? it.parent.name.sameAs(parentName) : (it.parent == null))
    }.with { found ->
      if (!found) throw new IllegalStateException(
        "In the page ${name.get()}, there are not any items whose name is: ${itemPath}\n${debugInfo}"
      )
      found
    }
  }

  /**
   * このページ内のアクション・イベント定義中の埋め込みパラメータを評価し、javascriptステートメントに変換する
   * @param param パラメータ定義
   * @return javascriptステートメント
   */
  PageBehaviorProp evalBehaviorParam(Map.Entry<String, String> param, Object debugInfo = '') {
    List<String> tokens = param.key.tokenize(':')
    PageBehaviorProp prop = new PageBehaviorProp(
      type:tokens[0], name:tokens[1], jsType:tokens[2..-1].join(':')
    )
    if (prop.type == '値') {
      prop.context = null
      prop.value = "(viewModel:ViewModel) => (${evaluate(param.value)})"
    }
    else if (prop.type == 'リスト値') {
      prop.context = null
      prop.value = valueWithIndexOf(param.value, debugInfo)
    }
    else if (prop.type == '画面項目名') {
      PageItem item = findPageItem(param.value, debugInfo)
      prop.context = item
      prop.value = item.with {
        "'${it.id}'"
      }
    }
    else if (prop.type == '画面モデル項目') {
      ViewModelItem modelItem = feature.findViewModelItem(param.value).orElseThrow{ new IllegalArgumentException(
        "Unknown model item: ${param.value}\n${debugInfo}"
      )}
      prop.context = modelItem
      prop.value = prop.context.with {
        "'${it.path}'"
      }
    }
    else if (prop.type == '画面モデルリスト項目') {
      ViewModelItem modelItem = feature.findViewModelItem(param.value).orElseThrow{ new IllegalArgumentException(
        "Unknown model item: ${param.value}\n${debugInfo}"
      )}
      prop.context = modelItem
      prop.value = modelItem.with {
        "(index) => `${it.parent.path}.\${index}.${it.itemKey}`"
      }
    }
    else if (prop.type == 'API呼び出し') {
      prop.context = null
      prop.value = "apiCalls['${param.value}']"
    }
    else if (prop.type == '画面名') {
      Page page = feature.findPage(param.value)
      if (!page) throw new IllegalArgumentException(
        "Unknown page name: ${param.value}\n${debugInfo}"
      )
      prop.context = page
      prop.value = "(m:ViewModel) => `${page.bindingPath}`"
    }
    else if (prop.type == '遷移先画面名') { //FIX-ME
      Page page = feature.findPage(param.value)
      if (!page) throw new IllegalStateException(
        "Unknown page name:${param.value}\n${debugInfo}"
      )
      prop.context = page
      prop.value = "(m:ViewModel) => `${page.ngIdentifier}`"
    }
    else {
      throw new IllegalStateException(
      "Unknown prameter type name:${prop.type}, it should be one of 画面項目名、画面モデル項目、API呼び出し、画面名\n${debugInfo}"
      )
    }
    return prop
  }

  /**
   * このページから直接の遷移先となる画面の一覧を返す。
   * @return 遷移先画面の一覧
   */
  List<Page> getPagesMovesTo() {
    pageBehaviors.collectMany {
      it.actionParams.findAll{ it.type.contains('画面名') }
                     .collect{ (Page)it.context }
    }
  }

  String valueWithIndexOf(String expr, Object debugInfo = '') {
    if (mayBeJsonLiteral(expr)) {
      return "(m:ViewModel, i:number) => (${expr})[i]"
    }
    ViewModelItem item = feature.findViewModelItem(expr).orElseThrow{new IllegalArgumentException(
      "Unknown model item: ${expr}\n${debugInfo}"
    )}
    "(m:ViewModel, i:number) => m.${item.parent.modelPath}[i].${item.itemKey}"
  }

  /**
   * このページ内の各画面項目のプロパティ定義式を評価した結果を返す。
   *
   * 文字列が<code>", [, {</code>で開始される場合、もしくは、数値リテラル、<code>true, false, null</code>
   * のいずかに一致する場合はJSONリテラルとみなしてその値をそのまま返す。
   * それ以外の文字列は、同一機能の表示モデル項目の参照とみなされる。
   *
   * @param descriptor プロパティ定義式
   * @param value 値定義式 (論理名:JSON式、論理名:親項目名.参照項目名、論理名:参照項目名)
   * @return このページ内の参照式を評価した結果
   */
  PageItemProp resolveProp(String descriptor, String value, Object debugInfo) {
    List<String> tokens = descriptor.tokenize(':')
    String key = tokens[0]
    String dataType = tokens[1]

    return new PageItemProp(
      key: key,
      dataType: dataType,
      value: evaluate(value, debugInfo),
      reference: false,
      debugInfo: debugInfo,
    )
  }

  String evaluate(String expr, Object debugInfo = null) {
    expr.replaceAll(/([^-=!(){}\[\]<>+*\/&|?:%"',]+|"([^"]*)"|'([^']*)')/) { List<String> m ->
      String token = m[0].trim()
      if (!token) return ' '
      if (m[2] != null) {
        return "'${m[2]}'"
      }
      if (m[3] != null) {
        return "'${m[3]}'"
      }
      if (token.with{it == 'true' || it == 'false' || it == 'null' ||it ==~ /[.0-9]+/}) {
        return token
      }
      if (token == 'コード区分') {
        return "viewModel.codeDefinitions"
      }
      "viewModel.${feature.findViewModelItem(token).orElseThrow{ new IllegalArgumentException("Unknown model path:${m[0]} in ${expr}\n${debugInfo}")}.modelPath}"
    }
  }

  /**
   * この画面内のイベント定義の一覧を返す。
   * 各イベント定義は以下の配列構造となる。
   * <pre>
   * [(イベント), (イベントフィルタ1), (イベントフィルタ2), ...]
   * </pre>
   * @return イベント定義
   */
  @Memoized
  List<List<String>> getEventNames() {
    pageBehaviors.collect {
      [it.eventDesc.get()] + (it.eventFilter ? [it.filterCondition.get()] : [])
    }.unique()
  }

  /**
   * 与えられたイベント記述に対応する画面挙動定義の一覧を返す。
   * @param description イベント記述
   * @return 条件に合致する画面挙動定義
   */
  @Memoized
  List<PageBehavior> findBehaviorsByEventDesc(List<String> description) {
    pageBehaviors.findAll{
      it.eventDesc.sameAs(description[0]) &&
      (it.eventFilter ? it.filterCondition.sameAs(description[1]) : description.size() == 1)
    }
  }

  @Memoized
  Map<String, List<ApiCall>> getApiMappings() {
    def result = apiCalls.groupBy{ it.name.get() }
    result.forEach{ name, mappings ->
      def bodyMappings = mappings.findAll{!it.dataItem}
      bodyMappings.forEach { bodyMapping ->
        if (bodyMapping.requestBody && mappings.findAll{it.requestBody}.size() > 1) throw new IllegalStateException(
          "There must be only one mapping entry when the whole response body was mapped to a view model item.\n${bodyMapping}"
        )
        if (bodyMapping.responseBody && mappings.findAll{it.responseBody}.size() > 1) throw new IllegalStateException(
          "There must be only one mapping entry when the whole response body was mapped to a view model item.\n${bodyMapping}"
        )
      }
    }
    result
  }

  /**
   * ページ内のAPI呼び出し情報を保持するjavascriptコードを返す
   * @return API呼び出し情報を保持するjavascriptコード
   */
  String apiDefinitionJs() {
    """\
    |{
    ${apiMappings.collect{ name, List<ApiCall> mappings ->
      Operation api = mappings.first().api
      String endPointUrl = ApiCall.resolveEndPointUrl(mappings)
      List<ApiCall> responseMappings = mappings.findAll{ it.responseBody && it.viewModelItem }
      List<ApiCall> requestPayloadMappings = mappings.findAll{ it.requestPayload && it.viewModelItem }
    """\
    |  '${name}': new ApiCall({
    |    id: '${name}',
    |    method: '${api.httpMehtod.get().toUpperCase()}',
    |    url: (m:ViewModel) => `${endPointUrl}`,
    |    payload:(model:ViewModel) => ({
    ${requestPayloadMappings.findAll{it.dataItem.topLevel}.collect{mapping -> """\
    |      ${mapping.dataItem.key}: ${ApiCall.mappedViewModelItemAsJs(mapping, mappings, 8)},
    """.trim()}.join('\n')}
    |    }),
    ${responseMappings.with{ it.empty ? """\
    |    modelUpdate: (model:ViewModel, body:any) => model,
    """ : it.any{it.bodyMappedAsAWhole} ? """\
    |    modelUpdate: (model:ViewModel, body:any) => ViewModel.update('${it.first().viewModelItem.path}', body, model),
    """ : """\
    |    modelUpdate:(model:ViewModel, body:any) => ViewModel.updates({
    ${ApiCall.topLevelViewModelMappings(it).collect{ mapping -> """\
    |      '${mapping.viewModelItem.path}': ${ApiCall.mappedDataItemAsJs(mapping, mappings, 8)},
    """.trim()}.join('\n')} 
    |    }, model),
    """}}
    ${requestPayloadMappings.with{it.empty ? """\
    |    errorUpdate: (model:ViewModel, body:any) => model,
    """ : """\
    |    errorUpdate: (model:ViewModel, body:any) => ViewModel.update('errorMessages', body.errors.map((e:any) => {
    |      switch (e.field) {
    ${it.collect{ mapping -> """\
    |        case '${mapping.dataItem.path}': return {modelPath:'${mapping.viewModelItem.path}', message:e.defaultMessage}
    """}.join('\n')}
    |        default: return null
    |      }
    |    }).filter((e:any) => !!e), model),
    """}}
    |  }),
    |
    """.trim()
    }.join('\n')}
    |}
    """.replaceAll(/\n(\s*\n)+/, '\n').stripMargin()
  }

  /**
   * このページのURL埋め込みパラメータに紐づく表示モデルの一覧を返す。
   * @return URL埋め込みパラメータに紐づく表示モデルの一覧
   */
  List<ViewModelItem> getEmbeddedParamsBoundModels() {
    path.embededParamNames.collect{ p ->
      feature.findViewModelItem(p).orElseThrow{
        new IllegalArgumentException("Bound param ${p} for this is not defined in ViewModel.\n${this}")
      }
    }
  }

  String eventPathFor(List<String> trigger) {
    "${feature.ngIdentifier}/${ngIdentifier}/${trigger.join(' ')}"
  }

}