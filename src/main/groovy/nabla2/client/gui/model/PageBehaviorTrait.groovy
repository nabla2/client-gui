package nabla2.client.gui.model

import groovy.transform.Memoized
import nabla2.metamodel.datatype.MultiLineText

trait PageBehaviorTrait<T> {

  abstract Action getAction()
  abstract Event getEvent()
  abstract EventFilter getEventFilter()
  abstract MultiLineText getActionDesc()
  abstract MultiLineText getEventDesc()
  abstract MultiLineText getFilterCondition()
  abstract Page getPage()

  @Memoized
  List<PageBehaviorProp> getActionParams() {
    List<Map.Entry<String, String>> params = action.name.captureWithName(actionDesc.get())
    params.collect{ param -> page.evalBehaviorParam(param, this) }
  }

  @Memoized
  List<PageBehaviorProp> getEventParams() {
    List<Map.Entry<String, String>> params = event.name.captureWithName(eventDesc.get())
    params.collect{ param -> page.evalBehaviorParam(param, this) }
  }

  @Memoized
  List<PageBehaviorProp> getFilterParams() {
    List<Map.Entry<String, String>> params = eventFilter.name.captureWithName(filterCondition.get())
    params.collect{ param -> page.evalBehaviorParam(param, this) }
  }
}