package nabla2.client.gui.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 画面挙動バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class PageBehaviorValidator implements Validator<PageBehavior> {
  Observable<ConstraintViolation<PageBehavior>> validate(Observable<Table> table$) {
    Observable<PageBehavior> entity$ = PageBehavior.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<PageBehavior>> validateUniqueConstraint(Observable<PageBehavior> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        featureName : entity.featureName.value.map{it.toString()}.orElse(null),
        pageName : entity.pageName.value.map{it.toString()}.orElse(null),
        eventDesc : entity.eventDesc.value.map{it.toString()}.orElse(null),
        filterCondition : entity.filterCondition.value.map{it.toString()}.orElse(null),
        actionDesc : entity.actionDesc.value.map{it.toString()}.orElse(null),
      ])
      PageBehavior another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<PageBehavior> duplicated = it.get('__duplicated__')
      new ConstraintViolation<PageBehavior>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, 機能名, 画面名, イベント内容, フィルタ条件, アクション内容',
          '画面挙動',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<PageBehavior>> validateCardinalityConstraint(Observable<PageBehavior> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.page == null) {
        violations.add(new ConstraintViolation<PageBehavior>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '画面',
            "プロジェクト名=${entity.projectName}, 機能名=${entity.featureName}, 名称=${entity.pageName}",
            entity,
          )
        ))
      }
      if (entity.event == null) {
        violations.add(new ConstraintViolation<PageBehavior>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'イベント',
            "プロジェクト名=${entity.projectName}, 名称=${entity.eventDesc}",
            entity,
          )
        ))
      }
      if (!entity.filterCondition.sameAs('NO_FILTER') &&entity.eventFilter == null) {
        violations.add(new ConstraintViolation<PageBehavior>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'イベントフィルタ',
            "プロジェクト名=${entity.projectName}, 名称=${entity.filterCondition}",
            entity,
          )
        ))
      }
      if (entity.action == null) {
        violations.add(new ConstraintViolation<PageBehavior>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'アクション',
            "プロジェクト名=${entity.projectName}, 名称=${entity.actionDesc}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<PageBehavior>> validatePropertyType(Observable<PageBehavior> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.featureName.messageIfInvalid.map{['機能名', it]}.orElse(null),
        entity.pageName.messageIfInvalid.map{['画面名', it]}.orElse(null),
        entity.eventDesc.messageIfInvalid.map{['イベント内容', it]}.orElse(null),
        entity.filterCondition.messageIfInvalid.map{['フィルタ条件', it]}.orElse(null),
        entity.actionDesc.messageIfInvalid.map{['アクション内容', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<PageBehavior>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<PageBehavior>> validateRequiredProperty(Observable<PageBehavior> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.featureName.literal.map{''}.orElse('機能名'),
        entity.pageName.literal.map{''}.orElse('画面名'),
        entity.eventDesc.literal.map{''}.orElse('イベント内容'),
        entity.filterCondition.literal.map{''}.orElse('フィルタ条件'),
        entity.actionDesc.literal.map{''}.orElse('アクション内容'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<PageBehavior>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}