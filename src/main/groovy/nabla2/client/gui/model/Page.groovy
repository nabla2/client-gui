package nabla2.client.gui.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.client.gui.model.PageTrait
import nabla2.metamodel.datatype.BooleanFlag
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.datatype.UrlWithPlaceholder
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.client.gui.model.Project
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.PageItem
import nabla2.client.gui.model.PageBehavior
import nabla2.client.gui.model.ApiCall
/**
 * 画面
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Page implements PageTrait<Page> {

  /** 論理名 */
  static final String ENTITY_NAME = "画面"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * 機能名
   */
  SingleLineText featureName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * パス
   */
  UrlWithPlaceholder path
  /**
   * コメント
   */
  MultiLineText comment
  /**
   * 初期画面
   */
  BooleanFlag initialPage

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * プロジェクト
   */
  @Memoized
  @JsonIgnore
  Project getProject() {
    Project.from(_table$).filter {
      this.projectName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 機能
   */
  @Memoized
  @JsonIgnore
  Feature getFeature() {
    Feature.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.featureName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 画面項目リスト
   */
  @Memoized
  @JsonIgnore
  List<PageItem> getPageItems() {
    PageItem.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.featureName.sameAs(it.featureName) &&
      this.name.sameAs(it.pageName)
    }.toList().blockingGet()
  }
  /**
   * 画面挙動リスト
   */
  @Memoized
  @JsonIgnore
  List<PageBehavior> getPageBehaviors() {
    PageBehavior.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.featureName.sameAs(it.featureName) &&
      this.name.sameAs(it.pageName)
    }.toList().blockingGet()
  }
  /**
   * API呼び出しリスト
   */
  @Memoized
  @JsonIgnore
  List<ApiCall> getApiCalls() {
    ApiCall.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.featureName.sameAs(it.featureName) &&
      this.name.sameAs(it.pageName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Page> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Page(
            projectName : new SingleLineText(row['プロジェクト名']),
            featureName : new SingleLineText(row['機能名']),
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            path : new UrlWithPlaceholder(row['パス']),
            comment : new MultiLineText(row['コメント']),
            initialPage : new BooleanFlag(row['初期画面']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}