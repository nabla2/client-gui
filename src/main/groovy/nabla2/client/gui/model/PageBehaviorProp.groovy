package nabla2.client.gui.model

import groovy.transform.Canonical

/**
 * イベント・アクション記述中の埋め込みパラメータ
 */
@Canonical
class PageBehaviorProp {
  String type
  String name
  String jsType
  Object context
  String value
}
