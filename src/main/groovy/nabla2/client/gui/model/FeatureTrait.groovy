package nabla2.client.gui.model

import groovy.transform.Memoized
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.datatype.UrlWithPlaceholder

trait FeatureTrait<T> {

  abstract StrictJavaIdentifier getIdentifier()
  abstract List<Page> getPages()
  abstract UrlWithPlaceholder getBasePath()
  abstract List<ViewModelItem> getViewModel()
  abstract Project getProject()

  /**
   * クラス名(upper-camel)の書式に沿った識別子を返す。
   * @return クラス名
   */
  @Memoized
  String getClassName() {
    identifier.upperCamelized.get()
  }

  /**
   * この機能の関連コンポーネントが配置されるディレクトリのパスを返す。
   * @return 機能の関連コンポーネントが配置されるディレクトリのパス
   */
  @Memoized
  String getComponentDir() {
    "features/${ngIdentifier}"
  }

  /**
   * Angularの標準的な形式の識別子(ハイフン区切り)を返す。
   * @return 識別子(angular標準)
   */
  @Memoized
  String getNgIdentifier() {
    identifier.dasherized.get()
  }

  /**
   * この機能内で使用されている全ての項目種別の一覧を返す。
   * @return 画面内で使用される項目種別の一覧
   */
  @Memoized
  List<PageItemType> getPageItemTypes() {
    pages.collectMany{ it.pageItemTypes }.unique()
  }

  /**
   * この機能内での初期遷移先画面を返す。
   * @return 初期遷移先画面
   */
  @Memoized
  Page getInitialPage() {
    pages.find{ page -> page.initialPage.truthy }
  }

  /**
   * この機能のルーティング用のパスを返す
   * @return ルーティング用パス
   */
  @Memoized
  String getRoutingPath() {
    String featurePath = this.basePath.embedParams { p ->
      ':' + findViewModelItem(p).orElseThrow{ new IllegalArgumentException(
        "The empedded parameter ${p} in base path does not refer to any view model item.\n${this}"
      )}.itemKey
    }.get()
    "/${featurePath}".replaceAll(/\/\/+/, '/')
  }

  /**
   * 変数バインディングを含むパスを返す。
   * @return 変数バインディングを含むパス
   */
  String getBindingPath() {
    String featurePath = this.basePath.embedParams { p ->
      '\\$\\{m.' + findViewModelItem(p).orElseThrow{ new IllegalArgumentException(
        "The empedded parameter ${p} in base path does not refer to any view model item.\n${this}"
      )}.modelPath + '\\}'
    }.get()
    "/${featurePath}".replaceAll(/\/\/+/, '/')
  }

  /**
   * この機能の表示モデル項目のうち、指定された名称に合致するものを取得する
   * ネストするモデル項目の名称は以下の形式で指定する。
   * <pre>
   * (親項目名).(項目名)
   * </pre>
   * 条件に合致する項目が存在しなければ、実行時例外を送出する。
   * 条件に合致する項目が存在しなければ、実行時例外を送出する。
   * @param modelPath モデル項目名称
   * @return 条件に合致する表示モデル項目
   */
  @Memoized
  Optional<ViewModelItem> findViewModelItem(String modelPath) {
    List<String> tokens = modelPath.tokenize('.')
    String parentName = (tokens.size() > 1) ? tokens.first() : null
    String childName = tokens.last()

    Optional.ofNullable(viewModel.find {
      it.name.sameAs(childName) &&
      (parentName ? (it.parent && it.parent.name.sameAs(parentName)) : (it.parent == null))
    })
  }


   /**
   * 指定された名称に合致する画面を返す。
   * 画面名は、<code>(機能名).(画面名)</code> もしくは<code>(画面名) </code>の形で指定し、
   * 後者の場合は、同一機能の画面名を検索する。
   * 条件に合致する画面が存在しない場合は実行時例外を送出する。
   *
   * @param pageName 画面の名称
   * @return 条件に合致する画面
   */
  Page findPage(String pageName) {
    List<String> names = pageName.tokenize('.')
    Feature toFeature = (names.size() == 1) \
                      ? (Feature)this \
                      : project.features.find{ it.name.sameAs(names[0]) }
    if (!toFeature) throw new IllegalStateException(
      "Unknwon feature name: ${pageName}"
    )
    Page toPage = toFeature.pages.find{ it.name.sameAs(names[-1])}
    if (!toFeature) throw new IllegalStateException(
      "Unknwon page name: ${names[-1]} in the feature of ${toFeature.name}"
    )
    toPage
  }
}