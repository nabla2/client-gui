package nabla2.client.gui.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 表示モデル項目バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class ViewModelItemValidator implements Validator<ViewModelItem> {
  Observable<ConstraintViolation<ViewModelItem>> validate(Observable<Table> table$) {
    Observable<ViewModelItem> entity$ = ViewModelItem.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<ViewModelItem>> validateUniqueConstraint(Observable<ViewModelItem> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        featureName : entity.featureName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
        parentItemName : entity.parentItemName.value.map{it.toString()}.orElse(null),
      ])
      ViewModelItem another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<ViewModelItem> duplicated = it.get('__duplicated__')
      new ConstraintViolation<ViewModelItem>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, 機能名, 名称, 親項目名',
          '表示モデル項目',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<ViewModelItem>> validateCardinalityConstraint(Observable<ViewModelItem> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.feature == null) {
        violations.add(new ConstraintViolation<ViewModelItem>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '機能',
            "プロジェクト名=${entity.projectName}, 名称=${entity.featureName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ViewModelItem>> validatePropertyType(Observable<ViewModelItem> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.featureName.messageIfInvalid.map{['機能名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.parentItemName.messageIfInvalid.map{['親項目名', it]}.orElse(null),
        entity.dataType.messageIfInvalid.map{['データ型', it]}.orElse(null),
        entity.defaultValue.messageIfInvalid.map{['デフォルト値', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<ViewModelItem>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ViewModelItem>> validateRequiredProperty(Observable<ViewModelItem> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.featureName.literal.map{''}.orElse('機能名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.parentItemName.literal.map{''}.orElse('親項目名'),
        entity.dataType.literal.map{''}.orElse('データ型'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<ViewModelItem>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}