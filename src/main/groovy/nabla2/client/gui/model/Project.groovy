package nabla2.client.gui.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.client.gui.model.ProjectTrait
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.datatype.UrlWithPlaceholder
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.PageItemType
import nabla2.client.gui.model.Event
import nabla2.client.gui.model.EventFilter
import nabla2.client.gui.model.Action
import nabla2.webapi.model.Service
/**
 * プロジェクト
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Project implements ProjectTrait<Project> {

  /** 論理名 */
  static final String ENTITY_NAME = "プロジェクト"

  // ----- プロパティ定義 ------ //

  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * ベースパス
   */
  UrlWithPlaceholder basePath
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 機能リスト
   */
  @Memoized
  @JsonIgnore
  List<Feature> getFeatures() {
    Feature.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * 画面項目種別リスト
   */
  @Memoized
  @JsonIgnore
  List<PageItemType> getPageItemTypes() {
    PageItemType.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * イベントリスト
   */
  @Memoized
  @JsonIgnore
  List<Event> getEvents() {
    Event.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * イベントフィルタリスト
   */
  @Memoized
  @JsonIgnore
  List<EventFilter> getEventFilters() {
    EventFilter.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * アクションリスト
   */
  @Memoized
  @JsonIgnore
  List<Action> getActions() {
    Action.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * サービスリスト
   */
  @Memoized
  @JsonIgnore
  List<Service> getServices() {
    Service.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Project> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Project(
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            basePath : new UrlWithPlaceholder(row['ベースパス']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}