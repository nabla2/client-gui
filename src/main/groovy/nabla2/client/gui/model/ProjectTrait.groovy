package nabla2.client.gui.model

import groovy.transform.Memoized

trait ProjectTrait<T> {

  abstract List<Feature> getFeatures()

  /**
   * 初期遷移先機能を返す。
   *
   * @return 初期遷移先機能
   */
  @Memoized
  Feature getInitialFeature() {
    features.find{feature -> feature.initial.truthy}
  }
}