package nabla2.client.gui.model

import groovy.transform.Memoized
import nabla2.metamodel.datatype.BooleanFlag
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.datatype.StrictJavaIdentifier

trait PageItemTypeTrait<T> {

  abstract StrictJavaIdentifier getIdentifier()
  abstract ParameterizedText getName()
  abstract BooleanFlag getListItem()
  abstract MultiLineText getState()
  abstract MultiLineText getAdditionalDeclarations()

  /**
   * このUI部品のタグ名を返す。
   * (コンポーネントクラス名と同じ)
   * @return タグ名
   */
  String getTagName() {
    className
  }

  /**
   * このUI部品を実装するコンポーネントクラス名を返す。
   * @return コンポーネントクラス名
   */
  String getClassName() {
    identifier.upperCamelized.get()
  }

  /**
   * Angularのコンベンションに従ったタグ名を返す。
   * @return タグ名(angular規約)
   */
  String getNgTagName() {
    "app-${ngIdentifier}"
  }

  /**
   * Angularのコンベンションに従った識別子(ハイフン区切り)を返す。
   * @return タグ名(angular規約)
   */
  String getNgIdentifier() {
    identifier.dasherized.get()
  }

  /**
   * この項目種別の固有属性値の一覧
   * @return 固有属性値の一覧
   */
  @Memoized
  List<PageItemTypeProp> getAdditionalProps() {
    name.params.collect{p -> p.tokenize(':').with {
      if (it.size < 3) throw new IllegalArgumentException(
        "Illegal parameter description of UI component: ${p}. its format should be ['description':'id':'jsType']\n${this}"
      )
      new PageItemTypeProp(description:it[0], name:it[1], type:it[2..-1].join(':'))}
    }
  }

  /**
   * この項目種別に定義された属性値の一覧
   * @return 属性値の一覧
   */
  @Memoized
  List<PageItemTypeProp> getProps() {
    (listItem.truthy ? [
      new PageItemTypeProp(
        description: '項目添字',
        name: 'index',
        type: 'number',
      )] : []
    ) + (propWithBinding ? [
      new PageItemTypeProp(
        description: "${propWithBinding.description}の表示モデルパス",
        name: 'viewModelPath',
        type: 'string',
      ),
      new PageItemTypeProp(
        description: "${propWithBinding.description}に対する精査エラー",
        name: 'errorMessages',
        type: '{modelPath:string, message:string}[]',
      ),
    ] : []
    ) + additionalProps
  }

  /**
   * このコンポーネントのステートとのバインディングが定義されているプロパティを返す。
   * ステートを持たないコンポーネントは常にnullを返す。
   * @return バインディングが定義されているプロパティ
   */
  @Memoized
  PageItemTypeProp getPropWithBinding() {
    if (state.empty) return null
    additionalProps.find { state.sameAs(it.description) }.with { found ->
      if (!found) throw new IllegalStateException(
        "a component's state '${state}' must be one of the property name of this component."
      )
      found
    }
  }

  /**
   * この項目種別に定義された属性名の一覧
   * @return 属性名の一覧
   */
  @Memoized
  List<String> getPropsNames() {
    props.collect{ it.name.replace('?', '') }
  }

  @Memoized
  List<String> getAdditionalModulesNames() {
    additionalDeclarations.value.map{ v ->
      (v =~ /\{(\s*[a-zA-Z0-9_]+(\s*,\s*[a-zA-Z0-9_]+)*\s*)}/)
        .collect{ it[1] }
        .collectMany{ it.tokenize(',').collect{ it.trim() }}
    }.orElse([])
  }
}