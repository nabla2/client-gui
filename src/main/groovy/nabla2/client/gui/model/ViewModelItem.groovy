package nabla2.client.gui.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.client.gui.model.ViewModelItemTrait
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.trait.Hierarchical
import nabla2.client.gui.model.Feature
/**
 * 表示モデル項目
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class ViewModelItem implements Hierarchical<ViewModelItem>, ViewModelItemTrait<ViewModelItem> {

  /** 論理名 */
  static final String ENTITY_NAME = "表示モデル項目"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * 機能名
   */
  SingleLineText featureName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * 親項目名
   */
  SingleLineText parentItemName
  /**
   * データ型
   */
  MultiLineText dataType
  /**
   * デフォルト値
   */
  MultiLineText defaultValue
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 機能
   */
  @Memoized
  @JsonIgnore
  Feature getFeature() {
    Feature.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.featureName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 親項目
   */
  @Memoized
  @JsonIgnore
  ViewModelItem getParent() {
    ViewModelItem.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.featureName.sameAs(it.featureName) &&
      this.parentItemName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 子項目
   */
  @Memoized
  @JsonIgnore
  List<ViewModelItem> getChildren() {
    ViewModelItem.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.featureName.sameAs(it.featureName) &&
      this.name.sameAs(it.parentItemName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<ViewModelItem> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new ViewModelItem(
            projectName : new SingleLineText(row['プロジェクト名']),
            featureName : new SingleLineText(row['機能名']),
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            parentItemName : new SingleLineText(row['親項目名'] ?: "NO_PARENTS"),
            dataType : new MultiLineText(row['データ型']),
            defaultValue : new MultiLineText(row['デフォルト値']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}