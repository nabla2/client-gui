package nabla2.client.gui.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.client.gui.model.ApiCallTrait
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.webapi.model.Operation
import nabla2.webapi.model.DataItem
import nabla2.client.gui.model.ViewModelItem
import nabla2.webapi.model.Report
/**
 * API呼び出し
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class ApiCall implements ApiCallTrait<ApiCall> {

  /** 論理名 */
  static final String ENTITY_NAME = "API呼び出し"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * 機能名
   */
  SingleLineText featureName
  /**
   * 画面名
   */
  SingleLineText pageName
  /**
   * API名
   */
  SingleLineText apiName
  /**
   * APIバージョン
   */
  SingleLineText apiVersion
  /**
   * 名称
   */
  SingleLineText name
  /**
   * データ項目区分
   */
  SingleLineText dataItemCategory
  /**
   * データ親項目名
   */
  SingleLineText dataParentItemName
  /**
   * データ項目名
   */
  SingleLineText dataItemName
  /**
   * 表示モデル親項目名
   */
  SingleLineText viewModelParentItemName
  /**
   * 表示モデル項目名
   */
  SingleLineText viewModelItemName
  /**
   * コメント
   */
  MultiLineText comment
  /**
   * 帳票名
   */
  SingleLineText reportName

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * API
   */
  @Memoized
  @JsonIgnore
  Operation getApi() {
    Operation.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.apiName.sameAs(it.name) &&
      this.apiVersion.sameAs(it.apiVersion)
    }.blockingFirst(null)
  }
  /**
   * データ項目
   */
  @Memoized
  @JsonIgnore
  DataItem getDataItem() {
    DataItem.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.apiName.sameAs(it.operationName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.dataItemCategory.sameAs(it.location) &&
      this.dataParentItemName.sameAs(it.parentName) &&
      this.dataItemName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 表示モデル項目
   */
  @Memoized
  @JsonIgnore
  ViewModelItem getViewModelItem() {
    ViewModelItem.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.featureName.sameAs(it.featureName) &&
      this.viewModelParentItemName.sameAs(it.parentItemName) &&
      this.viewModelItemName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 帳票
   */
  @Memoized
  @JsonIgnore
  Report getReport() {
    Report.from(_table$).filter {
      this.reportName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<ApiCall> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new ApiCall(
            projectName : new SingleLineText(row['プロジェクト名']),
            featureName : new SingleLineText(row['機能名']),
            pageName : new SingleLineText(row['画面名']),
            apiName : new SingleLineText(row['API名']),
            apiVersion : new SingleLineText(row['APIバージョン']),
            name : new SingleLineText(row['名称']),
            dataItemCategory : new SingleLineText(row['データ項目区分']),
            dataParentItemName : new SingleLineText(row['データ親項目名'] ?: "NO_PARENTS"),
            dataItemName : new SingleLineText(row['データ項目名'] ?: "RESPONSE_ROOT"),
            viewModelParentItemName : new SingleLineText(row['表示モデル親項目名'] ?: "NO_PARENTS"),
            viewModelItemName : new SingleLineText(row['表示モデル項目名']),
            comment : new MultiLineText(row['コメント']),
            reportName : new SingleLineText(row['帳票名']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}