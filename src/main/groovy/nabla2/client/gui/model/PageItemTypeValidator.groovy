package nabla2.client.gui.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 画面項目種別バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class PageItemTypeValidator implements Validator<PageItemType> {
  Observable<ConstraintViolation<PageItemType>> validate(Observable<Table> table$) {
    Observable<PageItemType> entity$ = PageItemType.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<PageItemType>> validateUniqueConstraint(Observable<PageItemType> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
        identifier : entity.identifier.value.map{it.toString()}.orElse(null),
      ])
      PageItemType another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<PageItemType> duplicated = it.get('__duplicated__')
      new ConstraintViolation<PageItemType>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, 名称, 識別子',
          '画面項目種別',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<PageItemType>> validateCardinalityConstraint(Observable<PageItemType> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.project == null) {
        violations.add(new ConstraintViolation<PageItemType>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'プロジェクト',
            "名称=${entity.projectName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<PageItemType>> validatePropertyType(Observable<PageItemType> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.componentPath.messageIfInvalid.map{['コンポーネントパス', it]}.orElse(null),
        entity.state.messageIfInvalid.map{['ステート', it]}.orElse(null),
        entity.hasChildren.messageIfInvalid.map{['子要素を許容', it]}.orElse(null),
        entity.listItem.messageIfInvalid.map{['リスト項目', it]}.orElse(null),
        entity.template.messageIfInvalid.map{['テンプレート', it]}.orElse(null),
        entity.style.messageIfInvalid.map{['スタイル', it]}.orElse(null),
        entity.customProperty.messageIfInvalid.map{['カスタムプロパティ', it]}.orElse(null),
        entity.additionalDeclarations.messageIfInvalid.map{['追加宣言部', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<PageItemType>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<PageItemType>> validateRequiredProperty(Observable<PageItemType> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.componentPath.literal.map{''}.orElse('コンポーネントパス'),
        entity.template.literal.map{''}.orElse('テンプレート'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<PageItemType>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}