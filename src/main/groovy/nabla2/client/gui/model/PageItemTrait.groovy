package nabla2.client.gui.model

import groovy.transform.Memoized
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.trait.Hierarchical

trait PageItemTrait<T> extends Hierarchical<PageItem> {

  abstract Page getPage()

  abstract PageItemType getType()

  abstract MultiLineText getItemTypeName()

  /**
   * コンポーネントのタグ名を返す
   * @return タグ名
   */
  String getTagName() {
    type.tagName
  }

  /**
   * コンポーネントのタグ名(angularの標準コンベンション)を返す
   * @return コンポーネントのタグ名
   */
  String getNgTagName() {
    type.ngTagName
  }

  /**
   * この項目のID属性値を返す
   * @return ID属性値
   */
  String getId() {
    Feature feature = page.feature
    "${feature.identifier}.${page.identifier}.${hierarchy.collect{it.identifier.get()}.join('.')}"
  }

  /**
   * この項目の固有属性値のリストを返す
   * @return 固有属性値のリスト
   */
  @Memoized
  List<PageItemProp> getProps() {
    type.name
        .captureOptions(itemTypeName.get())
        .findAll{ it.key != null }
        .collect{ page.resolveProp(it.key, it.value, this) }
  }

  /**
   * この項目の即値(リテラル)で定義された固有属性値のリストを返す
   * @return 即値定義された固有属性値のリスト
   */
  @Memoized
  List<PageItemProp> getLiteralProps() {
    props.findAll{ it.literal }
  }

  /**
   * この項目の即値(リテラル)で定義された固有属性値のリストを返す
   * @return 即値定義された固有属性値のリスト
   */
  @Memoized
  List<PageItemProp> getReferenceProps() {
    props.findAll{ it.reference }
  }
}