package nabla2.client.gui.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * API呼び出しバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class ApiCallValidator implements Validator<ApiCall> {
  Observable<ConstraintViolation<ApiCall>> validate(Observable<Table> table$) {
    Observable<ApiCall> entity$ = ApiCall.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<ApiCall>> validateUniqueConstraint(Observable<ApiCall> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        featureName : entity.featureName.value.map{it.toString()}.orElse(null),
        pageName : entity.pageName.value.map{it.toString()}.orElse(null),
        apiName : entity.apiName.value.map{it.toString()}.orElse(null),
        apiVersion : entity.apiVersion.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
        dataItemCategory : entity.dataItemCategory.value.map{it.toString()}.orElse(null),
        dataParentItemName : entity.dataParentItemName.value.map{it.toString()}.orElse(null),
        dataItemName : entity.dataItemName.value.map{it.toString()}.orElse(null),
        viewModelParentItemName : entity.viewModelParentItemName.value.map{it.toString()}.orElse(null),
        viewModelItemName : entity.viewModelItemName.value.map{it.toString()}.orElse(null),
      ])
      ApiCall another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<ApiCall> duplicated = it.get('__duplicated__')
      new ConstraintViolation<ApiCall>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, 機能名, 画面名, API名, APIバージョン, 名称, データ項目区分, データ親項目名, データ項目名, 表示モデル親項目名, 表示モデル項目名',
          'API呼び出し',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<ApiCall>> validateCardinalityConstraint(Observable<ApiCall> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.api == null) {
        violations.add(new ConstraintViolation<ApiCall>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '操作',
            "プロジェクト名=${entity.projectName}, 名称=${entity.apiName}, APIバージョン=${entity.apiVersion}",
            entity,
          )
        ))
      }
      if (entity.viewModelItem == null) {
        violations.add(new ConstraintViolation<ApiCall>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '表示モデル項目',
            "プロジェクト名=${entity.projectName}, 機能名=${entity.featureName}, 親項目名=${entity.viewModelParentItemName}, 名称=${entity.viewModelItemName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ApiCall>> validatePropertyType(Observable<ApiCall> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.featureName.messageIfInvalid.map{['機能名', it]}.orElse(null),
        entity.pageName.messageIfInvalid.map{['画面名', it]}.orElse(null),
        entity.apiName.messageIfInvalid.map{['API名', it]}.orElse(null),
        entity.apiVersion.messageIfInvalid.map{['APIバージョン', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.dataItemCategory.messageIfInvalid.map{['データ項目区分', it]}.orElse(null),
        entity.dataParentItemName.messageIfInvalid.map{['データ親項目名', it]}.orElse(null),
        entity.dataItemName.messageIfInvalid.map{['データ項目名', it]}.orElse(null),
        entity.viewModelParentItemName.messageIfInvalid.map{['表示モデル親項目名', it]}.orElse(null),
        entity.viewModelItemName.messageIfInvalid.map{['表示モデル項目名', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
        entity.reportName.messageIfInvalid.map{['帳票名', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<ApiCall>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ApiCall>> validateRequiredProperty(Observable<ApiCall> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.featureName.literal.map{''}.orElse('機能名'),
        entity.pageName.literal.map{''}.orElse('画面名'),
        entity.apiName.literal.map{''}.orElse('API名'),
        entity.apiVersion.literal.map{''}.orElse('APIバージョン'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.dataItemCategory.literal.map{''}.orElse('データ項目区分'),
        entity.dataParentItemName.literal.map{''}.orElse('データ親項目名'),
        entity.dataItemName.literal.map{''}.orElse('データ項目名'),
        entity.viewModelParentItemName.literal.map{''}.orElse('表示モデル親項目名'),
        entity.viewModelItemName.literal.map{''}.orElse('表示モデル項目名'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<ApiCall>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}