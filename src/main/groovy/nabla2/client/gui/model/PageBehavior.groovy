package nabla2.client.gui.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.client.gui.model.PageBehaviorTrait
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.Event
import nabla2.client.gui.model.EventFilter
import nabla2.client.gui.model.Action
/**
 * 画面挙動
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class PageBehavior implements PageBehaviorTrait<PageBehavior> {

  /** 論理名 */
  static final String ENTITY_NAME = "画面挙動"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * 機能名
   */
  SingleLineText featureName
  /**
   * 画面名
   */
  SingleLineText pageName
  /**
   * イベント内容
   */
  MultiLineText eventDesc
  /**
   * フィルタ条件
   */
  MultiLineText filterCondition
  /**
   * アクション内容
   */
  MultiLineText actionDesc
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 画面
   */
  @Memoized
  @JsonIgnore
  Page getPage() {
    Page.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.featureName.sameAs(it.featureName) &&
      this.pageName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * イベント
   */
  @Memoized
  @JsonIgnore
  Event getEvent() {
    Event.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.eventDesc.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * イベントフィルタ
   */
  @Memoized
  @JsonIgnore
  EventFilter getEventFilter() {
    EventFilter.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.filterCondition.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * アクション
   */
  @Memoized
  @JsonIgnore
  Action getAction() {
    Action.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.actionDesc.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<PageBehavior> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new PageBehavior(
            projectName : new SingleLineText(row['プロジェクト名']),
            featureName : new SingleLineText(row['機能名']),
            pageName : new SingleLineText(row['画面名']),
            eventDesc : new MultiLineText(row['イベント内容']),
            filterCondition : new MultiLineText(row['フィルタ条件'] ?: "NO_FILTER"),
            actionDesc : new MultiLineText(row['アクション内容']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}