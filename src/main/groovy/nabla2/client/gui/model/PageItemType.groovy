package nabla2.client.gui.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.client.gui.model.PageItemTypeTrait
import nabla2.metamodel.datatype.BooleanFlag
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.client.gui.model.Project
/**
 * 画面項目種別
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class PageItemType implements PageItemTypeTrait<PageItemType> {

  /** 論理名 */
  static final String ENTITY_NAME = "画面項目種別"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * 名称
   */
  ParameterizedText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * コンポーネントパス
   */
  SingleLineText componentPath
  /**
   * ステート
   */
  MultiLineText state
  /**
   * 子要素を許容
   */
  BooleanFlag hasChildren
  /**
   * リスト項目
   */
  BooleanFlag listItem
  /**
   * テンプレート
   */
  MultiLineText template
  /**
   * スタイル
   */
  MultiLineText style
  /**
   * カスタムプロパティ
   */
  MultiLineText customProperty
  /**
   * 追加宣言部
   */
  MultiLineText additionalDeclarations
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * プロジェクト
   */
  @Memoized
  @JsonIgnore
  Project getProject() {
    Project.from(_table$).filter {
      this.projectName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<PageItemType> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new PageItemType(
            projectName : new SingleLineText(row['プロジェクト名']),
            name : new ParameterizedText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            componentPath : new SingleLineText(row['コンポーネントパス']),
            state : new MultiLineText(row['ステート']),
            hasChildren : new BooleanFlag(row['子要素を許容']),
            listItem : new BooleanFlag(row['リスト項目']),
            template : new MultiLineText(row['テンプレート']),
            style : new MultiLineText(row['スタイル']),
            customProperty : new MultiLineText(row['カスタムプロパティ']),
            additionalDeclarations : new MultiLineText(row['追加宣言部']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}