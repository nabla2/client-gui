package nabla2.client.gui.model

import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier

trait ViewModelItemTrait<T> {

  abstract MultiLineText getDataType()

  abstract Feature getFeature()

  abstract StrictJavaIdentifier getIdentifier()

  abstract List<ViewModelItem> getHierarchy()

  /**
   * パス文字列等で使用するキー文字列を返す。
   * @return キー文字列
   */
  String getItemKey() {
    identifier.get()
  }

  /**
   * この項目が配列かどうかを返す。
   * @return この項目が配列ならtrue
   */
  boolean isArray() {
    dataType.get().endsWith(']')
  }

  /**
   * この項目がオブジェクト(辞書配列)かどうかを返す。
   * @return この項目が配列ならtrue
   */
  boolean isObject() {
    dataType.get().endsWith('}')
  }

  /**
   * この項目の画面表示モデル上の位置を表す文字列を返す。
   * 表示モデルの最上位から階層構造に沿って、項目の識別子を'.'で連結した文字列となる。
   * 項目が配列である場合は、項目の識別子の後に'[]'を付加する。
   *
   * @return この項目の画面表示モデル上の位置を表す文字列
   */
  String getModelPath() {
    feature.identifier.get() + '.' + this.hierarchy.collect{
      it.identifier + (it.array  ? '[]' : '')
    }
    .join('.')
    .replaceAll(/\[]\.[^.]+/, '[]')
    .replaceAll(/\[]$/, '')
  }

  /**
   * この項目の画面表示モデル上の位置を表す文字列を返す。
   * 表示モデルの最上位から階層構造に沿って、項目の識別子を'.'で連結した文字列となる。
   *
   * @return この項目の画面表示モデル上の位置を表す文字列
   */
  String getPath() {
    "${feature.identifier}.${hierarchy.collect{it.identifier.get()}.join('.')}"
  }
}