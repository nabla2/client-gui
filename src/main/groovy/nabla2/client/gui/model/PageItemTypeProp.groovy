package nabla2.client.gui.model

import groovy.transform.Immutable

/**
 * 画面項目種別毎の追加属性
 */
@Immutable
class PageItemTypeProp {

  /** 属性名 */
  final String name

  /** JSデータ型 */
  final String type

  /** 詳細說明 */
  final String description
}
