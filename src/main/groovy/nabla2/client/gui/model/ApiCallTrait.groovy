package nabla2.client.gui.model

import nabla2.metamodel.datatype.SingleLineText
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.Operation

import java.util.function.Function

/**
 * API呼び出し定義モデルの追加操作
 */
trait ApiCallTrait<T> {

  abstract ViewModelItem getViewModelItem()
  abstract DataItem getDataItem()
  abstract SingleLineText  getDataItemCategory()

  boolean isRequestParameter() {
    dataItemCategory.sameAs("リクエストパラメータ")
  }

  boolean isRequestPath() {
    dataItemCategory.sameAs("リクエストパス")
  }

  boolean isRequestBody() {
    dataItemCategory.sameAs("リクエストボディ")
  }

  boolean isRequestPayload() {
    requestParameter || requestBody
  }

  boolean isResponseBody() {
    dataItemCategory.sameAs('レスポンスボディ')
  }

  boolean isBodyMappedAsAWhole() {
    dataItem == null
  }

  static String resolveEndPointUrl(List<ApiCall> mappings) {
    Function<String, String> params = { dataItemName ->
      mappings.find{ it.requestPath && it.dataItem.name.sameAs(dataItemName) }.with { found ->
        if (!found) throw new IllegalStateException(
          "Unknown path parameter, ${dataItemName} is defined in the api call ${mappings.first().name}"
        )
        '\\$\\{m.' + found.viewModelItem.modelPath + '\\}'
      }
    }
    ApiCall mapping = mappings.first()
    mapping.report ? mapping.report.getEndPointUrl(params)
                   : mapping.api.getEndPointUrl(params)
  }

  /**
   * このマッピングが配列間のマッピングを表しているかどうかを返す。
   * @return このマッピングが配列間のマッピングであればtrue
   */
  boolean isIterable() {
    viewModelItem.array && dataItem.array
  }

  /**
   * 項目間のマッピングの型が整合しているかどうかを返す。
   * @return 型が整合していればtrue
   */
  boolean isValidMapping() {
    viewModelItem.array && dataItem.array ||
    viewModelItem.object && dataItem.object ||
    viewModelItem.dataType.sameAs('string') && dataItem.type.sameAs('string') ||
    viewModelItem.dataType.sameAs('number') && dataItem.type.sameAs('number') ||
    viewModelItem.dataType.sameAs('boolean') && dataItem.type.sameAs('boolean')
  }

  /**
   * 与えられたAPIマッピング項目のうち、
   * マッピング対象の画面表示モデルがトップレベル(=親項目がない)項目のリストを取得する。
   * @param mappings APIマッピング定義
   * @return 条件に合致するマッピング項目のリスト
   */
  static List<ApiCall> topLevelViewModelMappings(List<ApiCall> mappings) {
    mappings.findAll{ it.viewModelItem.hierarchy.with{it.subList(0, it.size()-1)}.every{!it.array} }
  }

  /**
   * 与えられたAPIマッピング定義から、APIレスポンスデータ -> 画面表示項目 のマッピングを行うjavascriptコードを生成する。
   * @param mapping
   * @param mappings
   * @param offset
   * @param base
   * @return
   */
  static String mappedDataItemAsJs(ApiCall mapping, List<ApiCall> mappings, int offset = 2, DataItem base = null) {
    String PAD  = ' ' * offset
    String dataPath = mapping.dataItem.relativePathFrom(base)
    String prefix = base ? 'each' : 'body'
    if (!mapping.iterable || mapping.dataItem.children.empty) { // not array | an array whose content does not need to be mapped
      return "${prefix}.${dataPath}"
    }
    if (!mapping.dataItem.children.first().object) { // array of single values
      return "${prefix}.${dataPath}"
    }
    // array of object
    List<ApiCall> nestedMappings =  mappings.findAll{
      !it.dataItem.object && it.dataItem.isDescendentFrom(mapping.dataItem)
    }
    DataItem iterationBase = mapping.dataItem.children.first()
    """\
    ${prefix}.${dataPath}.map((each:any) => {
    |${PAD}  return {
    ${nestedMappings.collect{child -> """\
    |${PAD}    '${child.viewModelItem.itemKey}':${mappedDataItemAsJs(child, mappings, offset + 4, iterationBase)},
    """.trim()}.join('\n')}
    |${PAD}  }
    |${PAD}})
    """.trim()
  }

  static String mappedViewModelItemAsJs(ApiCall mapping, List<ApiCall> mappings, int offset = 2, ViewModelItem base = null) {
    String PAD  = ' ' * offset
    String dataPath = mapping.viewModelItem.relativePathFrom(base) {it.identifier.get()}
    String prefix = base ? 'each' : 'model'
    if (!mapping.iterable || mapping.viewModelItem.children.empty) { // not array | an array whose content does not need to be mapped
      return "${prefix}.${dataPath}"
    }
    if (!mapping.viewModelItem.children.first().object) { // array of single values
      return "${prefix}.${dataPath}"
    }
    // array of object
    List<ApiCall> nestedMappings =  mappings.findAll{
      !it.viewModelItem.object && it.viewModelItem.isDescendentFrom(mapping.viewModelItem)
    }
    ViewModelItem iterationBase = mapping.viewModelItem.children.first()
    """\
    ${prefix}.${dataPath}.map((each:any) => {
    |${PAD}  return {
    ${nestedMappings.collect{child -> """\
    |${PAD}    '${child.dataItem.key}':${mappedViewModelItemAsJs(child, mappings, offset + 4, iterationBase)},
    """.trim()}.join('\n')}
    |${PAD}  }
    |${PAD}})
    """.trim()
  }


//model.${mapping.viewModelItem.modelPath},

}
