package nabla2.client.gui.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 画面バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class PageValidator implements Validator<Page> {
  Observable<ConstraintViolation<Page>> validate(Observable<Table> table$) {
    Observable<Page> entity$ = Page.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Page>> validateUniqueConstraint(Observable<Page> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        featureName : entity.featureName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      Page another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<Page> duplicated = it.get('__duplicated__')
      new ConstraintViolation<Page>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, 機能名, 名称',
          '画面',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<Page>> validateCardinalityConstraint(Observable<Page> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.project == null) {
        violations.add(new ConstraintViolation<Page>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'プロジェクト',
            "名称=${entity.projectName}",
            entity,
          )
        ))
      }
      if (entity.feature == null) {
        violations.add(new ConstraintViolation<Page>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '機能',
            "プロジェクト名=${entity.projectName}, 名称=${entity.featureName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Page>> validatePropertyType(Observable<Page> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.featureName.messageIfInvalid.map{['機能名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.path.messageIfInvalid.map{['パス', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
        entity.initialPage.messageIfInvalid.map{['初期画面', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Page>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Page>> validateRequiredProperty(Observable<Page> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.featureName.literal.map{''}.orElse('機能名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Page>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}