package nabla2.client.gui.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 画面項目バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class PageItemValidator implements Validator<PageItem> {
  Observable<ConstraintViolation<PageItem>> validate(Observable<Table> table$) {
    Observable<PageItem> entity$ = PageItem.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<PageItem>> validateUniqueConstraint(Observable<PageItem> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        featureName : entity.featureName.value.map{it.toString()}.orElse(null),
        pageName : entity.pageName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
        parentItemName : entity.parentItemName.value.map{it.toString()}.orElse(null),
      ])
      PageItem another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<PageItem> duplicated = it.get('__duplicated__')
      new ConstraintViolation<PageItem>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, 機能名, 画面名, 名称, 親項目名',
          '画面項目',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<PageItem>> validateCardinalityConstraint(Observable<PageItem> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.page == null) {
        violations.add(new ConstraintViolation<PageItem>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '画面',
            "プロジェクト名=${entity.projectName}, 機能名=${entity.featureName}, 名称=${entity.pageName}",
            entity,
          )
        ))
      }
      if (entity.type == null) {
        violations.add(new ConstraintViolation<PageItem>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '画面項目種別',
            "プロジェクト名=${entity.projectName}, 名称=${entity.itemTypeName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<PageItem>> validatePropertyType(Observable<PageItem> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.featureName.messageIfInvalid.map{['機能名', it]}.orElse(null),
        entity.pageName.messageIfInvalid.map{['画面名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.parentItemName.messageIfInvalid.map{['親項目名', it]}.orElse(null),
        entity.itemTypeName.messageIfInvalid.map{['項目種別名', it]}.orElse(null),
        entity.defaultValue.messageIfInvalid.map{['デフォルト値', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<PageItem>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<PageItem>> validateRequiredProperty(Observable<PageItem> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.featureName.literal.map{''}.orElse('機能名'),
        entity.pageName.literal.map{''}.orElse('画面名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.parentItemName.literal.map{''}.orElse('親項目名'),
        entity.itemTypeName.literal.map{''}.orElse('項目種別名'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<PageItem>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}