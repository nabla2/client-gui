package nabla2.client.gui.model

import groovy.transform.Canonical

/**
 * この項目の属性値を表す値クラス
 */
@Canonical
class PageItemProp {

  /** 属性名 */
  String key

  /** この属性値のJSデータ型 */
  String dataType

  /** この属性値の値 */
  String value

  /** この値が参照値であるかどうか */
  boolean reference

  /** デバッグ情報 */
  Object debugInfo

  /**
   * この属性値が即値(リテラル値)であるかどうか
   * @return 即値であればtrue
   */
  boolean isLiteral() {
    !reference
  }
}
