package nabla2.client.gui

import groovy.transform.Canonical
import io.reactivex.Observable
import nabla2.client.gui.model.Action
import nabla2.client.gui.model.ActionValidator
import nabla2.client.gui.model.ApiCall
import nabla2.client.gui.model.ApiCallValidator
import nabla2.client.gui.model.Event
import nabla2.client.gui.model.EventFilter
import nabla2.client.gui.model.EventFilterValidator
import nabla2.client.gui.model.EventValidator
import nabla2.client.gui.model.Feature
import nabla2.client.gui.model.FeatureValidator
import nabla2.client.gui.model.Page
import nabla2.client.gui.model.PageBehavior
import nabla2.client.gui.model.PageBehaviorValidator
import nabla2.client.gui.model.PageItem
import nabla2.client.gui.model.PageItemType
import nabla2.client.gui.model.PageItemTypeValidator
import nabla2.client.gui.model.PageItemValidator
import nabla2.client.gui.model.PageValidator
import nabla2.client.gui.model.Project
import nabla2.client.gui.model.ProjectValidator
import nabla2.client.gui.model.ViewModelItem
import nabla2.client.gui.model.ViewModelItemValidator
import nabla2.excel.ExcelWorkbookLoader
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation

/**
 * GUIクライアントエンティティローダ
 *
 * @author nabla2.metamodel.generator
 */
@Canonical
class GuiEntityLoader {

  final Observable<Table> table$

  private GuiEntityLoader(File file) {
    table$ = ExcelWorkbookLoader.from(file).table$
  }

  private GuiEntityLoader(InputStream stream) {
    table$ = ExcelWorkbookLoader.from(stream).table$
  }

  static GuiEntityLoader from(File file) {
    new GuiEntityLoader(file)
  }

  static GuiEntityLoader from(InputStream stream) {
    new GuiEntityLoader(stream)
  }

  Observable<Project> getProject$() {
    Project.from(table$)
  }
  Observable<Feature> getFeature$() {
    Feature.from(table$)
  }
  Observable<ViewModelItem> getViewModelItem$() {
    ViewModelItem.from(table$)
  }
  Observable<Page> getPage$() {
    Page.from(table$)
  }
  Observable<PageItem> getPageItem$() {
    PageItem.from(table$)
  }
  Observable<PageItemType> getPageItemType$() {
    PageItemType.from(table$)
  }
  Observable<PageBehavior> getPageBehavior$() {
    PageBehavior.from(table$)
  }
  Observable<Event> getEvent$() {
    Event.from(table$)
  }
  Observable<EventFilter> getEventFilter$() {
    EventFilter.from(table$)
  }
  Observable<Action> getAction$() {
    Action.from(table$)
  }
  Observable<ApiCall> getApiCall$() {
    ApiCall.from(table$)
  }

  Observable<ConstraintViolation<?>> getConstraintViolation$() {
    Observable.fromArray(
      new ProjectValidator().validate(table$),
      new FeatureValidator().validate(table$),
      new ViewModelItemValidator().validate(table$),
      new PageValidator().validate(table$),
      new PageItemValidator().validate(table$),
      new PageItemTypeValidator().validate(table$),
      new PageBehaviorValidator().validate(table$),
      new EventValidator().validate(table$),
      new EventFilterValidator().validate(table$),
      new ActionValidator().validate(table$),
      new ApiCallValidator().validate(table$),
    ).flatMap{ it }
  }

}